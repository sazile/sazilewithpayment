//
//  ForgotPasswordViewController.swift
//  Sazile
//
//  Created by harjitsingh on 09/11/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import ADCountryPicker
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {
    
    var dailingCode = String()
    
    var mobileNumberWithCountryCodeStr = String()
    var VERIFICATIONID = String()
    
    //MARK:-  OUTLETS
    @IBOutlet weak var dropDownImgVw: UIImageView!
    @IBOutlet weak var forgotPasswordLbl: UILabel!
    @IBOutlet weak var countryImgVw: UIImageView!
    @IBOutlet weak var mobileNumberTF: UITextField!
    
    @IBOutlet weak var countryCodeTF: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutDesign()
        isCountrySelectedForForgot = false
        
        nextBtn.layer.shadowColor = UIColor.lightGray.cgColor
        nextBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        nextBtn.layer.shadowOpacity = 3.0
        nextBtn.layer.shadowRadius = 0.0
        nextBtn.layer.masksToBounds = false
        nextBtn.layer.cornerRadius = nextBtn.frame.size.height / 2
        
        self.forgotPasswordLbl.text = (L102Language.AMLocalizedString(key: "forgot_password", value: ""))
        self.mobileNumberTF.placeholder = (L102Language.AMLocalizedString(key: "mobile_number", value: ""))
        self.nextBtn.setTitle(L102Language.AMLocalizedString(key: "next", value: ""), for: .normal)
    }
    
    func layoutDesign() {
        
        let imgVw = UIImage(named: "sazileLogo")
        let navLogo = UIImageView(image:imgVw)
        self.navigationItem.titleView = navLogo
    }
    static func getInstance() -> ForgotPasswordViewController {
        
        let storyboard = UIStoryboard(name: "ForgotPassword", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isCountrySelectedForForgot{
            isCountrySelectedForForgot = false
        } else {
            
            countryCodeTF.text = "YT +262"
            countryImgVw.image = UIImage(named: "YT")
            dailingCode = "+262"
            
        }
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onTapCountryCodeBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
        let picker = ADCountryPicker(style: .grouped)
        picker.delegate = self
        picker.showCallingCodes = true
        picker.defaultCountryCode = "YT"
        picker.pickerTitle = (L102Language.AMLocalizedString(key: "select_country", value: ""))
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
    }
}
    
    @IBAction func onTapNextBtn(_ sender: UIButton) {
        
//        let vc = OTPViewController.getInstance()
//        self.navigationController?.pushViewController(vc, animated: true)
        
        DispatchQueue.main.async {
        self.mobileNumberTF.text = self.mobileNumberTF.text?.trimmingCharacters(in: .whitespaces)

        if self.countryCodeTF.hasText{

            if self.mobileNumberTF.hasText{
                self.mobileNumberAuthenticationWithFirebase()

            } else {

                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_phone_no", value: "")))
            }

        } else {
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "select_country123", value: "")))

        }
    }
}

    func mobileNumberAuthenticationWithFirebase() {

        self.showActivityIndicator()
        
        mobileNumberWithCountryCodeStr = String(format: "%@%@", dailingCode,self.mobileNumberTF.text ?? "")
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumberWithCountryCodeStr, uiDelegate: nil) { (verificationID, error) in

            if (error != nil) {

                self.hideActivityIndicator()

                ERROR_MESSAGE = error?.localizedDescription ?? ""

                //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "phone_number_format", value: "")))

                return

            } else {

                if verificationID != nil {

                    self.VERIFICATIONID = verificationID ?? ""
                    print("Verification Id From Firebase : \(self.VERIFICATIONID)")

                    self.hideActivityIndicator()

                    DispatchQueue.main.async {

                        let oVc = OTPViewController.getInstance()
                        oVc.firVerificationId = self.VERIFICATIONID
                        oVc.mobileNumberStr = self.mobileNumberWithCountryCodeStr
                        oVc.isForForgotPassword = true
                        self.navigationController?.pushViewController(oVc, animated: true)
                    }
                }
            }
            
        }
    }
    
}

//MARK: - COUNTRYPICKER DELEGATE
extension ForgotPasswordViewController: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        countryCodeTF.text = String(format: "%@ %@", code,dialCode)
        
        countryImgVw.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode
        
        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")
        isCountrySelectedForForgot = true
        navigationController?.popViewController(animated: true)
    }
}

