//
//  SplashViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 24/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    //MARK:-  OUTLETS
    @IBOutlet weak var lngeBtn: UIButton!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var registerLbl: UILabel!
    @IBOutlet weak var registerBgVw: UIView!
    @IBOutlet weak var logInBgVw: UIView!
    
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        languageSelection()
        notificationTimer?.invalidate()
        notificationTimer = nil

        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        let currentLang = UserDefaults.standard.value(forKey: "CurrentLanguage")
        
        if !(currentLang != nil) {
            
            L102Language.setAppleLAnguageTo(lang: "en")
            
        } else {
            
            L102Language.setAppleLAnguageTo(lang: currentLang as! String)
        }
    }
    
    //MARK: - LAYOUT DESIGN
    func layoutDesign() {
        
        registerBgVw.layer.borderWidth = 2.0
        registerBgVw.layer.borderColor = UIColor.white.cgColor
        
        registerBgVw.layer.cornerRadius = registerBgVw.frame.size.height / 2
        registerBgVw.clipsToBounds = true
        
        logInBgVw.layer.borderWidth = 2.0
        logInBgVw.layer.borderColor = UIColor.white.cgColor
        
        logInBgVw.layer.cornerRadius = logInBgVw.frame.size.height / 2
        logInBgVw.clipsToBounds = true
        
        lngeBtn.layer.borderWidth = 2.0
        lngeBtn.layer.cornerRadius = lngeBtn.frame.size.height/2
        lngeBtn.layer.borderColor = UIColor.white.cgColor
        
        
    }
    //MARK: - LANGUAGE SELECTION
    func languageSelection(){
        
        self.loginLbl.text = (L102Language.AMLocalizedString(key: "To_log_in", value: ""))
        self.registerLbl.text = (L102Language.AMLocalizedString(key: "create_an_accounts", value: ""))
        self.lngeBtn.setTitle((L102Language.AMLocalizedString(key: "locale", value: "")), for: .normal)
        
    }
    
    
    //MARK:- IB ACTIONS
    @IBAction func onTapLanguageBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            if (UserDefaults.standard.value(forKey: "CurrentLanguage") as? String) != nil {
                
                let currentLang : String = UserDefaults.standard.value(forKey: "CurrentLanguage") as? String ?? ""
                print("Current Language : \(currentLang)")
                
                if currentLang == "en" {
                    
                    L102Language.setAppleLAnguageTo(lang: "fr")
                    
                } else {
                    
                    L102Language.setAppleLAnguageTo(lang: "en")
                    
                }
                
                self.languageSelection()
                
            }
            
        }
    }
    
    @IBAction func onTapRegisterBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let rVc = RegisterViewController.getInstance()
            self.navigationController?.pushViewController(rVc, animated: true)
        }
    }
    
    @IBAction func onTapLoginBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let lVc = LoginViewController.getInstance()
            lVc.isRegisteredSuccessfully = false
            self.navigationController?.pushViewController(lVc, animated: true)
        }
    }
}
