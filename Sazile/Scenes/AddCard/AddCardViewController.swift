//
//  AddCardViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import Stripe
import SkyFloatingLabelTextField
class AddCardViewController: UIViewController,UITextFieldDelegate {
//pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk

    @IBOutlet weak var cardImgVw: UIImageView!
    @IBOutlet weak var cvcTF: UITextField!
    @IBOutlet weak var expireDateTF: UITextField!
    @IBOutlet weak var cardNumTF: UITextField!
    @IBOutlet weak var cardHolderName: UITextField!
    @IBOutlet weak var addPaymentBtn: UIButton!
    
    var addCard : [String: Any] = [:]
    var userId = String()
    var tokenKey = String()
    var massageID = Int()
    var strClientSecret : String?
    var stpToken : STPToken? = nil
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cardNumTF.delegate = self
        self.expireDateTF.delegate = self
        //self.cvcTF.delegate = self
        //IQKeyboardManager.shared.enable = false
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "save_card", value: ""))
        self.cardHolderName.placeholder = (L102Language.AMLocalizedString(key: "text_credit_card_holder_name", value: ""))
        self.expireDateTF.placeholder = (L102Language.AMLocalizedString(key: "card_expiry", value: ""))
        self.addPaymentBtn.setTitle(L102Language.AMLocalizedString(key: "add_payment", value: ""), for: .normal)
        self.cardNumTF.placeholder = (L102Language.AMLocalizedString(key: "card_number", value: ""))
        
        addPaymentBtn.layer.shadowColor = UIColor.lightGray.cgColor
        addPaymentBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        addPaymentBtn.layer.shadowOpacity = 3.0
        addPaymentBtn.layer.shadowRadius = 0.0
        addPaymentBtn.layer.masksToBounds = false
        addPaymentBtn.layer.cornerRadius = addPaymentBtn.frame.size.height / 2
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let addCardData = data as! NSDictionary
            
            tokenKey = addCardData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = addCardData["user_id"] as? String ?? ""
            print(userId)
        }
        
    }
    
    static func getInstance() -> AddCardViewController {
        let storyboard = UIStoryboard(name: "AddCard", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
    }
    func backOne() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: TEXTFIELD SHOUID CHARACTERS METHOD
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
        
        if (cardNumTF.text == "3"){
            cardImgVw.image = UIImage(named: "amex")
        }else if (cardNumTF.text == "4"){
            cardImgVw.image = UIImage(named: "visa")
        }else if (cardNumTF.text == "5"){
            cardImgVw.image = #imageLiteral(resourceName: "mc")
        }else if (cardNumTF.text == "30"){
            cardImgVw.image = UIImage(named: "dca")
        }else if (cardNumTF.text == "36"){
            cardImgVw.image = UIImage(named: "dclub")
        }else if (cardNumTF.text == "6"){
            cardImgVw.image = UIImage(named: "dc")
        }else if (cardNumTF.text == "35"){
            cardImgVw.image = UIImage(named: "jcb")
        }else if (cardNumTF.text == "62"){
            cardImgVw.image = UIImage(named: "up")
        }else if (cardNumTF.text == "60"){
            cardImgVw.image = UIImage(named: "hp")
        }else if (cardNumTF.text == "63"){
            cardImgVw.image = UIImage(named: "ip")
        }
        if textField == cardNumTF{
            if textField.text!.count >= 19{
                //expireDateTF.becomeFirstResponder()
                
                return true
            }
        }else if textField == expireDateTF{
            if textField.text!.count >= 4{
                //expireDateTF.becomeFirstResponder()
                return true
            }
        }else{
            if textField.text!.count <= 3{
                return false
            }
        }
        
        if textField == cardNumTF {
            textField.text = currentText.grouping(every: 4, with: " ")
            return false
        } else if textField == expireDateTF{ // Expiry Date Text Field
            textField.text = currentText.grouping(every: 2, with: "/")
            return false
        }
        return true
        
        
    }
    
    func getStripeKey(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(commonconfig)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING ======>"  + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print("Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json["data"] as? [String:Any]{
                                    if data["payment_is_live"] as! String == "1"{
                                        // STRIPE KEY
                                        StripeAPI.defaultPublishableKey = "pk_live_XS8g8vKcegq4oYNTenGzVr5Z00ELfZvd6B"
                                    }else{
                                        // STRIPE KEY
                                        StripeAPI.defaultPublishableKey = "pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk"
                                    }
                                    
                                    self.generateToken()
                                }
                            }
                            else
                            {
                                
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Add Card/ Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    //MARK: - IB ACTIONS
    @IBAction func onTapPaymentBtn(_ sender: Any) {
        DispatchQueue.main.async {
            self.cardHolderName.text = self.cardHolderName.text?.trimmingCharacters(in: .whitespaces)
            
            self.cardNumTF.text = self.cardNumTF.text?.trimmingCharacters(in: .whitespaces)
            
            self.expireDateTF.text = self.expireDateTF.text?.trimmingCharacters(in: .whitespaces)
            
            self.cvcTF.text = self.cvcTF.text?.trimmingCharacters(in: .whitespaces)
            
            if self.cardHolderName.hasText{
                
                if self.cardNumTF.hasText{
                    
                    if self.expireDateTF.hasText{
                        
                        if self.cvcTF.hasText{
                            self.generateToken()
                        } else{
                            
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_cvc", value: "")))
                            
                        }
                        
                    } else{
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_expiration", value: "")))
                        
                    }
                    
                } else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "card_number_visa", value: "")))
                    
                }
                
            } else {
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_holder_name", value: "")))
                
                
            }
            
            
            
        }
    }
}
//token_key:87dWl1nusLFc8TJqcXWF8ECgyb26JGbROoOaqcFSXk56ePwrMt
//stripe_token:tok_1EIXdJAIC9dbZPYjjGTYS0yG
//user_id:52
//card_last4_digit:1111
//card_exp_month:11
//card_exp_year:2023
//card_type:VISA
//card_holder_name:Vipin

extension String {
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
        let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
        return String(cleanedUpCopy.enumerated().map() {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
        }.joined().dropFirst())
    }
}


//MARK: -  CARD ADD API
extension AddCardViewController{
    //generate stripe
    ///we will get token from stripe
    func generateToken() {
        let comps = self.expireDateTF.text?.components(separatedBy: "/")
        let f = UInt(comps!.first!)
        let l = UInt(comps!.last!)
        
        let cardParams = STPCardParams()
        cardParams.name = self.cardHolderName.text
        cardParams.number = self.cardNumTF.text
        cardParams.expMonth = f ?? 0
        cardParams.expYear =  l ?? 0
        cardParams.cvc = self.cvcTF.text
        
        STPAPIClient.shared.createToken(withCard: cardParams) { [self]  (token: STPToken?, error: Error?) in
            print("Printing Strip response:\(String(describing: token?.allResponseFields))\n\n")
            print("Printing Strip Token:\(String(describing: token?.tokenId))")
            
            if error != nil {
                print(error?.localizedDescription as Any)
            }
            
            if token != nil{
                
                stpToken = token
                self.getClientSecretFromServer(stripe_token: token?.tokenId ?? "", user_id: userId)
                
            } else {
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "invalid_cardnumber", value: "")))
                
            }
        }
    }
    //create client Secret from server side
    ///client Secret used to save card card or final payment
    func getClientSecretFromServer(stripe_token: String, user_id: String) {
        //4000 0027 6000 3184 for auth card test
        //4242 4242 4242 4242 for normal card without auth
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(setupintent)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "stripe_token" : stripe_token
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING ======>"  + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Add Card Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                // self.transactionHistory = data
                                // self.backOne()
                                self.hideActivityIndicator()
                                if let data = json ["data"] as? [String : Any] {
                                    self.strClientSecret = data["client_secret"] as? String ?? ""
                                    self.setUPIntenet()
                                }
                            }
                            else
                            {
                                self.hideActivityIndicator()
                                UIAlertController.showAlert(vc: self, title: "", message: json["msg"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    print("Add Card/ Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    //Used for 3D auth
    /// client secret from used here if card is 3D secure then new window open for 3D auth
    /// if infomation provided on 3D auth screen is correct then success block will call other wise failure block call
    func setUPIntenet() {
            guard let setupIntentClientSecret = strClientSecret else {
                return;
            }
            // Collect card details
                let comps = self.expireDateTF.text?.components(separatedBy: "/")
                let f = UInt(comps!.first!)
                let l = UInt(comps!.last!)
                
                let cardParams = STPPaymentMethodCardParams()
        cardParams.number = String(format: "%@", self.cardNumTF.text!)
            cardParams.expMonth = NSNumber(value: f ?? 0)
              cardParams.expYear =  NSNumber(value: l ?? 0)
        cardParams.cvc = String(format: "%@", self.cvcTF.text!)
        let billingDetails = STPPaymentMethodBillingDetails()
            billingDetails.name = self.cardHolderName.text
            let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: billingDetails, metadata: nil)
        let setupIntentParams = STPSetupIntentConfirmParams(clientSecret: String(format: "%@", setupIntentClientSecret as CVarArg))
        setupIntentParams.paymentMethodParams = paymentMethodParams


            // Submit the payment
        let paymentHandler = STPPaymentHandler.shared()
        paymentHandler.confirmSetupIntent(setupIntentParams, with: self) { (status, paymentIntent, error) in
                 switch (status) {
                 case .failed:
                    print(error?.localizedDescription ?? "")
                    print(error?.localizedFailureReason ?? "")
                    print(error?.localizedRecoveryOptions ?? "")
                    print(error?.userInfo ?? "")
                     break
                 case .canceled:
                     break
                 case .succeeded:
                    let card  =  self.stpToken?.allResponseFields["card"] as?  [String : Any]
                    let lastFourDigits : String = self.stpToken?.card?.last4 ?? ""
                    let expireMonth = UInt(self.stpToken?.card?.expMonth ?? 0)
                    let expireYear = UInt(self.stpToken?.card?.expYear ?? 0)
                    let cardType : String = card?["brand"] as? String ?? ""
                    let cardHolederName : String = card?["name"] as? String ?? ""
                    self.getAddCardStripePayment(token: self.stpToken?.tokenId ?? "", lastFourDigit: lastFourDigits, cardExpireMonth: String(expireMonth), cardExpireYear: String(expireYear), cardType: cardType, cardHolderName: String(cardHolederName))
                     break
                 @unknown default:
                     fatalError()
                     break
                 }
             }
        }
    // Call add card
    func getAddCardStripePayment(token: String, lastFourDigit: String, cardExpireMonth: String, cardExpireYear: String, cardType: String, cardHolderName: String) {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(ADD_CARD)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "card_last4_digit": lastFourDigit,
                "card_exp_month": cardExpireMonth,
                "card_exp_year": cardExpireYear,
                "card_type": cardType ,
                "card_holder_name" : cardHolderName,
                "stripe_token": token
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING ======>"  + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Add Card Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                // self.transactionHistory = data
                                // self.backOne()
                                self.massageID = json["message_id"] as? Int ?? 0
                                if self.massageID == 86 {
                                    
                                    DispatchQueue.main.async {
                                        
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "card_add_success", value: "")) as? String ?? "", buttonTitle: "OK") {
                                            
                                            self.navigationController?.popViewController(animated: true)
                                            
                                        }
                                        // UIAlertController.showAlert(vc: self, title: "", message: json["msg"] as? String ?? "")
                                        
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["msg"] as? String ?? "")
                                    
                                }
                                
                                
                            }
                            else
                            {
                                self.hideActivityIndicator()
                                UIAlertController.showAlert(vc: self, title: "", message: json["msg"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Add Card/ Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
   
   
}

extension AddCardViewController: STPAuthenticationContext {
  func authenticationPresentingViewController() -> UIViewController {
      return self
  }
}
