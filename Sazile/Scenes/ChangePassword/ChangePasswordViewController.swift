//
//  ChangePasswordViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    //MARK: -  OUTLETS
    @IBOutlet weak var confirmPasswordLbl: UILabel!
    @IBOutlet weak var newPasswordLbl: UILabel!
    @IBOutlet weak var oldPasswordLbl: UILabel!
    @IBOutlet weak var oldPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var updatePWBtn: UIButton!
    
    var dataDict : [String:Any] = [:]
    var tokenKey = String()
    var userId = String()
    var massageID = Int()
    var massageID1 = Int()
    var massageID2 = Int()

    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updatePWBtn.layer.shadowColor = UIColor.lightGray.cgColor
        updatePWBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        updatePWBtn.layer.shadowOpacity = 3.0
        updatePWBtn.layer.shadowRadius = 0.0
        updatePWBtn.layer.masksToBounds = false
        updatePWBtn.layer.cornerRadius = updatePWBtn.frame.size.height / 2
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "Change_Password", value: ""))
        self.oldPasswordLbl.text = (L102Language.AMLocalizedString(key: "old_password", value: ""))
        self.newPasswordLbl.text = (L102Language.AMLocalizedString(key: "new_password", value: ""))
        self.confirmPasswordLbl.text = (L102Language.AMLocalizedString(key: "confirm_password", value: ""))
        self.updatePWBtn.setTitle(L102Language.AMLocalizedString(key: "update", value: ""), for: .normal)
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onClickCancelBtn)), animated: true)
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let changedata = data as! NSDictionary
            
            tokenKey = changedata["token_key"] as? String ?? ""
            print(tokenKey)
            userId = changedata["user_id"] as? String ?? ""
            print(userId)
        }
    }
    
    static func getInstance() -> ChangePasswordViewController {
        
        let storyboard = UIStoryboard(name: "ChangePassword", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
    }
    
    //MARK: - IBACTIONS
    @objc func onClickCancelBtn() {
        
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
   
    @IBAction func onTapPWBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            if self.oldPasswordTF.hasText {
                
                if self.newPasswordTF.hasText {
                    
                    if self.confirmPasswordTF.hasText{
                        
                        self.getchangepassword()
                        
                    } else {
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_confirm_pwd", value: "")))
                    }
                    
                } else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_new_pwd", value: "")))
                }
                
            } else {
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_old_pwd", value: "")))
                
            }
        }
        
    }
}

//MARK: - CHANGE PASSWORD API
extension ChangePasswordViewController {
    
    func getchangepassword() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(CHANGE_PASSWORD)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "old_password": oldPasswordTF.text ?? "",
                          "new_password": newPasswordTF.text ?? "",
                          "confirm_password": confirmPasswordTF.text ?? ""
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Change Password Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                
                                if self.massageID == 124 {
                                    DispatchQueue.main.async {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "change_pwd_success", value: "")), buttonTitle: "Ok") {
                                            let hvc = MainViewController.getInstance()
                                            hvc.setup(type: 1)
                                            self.navigationController?.pushViewController(hvc, animated: true)
                                        }
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    
                                    
                                }
                                
                                
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        self.massageID1 = json["message_id"] as? Int ?? 0
                                        self.massageID2 = json["message_id"] as? Int ?? 0
                                        if self.massageID1 == 125 {
                                            
                                            UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "incorrect_oldpwd", value: "")))

                                        } else if self.massageID2 == 123 {
                                            
                                            UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "pwd_conform_wrong", value: "")))

                                            
                                        } else {
                                            
                                            UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")

                                            
                                        }
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Profile Error:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))

        }
    }
    
}


