//
//  PlansViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class PlansViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var planDataArray : [[String : Any]] = []
    var tokenKey = String()
    var userId = String()
    var planMinutes = String()
    var planId = String()
    var massageID = Int()

    var imageArr = ["bg-1","bg-2","bg-3"]
    
    @IBOutlet weak var plansCollVw: UICollectionView!
    @IBOutlet weak var choosePlansLbl: UILabel!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "plans", value: ""))
        self.choosePlansLbl.text = (L102Language.AMLocalizedString(key: "Choose_a_coupon", value: ""))
        self.choosePlansLbl.text = (L102Language.AMLocalizedString(key: "Choose_a_coupon", value: ""))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let plansdata = data as! NSDictionary
            
            tokenKey = plansdata["token_key"] as? String ?? ""
            print(tokenKey)
            userId = plansdata["user_id"] as? String ?? ""
            print(userId)
            getplans()
        }
    }
    
    static func getInstance() -> PlansViewController {
        
        let storyboard = UIStoryboard(name: "Plans", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PlansViewController") as! PlansViewController
    }
    
    func payMoneyRideAlert() {
        
        let msg = String(format: "%@ %@ minutes Ride",(L102Language.AMLocalizedString(key: "are_you_sure", value: "")) ,planMinutes)
        
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:(L102Language.AMLocalizedString(key: "cancel1", value: "")), style: UIAlertAction.Style.destructive, handler: { _ in
            
        }))
        alert.addAction(UIAlertAction(title:(L102Language.AMLocalizedString(key: "okay", value: "")),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        let pcVC = PaymentCardViewController.getInstance()
                                        pcVC.planId = self.planId
                                        pcVC.isFromPlansBool = true
                                        self.navigationController?.pushViewController(pcVC, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - CollectionView DELEGATE & DATASOURCE
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return planDataArray.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.size.width) / 2 - 20, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        planMinutes = planDataArray[indexPath.item]["plan_minutes"] as? String ?? ""
        planId = planDataArray[indexPath.item]["plan_id"] as? String ?? ""
        
        print(planMinutes)
        payMoneyRideAlert()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlansCollectionViewCell", for: indexPath) as! PlansCollectionViewCell
        
        cell.gainLbl.text = (L102Language.AMLocalizedString(key: "Gain_de", value: ""))
        
        cell.mimutesLbl.text = String(format: " %@ Minutes %@",  planDataArray[indexPath.item]["plan_minutes"] as? String ?? "",(L102Language.AMLocalizedString(key: "ride", value: "")))
        
        cell.moneyLbl.text = String(format: " %@ € ",  planDataArray[indexPath.item]["plan_price"] as? String ?? "")
        
        cell.offerLbl.text = String(format: "+%@ Minutes ",  planDataArray[indexPath.item] ["gain_minutes"]as? String ?? "")
        cell.bgImgVw.image = UIImage(named: imageArr[indexPath.item % imageArr.count])
        // cell.bgImgVw.image =  imageArr[indexPath.item % imageArr.count]
        // cell.bgImgVw.image =  imageArr[indexPath.item]
        
        cell.bgImgVw.layer.cornerRadius = 10
        cell.bgImgVw.clipsToBounds = true
        
        cell.plansVw.layer.shadowOffset = CGSize(width:-1, height:-1 )
        cell.plansVw.layer.shadowOpacity = 1
        cell.plansVw.layer.shadowOffset = CGSize.zero
        cell.plansVw.layer.shadowRadius = 2
        cell.plansVw.clipsToBounds = false
        
        return cell
        
    }
    
}

//MARK: - PLANS API

extension PlansViewController {
    
    func getplans() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(PLAN_LIST)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Plan Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json["data"] as? [[String : Any]] {
                                    
                                    self.planDataArray = data
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.plansCollVw.delegate = self
                                        self.plansCollVw.dataSource = self
                                        self.plansCollVw.reloadData()
                                    }
                                }
                                
                            }
                            else
                            {
                                self.massageID = json["message_id"] as? Int ?? 0
                                if self.massageID == 74 {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "no_plans", value: "")))

                                    
                                } else {
                                    
                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else {
                                            
                                            UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                        }
                                    }
                                    
                                }


                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Plan Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}
