//
//  PlansCollectionViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class PlansCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gainLbl: UILabel!
    @IBOutlet weak var bgImgVw: UIImageView!
    @IBOutlet weak var mimutesLbl: UILabel!
    @IBOutlet weak var moneyLbl: UILabel!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var plansVw: UIView!
    
}

