//
//  inviteViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class InviteViewController: UIViewController {
    
    var invitedFriends : [[String : Any]] = []
    var tokenKey = String()
    var userId = String()
    var page = NSInteger()
    @IBOutlet weak var invitefriendsLbl: UILabel!
    @IBOutlet weak var earningMinsVw: UIView!
    @IBOutlet weak var totalNumbersJoinedLbl: UILabel!
    @IBOutlet weak var minsLbl: UILabel!
    @IBOutlet weak var inviteTblVw: UITableView!
    @IBOutlet weak var inviteFrdsBtn: UIButton!
    @IBOutlet weak var noFriendsLbl: UILabel!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "invite_friends", value: ""))
        self.invitefriendsLbl.text = (L102Language.AMLocalizedString(key: "earning_minutes", value: ""))
        self.inviteFrdsBtn.setTitle(L102Language.AMLocalizedString(key: "invite_more_friends", value: ""), for: .normal)
        
        inviteTblVw.separatorStyle = .none
        
        earningMinsVw.layer.cornerRadius = 5
        earningMinsVw.clipsToBounds = true
        earningMinsVw.layer.shadowOffset = CGSize(width:2, height:2 )
        earningMinsVw.layer.shadowColor = UIColor.black.cgColor
        earningMinsVw.layer.shadowOpacity = 2
        earningMinsVw.layer.shadowOffset = CGSize.zero
        earningMinsVw.layer.shadowRadius = 1
        earningMinsVw.clipsToBounds = false
        
        inviteFrdsBtn.layer.shadowColor = UIColor.lightGray.cgColor
        inviteFrdsBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        inviteFrdsBtn.layer.shadowOpacity = 3.0
        inviteFrdsBtn.layer.shadowRadius = 0.0
        inviteFrdsBtn.layer.masksToBounds = false
        inviteFrdsBtn.layer.cornerRadius = inviteFrdsBtn.frame.size.height / 2
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noFriendsLbl.isHidden = true
        page = 1
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let earningData = data as! NSDictionary
            
            tokenKey = earningData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = earningData["user_id"] as? String ?? ""
            print(userId)
            
        }
        inviteFriends()
        
    }
    
    static func getInstance() -> InviteViewController {
        
        let storyboard = UIStoryboard(name: "Invite", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
    }
    
    
    
    //MARK: - IB ACTIONS
    @IBAction func onTapInviteBtn(_ sender: UIButton) {
        
        
        DispatchQueue.main.async {
            
            let message = ""
            let items = [message]
            
            let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            // if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
            if IS_IPHONE {
                
                self.present(activityController, animated: true, completion: nil)
                
            } else {
                
                let popOver = activityController.popoverPresentationController
                if popOver != nil {
                    popOver?.sourceView = activityController.view
                    popOver?.sourceRect = CGRect(x: 0, y: 0, width: 0, height: 0)
                    
                    self.present(activityController, animated: true)
                }
            }
        }
        
    }
    
}

// MARK:- TABLE VIEW DELEGATE DATA SOURCE
extension InviteViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return invitedFriends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NumberJoiendCell") as! NumberJoiendCell
        
        cell.profileNameLbl.text = String(format: "%@",invitedFriends[indexPath.row]["fname"] as? String ?? "" )
        
        cell.profileImg.sd_setImage(with: URL(string: invitedFriends[indexPath.row]["profile_pic"] as? String ?? ""), placeholderImage: UIImage(named: "user"))
        
        cell.profileImg.layer.cornerRadius = cell.profileImg.frame.size.height/2
        cell.profileImg.clipsToBounds = true
        cell.selectionStyle = .none
        cell.numberJoinedVw.layer.cornerRadius = 5
        cell.numberJoinedVw.clipsToBounds = true
        cell.numberJoinedVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.numberJoinedVw.layer.shadowOpacity = 1.0
        cell.numberJoinedVw.layer.shadowOffset = CGSize.zero
        cell.numberJoinedVw.layer.shadowRadius = 1
        cell.numberJoinedVw.clipsToBounds = false
        
        return cell
    }
    
}

//MARK: - INIVITE API
extension InviteViewController{
    
    func inviteFriends() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(REFERRAL_HISTORY)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" invited Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.minsLbl.text = String(format: "%@ min",json["total_earning"] as? String ?? "")
                                self.totalNumbersJoinedLbl.text = String(format: "%@ %@",json["total_joined_friends"] as? String ?? "",(L102Language.AMLocalizedString(key: "freinds_joined", value: "")))
                                
                                
                                if let data = json["data"] as? [[String : Any]] {
                                    self.invitedFriends = data
                                    
                                    DispatchQueue.main.async {
                                        self.inviteTblVw.delegate = self
                                        self.inviteTblVw.dataSource = self
                                        self.inviteTblVw.reloadData()
                                        
                                    }
                                }
                                
                            }
                            else
                            {
                                
                                self.minsLbl.text = (L102Language.AMLocalizedString(key: "0min", value: ""))
                                
                                self.totalNumbersJoinedLbl.text = (L102Language.AMLocalizedString(key: "freinds_joined00", value: ""))
                                self.noFriendsLbl.isHidden = false
                                self.noFriendsLbl.text = (L102Language.AMLocalizedString(key: "no_freinds_joined00", value: ""))
                                //                               UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "no_freinds_joined00", value: "")))
                                
                                if let error = json[""] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "no_freinds_joined00", value: "")))
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Invited Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}


