//
//  RecentTransactionsViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//user_id:1
//token_key:1xpMVOq00TOWcgwFKgZIAkGrxIK9lUzVTWxFiglqtKDvFdEKtN


import UIKit

class RecentTransactionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var noTransactionsLbl: UILabel!
    var transactionHistory : [[String : Any]] = []
    var tokenKey = String()
    var userId = String()
    var createdAt = String()
    var datestr = String()
    var the_TypeStr = String()
    var paymentMethodStr = String()
    var isFromPagination = Bool()
    var isPagesAvailable = Bool()
    var page = NSInteger()
    var totalPages = NSInteger()
    
    @IBOutlet weak var recentTrnsTblVw: UITableView!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "recent_transactions", value: ""))
        //title = "Recent Transactions"
        recentTrnsTblVw.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noTransactionsLbl.isHidden = true
        page = 1
        isPagesAvailable = true
        isFromPagination = false
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let messeagesData = data as! NSDictionary
            
            tokenKey = messeagesData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = messeagesData["user_id"] as? String ?? ""
            print(userId)
            
        }
        getTransactionsHistory()
        
    }
    
    static func getInstance() -> RecentTransactionsViewController {
        
        let storyboard = UIStoryboard(name: "RecentTransactions", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RecentTransactionsViewController") as! RecentTransactionsViewController
    }
    
    @objc func loadTable() {
        self.recentTrnsTblVw.reloadData()
    }
    
    
    //MARK: - UITABLEVIEW DELEGATE & DATASOURCE
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return transactionHistory.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentTransactionsTableViewCell") as! RecentTransactionsTableViewCell
        
        paymentMethodStr = transactionHistory[indexPath.row]["th_payment_method"] as? String ?? ""
        the_TypeStr = transactionHistory[indexPath.row]["th_type"] as? String ?? ""
        print("See The ====>\(the_TypeStr)")
        
        if paymentMethodStr == "wallet" {
            
            // Wallet
            
            if the_TypeStr == "debit" {
                
                cell.moneyDedudectLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "minutes_deducted", value: "")),transactionHistory[indexPath.row]["th_payment_method"] as? String ?? "")
                
                cell.moneyLbl.text = String(format: "%@ Min",transactionHistory[indexPath.row]["th_credit_debit_minutes"] as? String ?? "")
            }
            
        } else {
            
            // Card
            
            if the_TypeStr == "debit" {
                
                cell.moneyDedudectLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "amount_deducted", value: "")),transactionHistory[indexPath.row]["last_four_digit"] as? String ?? "")
                
            } else {
                
                cell.moneyDedudectLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "amount_added", value: "")),transactionHistory[indexPath.row]["last_four_digit"] as? String ?? "")
            }
            
            cell.moneyLbl.text = String(format: "%@ €",transactionHistory[indexPath.row]["transaction_amt"] as? String ?? "")
        }
        cell.closeingBlncLbl.text = String(format: "%@ %@ min",(L102Language.AMLocalizedString(key: "closing_balance", value: "")),transactionHistory[indexPath.row]["th_closing_minutes"]as? String ?? "")
                
        let dateStr = transactionHistory[indexPath.row]["created_at"]as? String ?? ""
        let strArr = dateStr.components(separatedBy: " ")
        let date1 = strArr[0]
        cell.dateLbl.text = date1
        
        cell.selectionStyle = .none
        cell.recentTrnsCell.layer.cornerRadius = 5
        cell.recentTrnsCell.clipsToBounds = true
        
        cell.recentTrnsCell.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.recentTrnsCell.layer.shadowOpacity = 1.0
        cell.recentTrnsCell.layer.shadowOffset = CGSize.zero
        cell.recentTrnsCell.layer.shadowRadius = 1
        cell.recentTrnsCell.clipsToBounds = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == transactionHistory.count - 1 {
            
            if totalPages == 1 {
                
                isPagesAvailable = false
            }
            
            if isPagesAvailable == true {
                
                page = page + 1
                isFromPagination = true
                
                if totalPages == page {
                    
                    isPagesAvailable = false
                    
                }
                
                getTransactionsHistory()
            }
        }
        
    }
    
}

//MARK: - RECENT TRANSACTIONS API
extension RecentTransactionsViewController{
    
    func getTransactionsHistory()  {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(PAYMENT_LIST)"
            
            let params = [  "user_id": userId,
                            "token_key": tokenKey,
                            "page" : String(format: "%ld", Int(page)),
                            "limit" : String(format: "%ld", Int(10))
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Recent Transactions Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            
                            if(status_code == "1") {
                                
                                self.totalPages = json["total_page"] as! NSInteger
                                
                                if self.isFromPagination {
                                    
                                    if let data = json["data"] as? [[String : Any]] {
                                        
                                        self.transactionHistory.append(contentsOf: data)
                                        
                                        DispatchQueue.main.async {
                                            
                                            self.recentTrnsTblVw.reloadData()
                                        }
                                    }
                                    
                                } else {
                                    
                                    if let data = json["data"] as? [[String : Any]] {
                                        
                                        self.transactionHistory = data
                                        
                                        DispatchQueue.main.async {
                                            
                                            self.recentTrnsTblVw.delegate = self
                                            self.recentTrnsTblVw.dataSource = self
                                            
                                            self.recentTrnsTblVw.reloadData()
                                        }
                                    }
                                }
                                
                            }
                            else
                            {
                                
                                if !self.isFromPagination {
                                    
                                }
                                
                                self.isPagesAvailable = false
                                self.page = self.page - 1
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        self.noTransactionsLbl.isHidden = false
                                        self.noTransactionsLbl.text = (L102Language.AMLocalizedString(key: "no_transactions", value: ""))

                                       // UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                            
                        }
                    }
                case .failure(let err):
                    
                    print("recent Transactions Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
//            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
}


