//
//  HoldedAmountDetailsViewController.swift
//  Sazile
//
//  Created by Pankaj Rana on 22/02/21.
//  Copyright © 2021 Harjit Singh. All rights reserved.
//

import UIKit

class HoldedAmountDetailsViewController: UIViewController {
    @IBOutlet weak var tableVwData: UITableView!
    @IBOutlet weak var noTrasactionsLbl: UILabel!
    
    var transHistory : [[String: Any]] = []
    var userId = String()
    var tokenKey = String()
    var the_TypeStr = String()
    var paymentMethodstr = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "holded_Amount", value: ""))
        // Do any additional setup after loading the view.
        tableVwData.separatorStyle = .none
    }
    
    static func getInstance() -> HoldedAmountDetailsViewController {
        
        let storyboard = UIStoryboard(name: "RecentTransactions", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HoldedAmountDetailsViewController") as! HoldedAmountDetailsViewController
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noTrasactionsLbl.isHidden = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            
            let transactionsData = data as! NSDictionary
            tokenKey = transactionsData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = transactionsData["user_id"] as? String ?? ""
            print(userId)
            
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            
        }
         gethold_refund_summary()
    }
    
    func gethold_refund_summary() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(HoldedAmount)"
            
            let params = [  "user_id": userId,
                            "token_key": tokenKey,
                            "page":"1",
                            "limit":"1"]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" MyWallet Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                            
                                //self.availableMinsLbl.text = json["available_minutes"] as? String ?? ""
                                
                                if let data = json["data"] as? [[String : Any]] {
                                    self.transHistory = data
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.tableVwData.delegate = self
                                        self.tableVwData.dataSource = self
                                        self.tableVwData.reloadData()
                                    }
                                }
                                
                            }else{
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        self.noTrasactionsLbl.isHidden = false
                                        self.noTrasactionsLbl.text = (L102Language.AMLocalizedString(key: "no_transactions", value: ""))
                                        
                                        //UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("MyWallet Error:\(err.localizedDescription)")
                    
                }
            }
            
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}

//MARK: - UITABLEVIEW DELEGATE & DATASOURCE
extension HoldedAmountDetailsViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.transHistory.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "holdedAmountCell") as! holdedAmountCell
        cell.holdedAmt.text = "Holded Amount\n€ \(self.transHistory[indexPath.row]["pr_hold_amount"] as? String ?? "0")"
        cell.capAmt.text = "Captured Amount\n€ \(self.transHistory[indexPath.row]["pr_capture_amount"] as? String ?? "0")"
        cell.refundAmt.text = "Refund Amount\n€ \(self.transHistory[indexPath.row]["pr_refund_amount"] as? String ?? "0")"
        
        cell.dateLbl.text = "Date\n€ \(self.transHistory[indexPath.row]["pr_created_at"] as? String ?? "0")"
        
        cell.selectionStyle = .none
        cell.tableArrayVw.layer.cornerRadius = 5
        cell.tableArrayVw.clipsToBounds = true
        
        cell.tableArrayVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.tableArrayVw.layer.shadowOpacity = 1
        cell.tableArrayVw.layer.shadowOffset = CGSize.zero
        cell.tableArrayVw.layer.shadowRadius = 1
        cell.tableArrayVw.clipsToBounds = false
        
        return cell
    }
    
}



