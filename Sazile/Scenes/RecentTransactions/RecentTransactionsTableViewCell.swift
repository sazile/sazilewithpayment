//
//  RecentTransactionsTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class RecentTransactionsTableViewCell: UITableViewCell {

    @IBOutlet weak var moneyLbl: UILabel!
    @IBOutlet weak var closeingBlncLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var moneyDedudectLbl: UILabel!
    @IBOutlet weak var moneyImgVw: UIImageView!
    @IBOutlet weak var recentTrnsCell: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
