//
//  Payment1ViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class Payment1ViewController: UIViewController {
    
    var noCardsArr : [[String: Any]] = []
    var userId = String()
    var tokenKey = String()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
          if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
                  
                  let getNoCardsArr = data as! NSDictionary
                  
                  tokenKey = getNoCardsArr["token_key"] as? String ?? ""
                  print(tokenKey)
                  userId = getNoCardsArr["user_id"] as? String ?? ""
                  print(userId)
    }
        //getNoCards()
    }
    
}

//token_key:EbXslkW1fCnhHhJnBsQl
//user_id:114

//MARK: - NOCARDS API

//extension Payment1ViewController{
//
//    func getNoCards() {
//
//        if Reachability.isNetworkAvailable(){
//
//            let urlString = "\(BASEURL)\()"
//
//            let params = [  "user_id": userId,
//                            "token": tokenKey
//            ]
//
//            #if DEBUG
//
//            print("BEGIN SAZILE======>URLSTRING" + urlString)
//            print("BEGIN SAZILE=====Parameters>\(params)")
//
//            #endif
//
//            self.showActivityIndicator()
//
//            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
//                for (key,values) in params{
//                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
//                }
//            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
//
//
//                switch result{
//
//                case .success(let upload, _,_):
//
//                    upload.responseJSON { response in
//
//                        #if DEBUG
//
//                        print(" No cards Response : \(response)")
//
//                        #endif
//
//                        if let err = response.error {
//
//                            self.hideActivityIndicator()
//                            print(err)
//                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
//
//                            return
//                        }
//
//                        if let result = response.result.value {
//
//                            let json = result as! NSDictionary
//
//                            let status_code : String = json["success"] as? String ?? ""
//
//                            if(status_code == "1") {
//
//                                if let data = json["data"] as? [[String : Any]] {
//                                    self.noCardsArr = data
//
//
//                               }
//                            }
//                            else
//                            {
//
//                                if let error = json["message"] as? String {
//
//                                    ERROR_MESSAGE = error
//
//                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
//
//                                        self.userSessionExpired()
//
//                                    } else {
//
//                                        self.hideActivityIndicator()
//                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
//                                    }
//                                }
//
//                            }
//                            self.hideActivityIndicator()
//                        }
//                    }
//
//                case .failure(let err):
//
//
//                    print(" No Cards Error:\(err.localizedDescription)")
//
//                }
//            }
//        }
//        else
//        {
//            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
//        }
//    }
//}
//
