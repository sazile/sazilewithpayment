//
//  MapMarkerWindowView.swift
//  Sazile
//
//  Created by Pankaj Rana on 03/03/21.
//  Copyright © 2021 Harjit Singh. All rights reserved.
//

import Foundation
import UIKit


protocol MapMarkerDelegate: class {
    func didTapReserveButton(data: NSDictionary)
}

class MapMarkerWindow: UIView {

    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var reserveLbl: UILabel!
    @IBOutlet weak var reserveVw: UIView!
    @IBOutlet weak var scooterAdressLbl: UILabel!
    @IBOutlet weak var scooterPerLbl: UILabel!
    @IBOutlet weak var scooterNUmLbl: UILabel!
    @IBOutlet weak var distanceKMLbl: UILabel!
    @IBOutlet weak var scootorDetailsVw: UIView!
    
    weak var delegate: MapMarkerDelegate?
    var spotData: NSDictionary?
    
    @IBAction func didTapReserveButton(_ sender: UIButton) {
        delegate?.didTapReserveButton(data: spotData!)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapMarkerWindowView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
}
