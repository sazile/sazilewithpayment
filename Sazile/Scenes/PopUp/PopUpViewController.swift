//
//  PopUpViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 05/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//user_id:1
//token_key:1xpMVOq00TOWcgwFKgZIAkGrxIK9lUzVTWxFiglqtKDvFdEKtN
//qr_code:22
//start_lat:31.255597211963213
//start_lon:75.70365096386718
//card_id:1




import UIKit
import AVFoundation
import CoreLocation
import FittedSheets
import Stripe

class PopUpViewController: UIViewController,QRCodeReaderViewControllerDelegate {
    
    var tapCount = 0
    var qrCodeStr = String()
    var userId = String()
    var tokenKey = String()
    var qrCodeScan : [String: Any] = [:]
    var onlineStr = String()
    var massageID = Int()
    var massageID1 = Int()
    var massageID2 = Int()
    var rideIdStop = String()
    
    var checkByUserZoneDict : [String: Any] = [:]
    
    var stopRideDetailsDict : [String: Any] = [:]
    var currentLattitude = NSString()
    var currentLongitude = NSString()
    var locationManager = CLLocationManager()
    var cardDataDict = NSDictionary()
    var fourDigitsStr = String()
    var cardID = String()
    var isFromLockorUnlockBool = Bool()
    var isReserverd = String()
    var isFromRideStopForHelmet = Bool()
    var deviceDict = NSDictionary()
    var zoneType = String()
    var batteryStr = String()
    var deviceID = String()
    var helmetStatusStr = String()
    
    
    
    // MARK: -  OUTLETS
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var helmetLbl: UILabel!
    @IBOutlet weak var helmetVw: UIView!
    @IBOutlet weak var scooterUnlockingLbl: UILabel!
    @IBOutlet weak var pleaseWaitLbl: UILabel!
    @IBOutlet weak var successfullyDoneLbl: UILabel!
    @IBOutlet weak var failedLodeAgain: UILabel!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var stripePaymentLbl: UILabel!
    @IBOutlet weak var qrCodeLbl: UILabel!
    @IBOutlet weak var enterCodeLbl: UILabel!
    @IBOutlet weak var scooterLockUnlock: UILabel!
    @IBOutlet weak var lastFourDigitsLbl: UILabel!
    @IBOutlet weak var enterCodeTF: UITextField!
    @IBOutlet weak var stripePaymentVw: UIView!
    @IBOutlet weak var saveCardBtn: UIButton!
    @IBOutlet weak var lastDigitCardNumVw: UIView!
    @IBOutlet weak var dismissBTn: UIButton!
    //MARK: - ENTER CODE VIEW
    @IBOutlet weak var enterCodeVw: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var scooterEndSccussVw: UIView!
    @IBOutlet weak var endOkBtn: UIButton!
    @IBOutlet weak var endSccussLbl: UILabel!
    
    //MARK: - SUCCESS VIEW
    @IBOutlet weak var successVw: UIView!
    @IBOutlet weak var readyToRideBtn: UIButton!
    
    @IBOutlet weak var scooterLockVw: UIView!
    @IBOutlet weak var bgVw: UIView!
    
    //MARK: - TRY AGAIN VIEW
    @IBOutlet weak var tryAgianVw: UIView!
    @IBOutlet weak var tryAgainBtn: UIButton!
    
    //MARK: - ADD AMOUNT 60 VIEW
    @IBOutlet weak var viewAddamount: UIView!
    @IBOutlet weak var OkPaymentBtn: UIButton!
    var strClientSecret : String?
    var strStripeOrderId : String?
    var strStripeId : String?

    @IBOutlet weak var qrCodeVw: QRCodeReaderView!{
        
        didSet {
            qrCodeVw.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = true
                $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
                
            })
        }
    }
    
    
    lazy var reader: QRCodeReader = QRCodeReader()
    
    lazy var readerVC: QRCodeReaderViewController = {
        
        let builder = QRCodeReaderViewControllerBuilder {
            
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr,.upce,.code39,.code39Mod43,.ean13,.ean8,.code93,.code128,.pdf417,.aztec,.dataMatrix], captureDevicePosition: .back)
            $0.showTorchButton         = false
            $0.preferredStatusBarStyle = .lightContent
            $0.showOverlayView         = true
            $0.rectOfInterest          = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
            
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.qrCodeLbl.text = (L102Language.AMLocalizedString(key: "you_can_find_the_qr_code_here", value: ""))
        self.enterCodeLbl.text = (L102Language.AMLocalizedString(key: "enter_the_code", value: ""))
        self.submitBtn.setTitle((L102Language.AMLocalizedString(key: "submitt", value: "")), for: .normal)
        self.stripePaymentLbl.text = (L102Language.AMLocalizedString(key: "stripe_payment", value: ""))
        self.changeBtn.setTitle((L102Language.AMLocalizedString(key: "change", value: "")), for: .normal)
        self.saveCardBtn.setTitle((L102Language.AMLocalizedString(key: "save", value: "")), for: .normal)
        
        self.failedLodeAgain.text = (L102Language.AMLocalizedString(key: "failed_to_load_code", value: ""))
        self.scooterUnlockingLbl.text = (L102Language.AMLocalizedString(key: "you_can_find_the_qr_code_here", value: ""))
        //  self.pleaseWaitLbl.text = (L102Language.AMLocalizedString(key: "adding_please_wait", value: ""))
        self.tryAgainBtn.setTitle((L102Language.AMLocalizedString(key: "try_again", value: "")), for: .normal)
        self.readyToRideBtn.setTitle((L102Language.AMLocalizedString(key: "ready_to_ride", value: "")), for: .normal)
        self.successfullyDoneLbl.text = (L102Language.AMLocalizedString(key: "success_done", value: ""))
        
        self.scooterLockUnlock.text = (L102Language.AMLocalizedString(key: "scooter_lock", value: ""))
         self.helmetLbl.text = (L102Language.AMLocalizedString(key: "helmet_start_ride", value: ""))
        self.okBtn.setTitle((L102Language.AMLocalizedString(key: "okay", value: "")), for: .normal)
        self.endOkBtn.setTitle((L102Language.AMLocalizedString(key: "okay", value: "")), for: .normal)
        self.endSccussLbl.text = (L102Language.AMLocalizedString(key: "locked_success", value: ""))

        self.enterCodeTF.text = ""
        
        self.successVw.isHidden = true
        self.enterCodeVw.isHidden = true
        self.tryAgianVw.isHidden = true
        self.scooterLockVw.isHidden = true
        self.viewAddamount.isHidden = true

        submitBtn.layer.shadowColor = UIColor.lightGray.cgColor
        submitBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        submitBtn.layer.shadowOpacity = 3.0
        submitBtn.layer.shadowRadius = 0.0
        submitBtn.layer.masksToBounds = false
        submitBtn.layer.cornerRadius = submitBtn.frame.size.height / 2
        
        okBtn.layer.shadowColor = UIColor.lightGray.cgColor
        okBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        okBtn.layer.shadowOpacity = 3.0
        okBtn.layer.shadowRadius = 0.0
        okBtn.layer.masksToBounds = false
        okBtn.layer.cornerRadius = okBtn.frame.size.height / 2
        
        endOkBtn.layer.shadowColor = UIColor.lightGray.cgColor
        endOkBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        endOkBtn.layer.shadowOpacity = 3.0
        endOkBtn.layer.shadowRadius = 0.0
        endOkBtn.layer.masksToBounds = false
        endOkBtn.layer.cornerRadius = endOkBtn.frame.size.height / 2

        scooterLockVw.layer.cornerRadius = 10
        scooterLockVw.clipsToBounds = true
        scooterLockVw.layer.shadowOffset = CGSize(width:2, height:2 )
        scooterLockVw.layer.shadowColor = UIColor.black.cgColor
        scooterLockVw.layer.shadowOpacity = 2
        scooterLockVw.layer.shadowOffset = CGSize.zero
        scooterLockVw.layer.shadowRadius = 1
        scooterLockVw.clipsToBounds = false
        
        helmetVw.layer.cornerRadius = 10
        helmetVw.clipsToBounds = true
        helmetVw.layer.shadowOffset = CGSize(width:2, height:2 )
        helmetVw.layer.shadowColor = UIColor.black.cgColor
        helmetVw.layer.shadowOpacity = 2
        helmetVw.layer.shadowOffset = CGSize.zero
        helmetVw.layer.shadowRadius = 1
        helmetVw.clipsToBounds = false
        
        enterCodeVw.layer.cornerRadius = 10
        enterCodeVw.clipsToBounds = true
        enterCodeVw.layer.shadowOffset = CGSize(width:-2, height:-2 )
        enterCodeVw.layer.shadowColor = UIColor.black.cgColor
        enterCodeVw.layer.shadowOpacity = 2
        enterCodeVw.layer.shadowOffset = CGSize.zero
        enterCodeVw.layer.shadowRadius = 2
        enterCodeVw.clipsToBounds = false
        
        readyToRideBtn.layer.shadowColor = UIColor.lightGray.cgColor
        readyToRideBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        readyToRideBtn.layer.shadowOpacity = 3.0
        readyToRideBtn.layer.shadowRadius = 0.0
        readyToRideBtn.layer.masksToBounds = false
        readyToRideBtn.layer.cornerRadius = readyToRideBtn.frame.size.height / 2
        
        successVw.layer.cornerRadius = 10
        successVw.clipsToBounds = true
        successVw.layer.shadowOffset = CGSize(width:2, height:2 )
        successVw.layer.shadowColor = UIColor.black.cgColor
        successVw.layer.shadowOpacity = 2
        successVw.layer.shadowOffset = CGSize.zero
        successVw.layer.shadowRadius = 1
        successVw.clipsToBounds = false
        
        saveCardBtn.layer.shadowColor = UIColor.lightGray.cgColor
        saveCardBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        saveCardBtn.layer.shadowOpacity = 3.0
        saveCardBtn.layer.shadowRadius = 0.0
        saveCardBtn.layer.masksToBounds = false
        saveCardBtn.layer.cornerRadius = saveCardBtn.frame.size.height / 2
        
        lastDigitCardNumVw.layer.borderWidth = 1
        lastDigitCardNumVw.layer.borderColor = UIColor.gray.cgColor
        lastDigitCardNumVw.clipsToBounds = true
        
        stripePaymentVw.layer.cornerRadius = 10
        stripePaymentVw.clipsToBounds = true
        stripePaymentVw.layer.shadowOffset = CGSize(width:1, height:1 )
        stripePaymentVw.layer.shadowOpacity = 1
        stripePaymentVw.layer.shadowOffset = CGSize.zero
        stripePaymentVw.layer.shadowRadius = 1
        stripePaymentVw.clipsToBounds = false
        
        tryAgianVw.layer.cornerRadius = 10
        tryAgianVw.clipsToBounds = true
        tryAgianVw.layer.shadowOffset = CGSize(width:2, height:2 )
        tryAgianVw.layer.shadowColor = UIColor.black.cgColor
        tryAgianVw.layer.shadowOpacity = 2
        tryAgianVw.layer.shadowOffset = CGSize.zero
        tryAgianVw.layer.shadowRadius = 1
        tryAgianVw.clipsToBounds = false
        
//        tryAgainBtn.layer.cornerRadius = tryAgainBtn.frame.size.height/2
//        tryAgainBtn.clipsToBounds = true
        tryAgainBtn.layer.shadowColor = UIColor.lightGray.cgColor
        tryAgainBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        tryAgainBtn.layer.shadowOpacity = 3.0
        tryAgainBtn.layer.shadowRadius = 0.0
        tryAgainBtn.layer.masksToBounds = false
        tryAgainBtn.layer.cornerRadius = tryAgainBtn.frame.size.height / 2

        
        
        scooterEndSccussVw.layer.cornerRadius = 10
        scooterEndSccussVw.clipsToBounds = true
        scooterEndSccussVw.layer.shadowOffset = CGSize(width:1, height:1 )
        scooterEndSccussVw.layer.shadowOpacity = 1
        scooterEndSccussVw.layer.shadowOffset = CGSize.zero
        scooterEndSccussVw.layer.shadowRadius = 1
        scooterEndSccussVw.clipsToBounds = false
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onClickCancelBtn)), animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.isFromRideStopForHelmet = false
        self.helmetVw.isHidden = true
        self.isFromLockorUnlockBool = false
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "scann", value: ""))
        self.helmetLbl.text = (L102Language.AMLocalizedString(key: "helmet_start_ride", value: ""))
        self.scooterEndSccussVw.isHidden = true
        if isForSelectCardBool {
            
            if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
                
                self.cardDataDict = cardData as! NSDictionary
                print("Card Data : \(cardData)")
                stripePaymentVw.isHidden = false
                dismissBTn.isHidden = false
                isForSelectCardBool = false
                self.lastFourDigitsLbl.text = self.cardDataDict["card_last_four_digit"] as? String ?? ""
                self.cardID = self.cardDataDict["card_id"] as? String ?? ""
                
            } else {
                
                self.stripePaymentVw.isHidden = true
                self.dismissBTn.isHidden = true
                
                DispatchQueue.main.async {
                    
                    let pcVC = PaymentCardViewController.getInstance()
                    self.navigationController?.pushViewController(pcVC, animated: true)
                }
            }
            
        } else {
            
            stripePaymentVw.isHidden = true
            dismissBTn.isHidden = true
            self.successVw.isHidden = true
            
            if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
                
                let qrCodeData = data as! NSDictionary
                
                tokenKey = qrCodeData["token_key"] as? String ?? ""
                print(tokenKey)
                userId = qrCodeData["user_id"] as? String ?? ""
                print(userId)
            }
            
            isFromQrCodeScan()
            determineMyCurrentLocation()
        }
        
        
        if isForScooterStopadmin {
            
            self.getStopScooterAdminAPI()
            
            
        } else {
            
            
        }
    }
    
    static func getInstance() -> PopUpViewController {
        
        let storyboard = UIStoryboard(name: "PopUp", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
    }
    
    //MARK: - DETERMINE MYCURRENT LOCATION
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.desiredAccuracy = CLLocationAccuracy(kCLDistanceFilterNone)
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        let locationObj = locationManager.location
        let coordLat = locationObj?.coordinate.latitude
        let coordLong = locationObj?.coordinate.longitude
        
        //print("Lat : \(coordLat ?? 0.00)")
        //print("Long : \(coordLong ?? 0.00)")
        
        currentLattitude = NSString(format: "%.6f", coordLat ?? 0.00)
        currentLongitude = NSString(format: "%.6f", coordLong ?? 0.00)
        
        print("BEGIN:Sazile======> Current lattitude : \(currentLattitude)")
        print("BEGIN:Sazile======> Current longitude : \(currentLongitude)")
    }
    
    //MARK: - CARD SELECTED FUNCTION
    
    func cardSelected(){
        
        DispatchQueue.main.async {
            self.helmetVw.isHidden = true
            
            if self.isReserverd == "0" {
                
                if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
                    
                    self.cardDataDict = cardData as! NSDictionary
                    
                    if self.cardDataDict.count == 0 {
                        self.helmetVw.isHidden = true
                        self.stripePaymentVw.isHidden = true
                        self.dismissBTn.isHidden = true
                        
                        DispatchQueue.main.async {
                            let pcVC = PaymentCardViewController.getInstance()
                            self.navigationController?.pushViewController(pcVC, animated: true)
                        }
                        
                    } else {
                        
                        self.stripePaymentVw.isHidden = false
                        self.dismissBTn.isHidden = false
                        self.lastFourDigitsLbl.text = self.cardDataDict["card_last_four_digit"] as? String ?? ""
                        self.cardID = self.cardDataDict["card_id"] as? String ?? ""
                    }
                    
                } else {
                    
                    self.helmetVw.isHidden = true
                    self.stripePaymentVw.isHidden = true
                    self.dismissBTn.isHidden = true
                    
                    DispatchQueue.main.async {
                        let pcVC = PaymentCardViewController.getInstance()
                        self.navigationController?.pushViewController(pcVC, animated: true)
                    }
                }
                
            } else {
                
                if self.isFromRideStopForHelmet {
                    self.helmetVw.isHidden = true
                    self.scooterLockVw.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
                        self.scooterLockVw.isHidden = true
                        let rdVC = RideDetailsViewController.getInstance()
                        let navigationController = UINavigationController(rootViewController: rdVC)
                        navigationController.modalPresentationStyle = .fullScreen
                        rdVC.rideHistrory = self.stopRideDetailsDict
                        self.present(navigationController, animated: true, completion: nil)
                    }
                    
                } else {
                    if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
                        
                        self.cardDataDict = cardData as! NSDictionary
                        if self.cardDataDict.count == 0 {
                            
                            self.stripePaymentVw.isHidden = true
                            self.dismissBTn.isHidden = true
                            self.helmetVw.isHidden = true
                            
                            DispatchQueue.main.async {
                                
                                let pcVC = PaymentCardViewController.getInstance()
                                self.navigationController?.pushViewController(pcVC, animated: true)
                            }
                            
                        } else {
                            
                            self.stripePaymentVw.isHidden = true
                            self.scooterLockVw.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
                                
                                self.scooterLockVw.isHidden = true
                                self.successVw.isHidden = false
                            }
                        }
                        
                    } else {
                        
                        self.stripePaymentVw.isHidden = true
                        self.dismissBTn.isHidden = true
                        self.helmetVw.isHidden = true
                        
                        DispatchQueue.main.async {
                            
                            let pcVC = PaymentCardViewController.getInstance()
                            self.navigationController?.pushViewController(pcVC, animated: true)
                        }
                    }
                }
            }
        }
        
    }
    //MARK:-  IBACTIONS
    @IBAction func onTapSaveCardBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.enterCodeVw.isHidden = true
            self.stripePaymentVw.isHidden = true
            self.scooterLockVw.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//                self.getStratScooterApI()
                self.scooterLockVw.isHidden = true
                self.successVw.isHidden = false
            }
        }
    }
    
    @IBAction func onTapEndOkBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)

        }
    }
    
    @objc func onClickCancelBtn() {
        
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapChangeCardBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            let pvVC = PaymentCardViewController.getInstance()
            isForSelectCardBool = false
            pvVC.strComeFrom = "changecard"
            self.navigationController?.pushViewController(pvVC, animated: true)
            
        }
        
    }
    
    @IBAction func onTapDismissBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.enterCodeVw.isHidden = true
            self.dismissBTn.isHidden = true
            self.stripePaymentVw.isHidden = true
            self.qrCodeVw.isHidden = false
            self.hideKeyBoard()
        }
    }
    
    @IBAction func onTapFlashBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.toggleFlash()
        }
    }
    
    @IBAction func onTapOkBtn(_ sender: UIButton) {
        self.helmetVw.isHidden = true
        self.dismissBTn.isHidden = true

        self.isFromQrCodeScan()
        /*  DispatchQueue.main.async {
         self.helmetVw.isHidden = true
         
         if self.isReserverd == "0" {
         
         if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
         
         self.cardDataDict = cardData as! NSDictionary
         
         if self.cardDataDict.count == 0 {
         self.helmetVw.isHidden = true
         self.stripePaymentVw.isHidden = true
         self.dismissBTn.isHidden = true
         
         DispatchQueue.main.async {
         let pcVC = PaymentCardViewController.getInstance()
         self.navigationController?.pushViewController(pcVC, animated: true)
         }
         
         } else {
         
         self.stripePaymentVw.isHidden = false
         self.dismissBTn.isHidden = false
         self.lastFourDigitsLbl.text = self.cardDataDict["card_last_four_digit"] as? String ?? ""
         self.cardID = self.cardDataDict["card_id"] as? String ?? ""
         }
         
         } else {
         
         self.helmetVw.isHidden = true
         self.stripePaymentVw.isHidden = true
         self.dismissBTn.isHidden = true
         
         DispatchQueue.main.async {
         let pcVC = PaymentCardViewController.getInstance()
         self.navigationController?.pushViewController(pcVC, animated: true)
         }
         }
         
         } else {
         
         if self.isFromRideStopForHelmet {
         self.helmetVw.isHidden = true
         self.scooterLockVw.isHidden = false
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
         self.scooterLockVw.isHidden = true
         let rdVC = RideDetailsViewController.getInstance()
         let navigationController = UINavigationController(rootViewController: rdVC)
         navigationController.modalPresentationStyle = .fullScreen
         rdVC.rideHistrory = self.stopRideDetailsDict
         self.present(navigationController, animated: true, completion: nil)
         }
         
         } else {
         
         if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
         
         self.cardDataDict = cardData as! NSDictionary
         if self.cardDataDict.count == 0 {
         
         self.stripePaymentVw.isHidden = true
         self.dismissBTn.isHidden = true
         self.helmetVw.isHidden = true
         
         DispatchQueue.main.async {
         
         let pcVC = PaymentCardViewController.getInstance()
         self.navigationController?.pushViewController(pcVC, animated: true)
         }
         
         } else {
         
         self.stripePaymentVw.isHidden = true
         self.scooterLockVw.isHidden = false
         
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
         
         self.scooterLockVw.isHidden = true
         self.successVw.isHidden = false
         }
         }
         
         } else {
         
         self.stripePaymentVw.isHidden = true
         self.dismissBTn.isHidden = true
         self.helmetVw.isHidden = true
         
         DispatchQueue.main.async {
         
         let pcVC = PaymentCardViewController.getInstance()
         self.navigationController?.pushViewController(pcVC, animated: true)
         }
         }
         }
         }
         }*/
    }
    
    @IBAction func onTapTryBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.dismissBTn.isHidden = true
            self.tryAgianVw.isHidden = true
            self.isFromQrCodeScan()
        }
        
    }
    
    @IBAction func onTapCodeBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.enterCodeTF.text = ""
            self.qrCodeVw.isHidden = true
            self.enterCodeVw.isHidden = false
            self.dismissBTn.isHidden = false
        }
    }
    
    @IBAction func readyToRideBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.scooterLockVw.isHidden = true
            self.successVw.isHidden = true
            if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
                
                self.cardDataDict = cardData as! NSDictionary
                if self.cardDataDict.count == 0 {
                    
                } else {
                    
                    self.lastFourDigitsLbl.text = self.cardDataDict["card_last_four_digit"] as? String ?? ""
                    self.cardID = self.cardDataDict["card_id"] as? String ?? ""
                    //self.deviceID = self.deviceDict["device_id"] as? String ?? ""
                    //print("DEVICE ID 123 : ======> \(self.deviceID)")
                    
                    self.getStratScooterApI()
                    
                }
                
            } else {
                
                self.stripePaymentVw.isHidden = true
                self.dismissBTn.isHidden = true
                
                DispatchQueue.main.async {
                    let pcVC = PaymentCardViewController.getInstance()
                    self.navigationController?.pushViewController(pcVC, animated: true)
                }
            }
        }
    }
    
    @IBAction func onTapSubmitBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.determineMyCurrentLocation()
            self.enterCodeTF.text = self.enterCodeTF.text?.trimmingCharacters(in: .whitespaces)
            if self.enterCodeTF.hasText {
                
                self.qrCodeStr = self.enterCodeTF.text ?? ""
                self.enterCodeVw.isHidden = true
                self.qrCodeVw.isHidden = false
                
                if isForEndRideBool {
                    
                    self.helmetLbl.text = (L102Language.AMLocalizedString(key: "helmet_end_ride", value: ""))
                    self.scooterLockUnlock.text = (L102Language.AMLocalizedString(key: "scooter_unlock", value: ""))
                    
                    self.deviceID = self.deviceDict["device_id"] as? String ?? ""
                    print("device id sri: =====> \( self.deviceID)")
                    //  self.getStopScooterAPI()
                    // self.getCheckScooterBookingByUserAPI()
                    self.getStopScooterCheckingByUserAPI()
                } else {
                    self.getCheckScooterBookingByUserAPI()
                }
                
            } else{
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_code_first", value: "")))
                
            }
        }
    }
    @IBAction func onTapPaymentOKBtn(_ sender: UIButton) {
        self.setUPIntenet(id: strStripeId ?? "0", order_id: strStripeOrderId ?? "0")
    }
    
    //Used for 3D auth
    /// client secret from used here if card is 3D secure then new window open for 3D auth
    /// if infomation provided on 3D auth screen is correct then success block will call other wise failure block call
    func setUPIntenet(id:String,order_id:String) {
            guard let setupIntentClientSecret = strClientSecret else {
                return;
            }

        let paymentIntentParams = STPPaymentIntentParams(clientSecret: setupIntentClientSecret)
        paymentIntentParams.paymentMethodId = id

        STPPaymentHandler.shared().confirmPayment(paymentIntentParams, with: self) { (status, paymentIntent, error) in
                 switch (status) {
                 case .failed:
                    print(error?.localizedDescription ?? "")
                    print(error?.localizedFailureReason ?? "")
                    print(error?.localizedRecoveryOptions ?? "")
                    print(error?.userInfo ?? "")
                     break
                 case .canceled:
                     break
                 case .succeeded:
                    self.retrievePlanAPI(order_id: self.strStripeOrderId ?? "0", payment_intent_id: paymentIntent!.stripeId)
                     break
                 @unknown default:
                     fatalError()
                     break
                 }
             }
        }
    
    func retrievePlanAPI(order_id:String,payment_intent_id:String) {
//        user_id:1
//        token_key:DEON1fgjHKgCxUF9t5eCg085w
//        pr_id:92
//        payment_intent_id:pi_1IJdryIjUebwhMeLR5peDvzF


        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(retrieve_hold_payment)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "pr_id": order_id,
                          "payment_intent_id": payment_intent_id
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Add Minutes Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""

                            if(status_code == "1" ) {
                                self.viewAddamount.isHidden = true
                                self.view.sendSubviewToBack(self.viewAddamount)
                                self.scooterLockVw.isHidden = true
                                self.successVw.isHidden = false
                                
                                DispatchQueue.main.async {
                                    self.enterCodeVw.isHidden = true
                                    self.stripePaymentVw.isHidden = true
                                    self.scooterLockVw.isHidden = false
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
                        //                self.scooterLockVw.isHidden = true
                        //                self.successVw.isHidden = false
                                        self.getStratScooterApI()

                                    }
                                }
                            }
                            else{
                                if let error = json["message"] as? String {
                                    ERROR_MESSAGE = error
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        self.userSessionExpired()
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                 }
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Add minutes Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            self.hideActivityIndicator()
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    func isFromQrCodeScan() {
        
        DispatchQueue.main.async {
            self.determineMyCurrentLocation()
            
            guard self.checkScanPermissions(), !self.reader.isRunning else { return }
            
            self.reader.didFindCode = { result in
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                
                print("Scan Result Value : \(result.value)")
                
                self.qrCodeStr = result.value
                
                if isForEndRideBool {
                    
                    self.getStopScooterCheckingByUserAPI()
                    // self.getCheckScooterBookingByUserAPI()
                    // self.getStopScooterAPI()
                    self.scooterLockUnlock.text = (L102Language.AMLocalizedString(key: "scooter_unlock", value: ""))

                } else {
                    
                    self.getCheckScooterBookingByUserAPI()
                }
            }
            
            self.reader.startScanning()
            
            guard self.checkScanPermissions() else { return }
            
            // self.readerVC.modalPresentationStyle = .formSheet
            self.readerVC.delegate = self
            
            self.readerVC.completionBlock = { (result: QRCodeReaderResult?) in
                
                if let result = result {
                    
                    
                    print("Completion with result: \(result.value) of type \(result.metadataType)")
                }
            }
            
        }
        
    }
    
    //MARK: -  QR Code Reader Permissions
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
            
        } catch let error as NSError {
            
            let alert: UIAlertController
            
            switch error.code {
                
            case -11852:
                
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    
                    DispatchQueue.main.async {
                        
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
            default:
                
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    //MARK: -  FLASH FUNCTION
    func toggleFlash() {
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else {return}
        
        do {
            
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                
                do {
                    
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            
            print(error)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        stripePaymentVw.isHidden = false
        
        print("SCANNING RESULT=======>")
        //
        dismiss(animated: true) { [self] in
            //
            
            //
            self.qrCodeStr = result.value
            
            //
            //           // self?.onePatientDetailsGet()
        }
        reader.stopScanning()
    }
    
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - START A RIDE API
extension PopUpViewController{
    
    func getStratScooterApI() {
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(START_RIDE)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "start_lat": currentLattitude,
                "start_lon": currentLongitude,
                "qr_code" : deviceID,
                //"device_id": qrCodeStr,
                "card_id": cardID
                ] as [String : Any] 
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Get Scooter Start Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            let authRequired : Int = json["is_authentication_required"] as? Int ?? 0

                            if(status_code == "1") {
                                
                                self.dismiss(animated: true, completion: nil)
                                
                            } else {
                                if(status_code == "0" && authRequired == 1) {
                                    let data = json["data"] as! [String:Any]
                                    self.strClientSecret = data["client_secret"] as? String ?? ""
                                    self.strStripeOrderId = String(format: "%@", data["pr_id"] as! CVarArg)
                                    self.strStripeId = String(format: "%@", data["id"] as! CVarArg)

                                    self.hideActivityIndicator()
                                    self.scooterLockVw.isHidden = true
                                    self.successVw.isHidden = true
                                    self.viewAddamount.isHidden = false
                                    self.view.bringSubviewToFront(self.viewAddamount)
                                    return
                                }
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else if ERROR_MESSAGE == "helmet" {
                                        // Do something
                                        
                                    } else {
                                        
                                        self.massageID = json["message_id"] as? Int ?? 0
                                        self.massageID1 = json["message_id"] as? Int ?? 0
                                        self.massageID2 = json["message_id"] as? Int ?? 0
                                        print("MESSAGE ID : =====> \(self.massageID)")
                                        if self.massageID == 54 {
                                            UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_deactive_select_other_device", value: "")), buttonTitle: "OK") {
                                            }
                                            
                                        } else if self.massageID1 == 61 {
                                            
                                            UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "helmet_status_check", value: "")), buttonTitle: "OK") {
                                                
                                            }
                                            
                                        } else if self.massageID2 == 55 {
                                            
                                            UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "qr_code_numeric_value", value: "")), buttonTitle: "OK") {
                                                
                                            }
                                        } else if self.massageID == 67 {
                                            self.scooterLockUnlock.text = (L102Language.AMLocalizedString(key: "scooter_lock", value: ""))
                                            
                                            self.helmetLbl.text = (L102Language.AMLocalizedString(key: "helmet_start_ride", value: ""))
                                            self.helmetVw.isHidden = false
                                            
                                        } else {
                                            
                                            UIAlertController.showAlertWithOkAction(self, title: "", message: json["message"] as? String ?? "", buttonTitle: "OK") {
                                                
                                                
                                            }
                                        }
                                        // UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("QR CODE Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    //MARK: - GET STOP SCOOTER API
    
    func getStopScooterAPI() {
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(STOP_RIDE)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "end_lat": currentLattitude,
                "end_lon": currentLongitude,
                "qr_code" : deviceID
                //"device_id": qrCodeStr,
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Get Stop Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                //                                zoneRedorNoneTimer?.invalidate()
                                //                                zoneRedorNoneTimer = nil
                                self.helmetLbl.text = (L102Language.AMLocalizedString(key: "helmet_end_ride", value: ""))
                                self.scooterLockUnlock.text = (L102Language.AMLocalizedString(key: "scooter_unlock", value: ""))
                                if let data = json as? [String : Any] {
                                    self.stopRideDetailsDict = data
                                    // self.isFromRideStopForHelmet = true
                                    // self.helmetVw.isHidden = false
                                    self.readyToRideBtn.setTitle((L102Language.AMLocalizedString(key: "ok_text", value: "")), for: .normal)

                                    self.scooterLockVw.isHidden = false
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
                                        self.scooterLockVw.isHidden = true
                                        self.scooterEndSccussVw.isHidden = false
                                        UserDefaults.standard.set(self.stopRideDetailsDict, forKey: "RIDE_DETAILS")
                                        print("sample btn dasmnsamcac")
                                        let controller = SheetViewController(controller: UIStoryboard(name: "RideDetails", bundle: nil).instantiateViewController(withIdentifier: "RideDetailsViewController"), sizes: [.fixed(500), .fixed(500)])
                                        self.present(controller, animated: false, completion: nil)

//                                        let rdVC = RideDetailsViewController.getInstance()
//                                        let navigationController = UINavigationController(rootViewController: rdVC)
//                                        navigationController.modalPresentationStyle = .fullScreen
//                                        rdVC.rideHistrory = self.stopRideDetailsDict
//                                        self.present(navigationController, animated: true, completion: nil)
                                    }
                                }
                                
                            } else {
                                
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                
                                if self.massageID == 67 {
                                    self.scooterLockUnlock.text = (L102Language.AMLocalizedString(key: "scooter_unlock", value: ""))

                                    self.helmetLbl.text = (L102Language.AMLocalizedString(key: "helmet_end_ride", value: ""))
                                    self.scooterLockVw.isHidden = false
                                    
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
                                        self.scooterLockVw.isHidden = true
                                        self.helmetVw.isHidden = false
                                    }
                                    
                                } else {
                                    
                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else if ERROR_MESSAGE == "helmet" {
                                            
                                            // Do something
                                            
                                            
                                        } else {
                                            
                                            self.tryAgianVw.isHidden = false
                                            
                                        }
                                        
                                    }
                                }
                                
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Stop Scooter Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
    //MARK: - GET SCOOTER CHECKING API
    
    func getCheckScooterBookingByUserAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(CHECK_SCOOTER_BOOKINGBY_USER)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "qr_code": qrCodeStr
            ]
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" CheckScooterBooking Response : \(response)")
                        
                        self.enterCodeTF.text = ""
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json as? [String : Any] {
                                    self.checkByUserZoneDict = data
                                    self.zoneType = self.checkByUserZoneDict["current_zone"] as? String ?? ""
                                    
                                }
                                self.helmetVw.isHidden = true
                                if let reserved = json["is_reserved"] as? String {
                                    self.isReserverd = reserved
                                    
                                    if self.isReserverd == "1" {
                                        self.stripePaymentVw.isHidden = true
                                        // self.helmetVw.isHidden = false
                                        
                                        if let ridDeviceDict = json["device_details"] as? [String : Any] {
                                            
                                            self.deviceDict = ridDeviceDict as NSDictionary
                                            self.deviceID = self.deviceDict["device_id"] as? String ?? ""
                                            self.cardSelected()
                                            
                                        }
                                    } else {
                                        
                                        if let ridDeviceDict = json["device_details"] as? [String : Any] {
                                            
                                            self.deviceDict = ridDeviceDict as NSDictionary
                                            self.onlineStr = self.deviceDict["online_status"] as? String ?? ""
                                            self.deviceID = self.deviceDict["device_id"] as? String ?? ""
                                            
                                            // self.qrCodeStr = self.deviceDict["device_id"] as? String ?? ""
                                            
                                            self.helmetStatusStr = self.deviceDict["helmet_status"] as? String ?? ""
                                            print("HELMET STATUS : =======> \(self.helmetStatusStr)")
                                            
                                            print("DEVICE ID : =======> \(self.deviceID)")
                                            
                                            print("ONLINE STATUS: ======> \(self.onlineStr)")
                                            
                                            if let batStr = self.deviceDict["battery"] as? String {
                                                
                                                let myString : String = batStr
                                                let int: Int? = Int(myString)
                                                
                                                if self.zoneType == "0" && int! <= 11 || self.zoneType == "1" && int! <= 11 || self.onlineStr == "off"{
                                                    
                                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                                                         self.isFromQrCodeScan()
                                                    }
                                                    
                                                } else if self.zoneType == "1" && int! >= 11 {
                                                    
                                                    
                                                    self.cardSelected()
                                                    
                                                    //  self.stripePaymentVw.isHidden = false
                                                    //  self.helmetVw.isHidden = false
                                                    
                                                }
                                                    
                                                else if self.zoneType == "2" {
                                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "start_zone", value: "")), buttonTitle: "Ok") {
                                                         self.isFromQrCodeScan()
                                                        self.dismissBTn.isHidden = true

                                                    }
                                                } else if self.zoneType == "0"{
                                                    
                                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "no_zone", value: "")), buttonTitle: "Ok") {
                                                         self.isFromQrCodeScan()
                                                        self.dismissBTn.isHidden = true

                                                    }
                                                    
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            } else  {
                                
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                
                                if self.massageID == 160 {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                                        self.isFromQrCodeScan()
                                    }
                                    
                                    
                                } else {
                                    
                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else {
                                            self.dismissBTn.isHidden = false
                                            self.tryAgianVw.isHidden = false
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("CheckScooterBooking Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    
    @objc func getTapCasBoxAp() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(TAP_CASE_BOX)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          
                          
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Tap case box Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            print(err)
                            //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                if self.massageID == 510 {
                                    
                                    self.view.makeToast ((L102Language.AMLocalizedString(key: "top_case_box", value: "")), duration: 2.0, position: .center, style: style)
                                    
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    
                                    
                                }
                                
                                
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        let defaults = UserDefaults.standard
                                        
                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()
                                        
                                        self.resetUserDefaults()
                                        
                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Tap case box Error:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
            //UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
        }
    }
    
    //MARK: - GET SCOOTER CHECKING API
    
    func getStopScooterCheckingByUserAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(CHECK_SCOOTER_BOOKINGBY_USER)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "qr_code": qrCodeStr
            ]
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" CheckScooterBooking Response : \(response)")
                        
                        self.enterCodeTF.text = ""
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json as? [String : Any] {
                                    self.checkByUserZoneDict = data
                                    self.zoneType = self.checkByUserZoneDict["current_zone"] as? String ?? ""

                                }
                                
                                
                                if let ridDeviceDict = json["device_details"] as? [String : Any] {
                                    
                                    self.deviceDict = ridDeviceDict as NSDictionary
                                    //  self.onlineStr = self.deviceDict["online_status"] as? String ?? ""
                                    self.deviceID = self.deviceDict["device_id"] as? String ?? ""
                                    
                                    // self.qrCodeStr = self.deviceDict["device_id"] as? String ?? ""
                                    
                                    //  self.helmetStatusStr = self.deviceDict["helmet_status"] as? String ?? ""
                                    //  print("HELMET STATUS : =======> \(self.helmetStatusStr)")
                                    
                                    print("DEVICE ID : =======> \(self.deviceID)")
                                    
                                    //  print("ONLINE STATUS: ======> \(self.onlineStr)")
                                    
                                    if self.zoneType == "1" {
                                        
                                        self.getStopScooterAPI()
                                        
                                        
                                    } else if self.zoneType == "0" {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "no_zone", value: "")), buttonTitle: "Ok") {
                                            self.isFromQrCodeScan()
                                        }
                                        
                                    } else {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "start_zone", value: "")), buttonTitle: "Ok") {
                                            self.isFromQrCodeScan()
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                                
                            } else  {
                                
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                
                                if self.massageID == 160 {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                                        self.isFromQrCodeScan()
                                    }
                                    
                                    
                                } else {
                                    
                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else {
                                            
                                            self.tryAgianVw.isHidden = false
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("CheckScooterBooking Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
        //MARK: - GET STOP SCOOTER API
        
        func getStopScooterAdminAPI() {
            if Reachability.isNetworkAvailable(){
                
                let urlString = "\(BASEURL)\(RIDEHISTRORY_DETAILS)"
                
                let params = [
                    "user_id": userId,
                    "token_key": tokenKey,
                    "ride_id" : rideIdStop
                
                    ] as [String : Any]
                
                #if DEBUG
                
                print("BEGIN SAZILE======>URLSTRING" + urlString)
                print("BEGIN SAZILE=====Parameters>\(params)")
                
                #endif
                
               // self.showActivityIndicator()
                
                ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                    for (key,values) in params{
                        MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                    
                    
                    switch result{
                        
                    case .success(let upload, _,_):
                        
                        upload.responseJSON { response in
                            
                            #if DEBUG
                            
                            print(" Get ride stop response : \(response)")
                            
                            #endif
                            
                            if let err = response.error {
                                
                                self.hideActivityIndicator()
                                print(err)
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                                
                                return
                            }
                            
                            if let result = response.result.value {
                                
                                let json = result as! NSDictionary
                                
                                let status_code : String = json["success"] as? String ?? ""
                                
                                if(status_code == "1") {
                                    
                                     if let data = json ["data"] as? [String : Any] {
                                        
                                        self.stopRideDetailsDict = data
                                        isForEndRideBool = false
                                        isFromDismissRideDetailsVwBool = false
                                  //  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0) {
                                        
                                        UserDefaults.standard.set(self.stopRideDetailsDict, forKey: "RIDE_DETAILS")
                                        print("sample btn dasmnsamcac")
                                        let controller = SheetViewController(controller: UIStoryboard(name: "RideDetails", bundle: nil).instantiateViewController(withIdentifier: "RideDetailsViewController"), sizes: [.halfScreen, .fullScreen])
                                        self.present(controller, animated: false, completion: nil)
                                       // }
                                    }
                                    
                                } else {
                                    
                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else if ERROR_MESSAGE == "helmet" {
                                            
                                            // Do something
                                            
                                            
                                        } else {
                                            
                                            self.tryAgianVw.isHidden = false
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                self.hideActivityIndicator()
                            }
                        }
                        
                    case .failure(let err):
                        
                        print(" Get ride stop Error:\(err.localizedDescription)")
                        
                    }
                }
            }else{
                
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
                //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
            }
        }
    
}

extension PopUpViewController: STPAuthenticationContext {
  func authenticationPresentingViewController() -> UIViewController {
      return self
  }
}
