//
//  DocuVfViewController.swift
//  Sazile
//
//  Created by Harjit Singh Mac on 07/12/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class DocuVfViewController: UIViewController {
    var massageStr = String()

    @IBOutlet weak var msgLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.msgLbl.text = massageStr
        print(massageStr)
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "messages", value: ""))

    }

    static func getInstance() -> DocuVfViewController{
        let storyboard = UIStoryboard(name: "DocuVf", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "DocuVfViewController") as! DocuVfViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
}
