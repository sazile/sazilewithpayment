//
//  MyWalletViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//user_id:1
//token_key:1xpMVOq00TOWcgwFKgZIAkGrxIK9lUzVTWxFiglqtKDvFdEKtN



import UIKit
import Stripe
class MyWalletViewController: UIViewController {
    
    @IBOutlet weak var noTrasactionsLbl: UILabel!
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var recentTrnsctionLbl: UILabel!
    @IBOutlet weak var paymentsLbl: UILabel!
    @IBOutlet weak var avilableminsLbl: UILabel!
    @IBOutlet weak var availableMinsLbl: UILabel!
    @IBOutlet weak var paymentVw: UIView!
    @IBOutlet weak var couponsVw: UIView!
    @IBOutlet weak var myReferralsVw: UIView!
    @IBOutlet weak var tableVwData: UITableView!
    @IBOutlet weak var holdedAmount: UIView!
    @IBOutlet weak var holdedAmountlbl: UILabel!
    
    @IBOutlet weak var coupansLbl: UILabel!
    @IBOutlet weak var referalBl: UILabel!
    var transHistory : [[String: Any]] = []
    var userId = String()
    var tokenKey = String()
    var the_TypeStr = String()
    var paymentMethodstr = String()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "my_wallet", value: ""))
        self.navigationItem.backButtonTitle = (L102Language.AMLocalizedString(key: "my_wallet", value: ""))
        self.avilableminsLbl.text = (L102Language.AMLocalizedString(key: "available_minutes", value: ""))
        self.paymentsLbl.text = (L102Language.AMLocalizedString(key: "payments", value: ""))
        self.referalBl.text = (L102Language.AMLocalizedString(key: "referral", value: ""))
        self.holdedAmountlbl.text = (L102Language.AMLocalizedString(key: "holded_Amount", value: ""))
        self.coupansLbl.text = (L102Language.AMLocalizedString(key: "coupons", value: ""))
        self.recentTrnsctionLbl.text = (L102Language.AMLocalizedString(key: "recent_transcartions", value: ""))
        self.viewAllBtn.setTitle(L102Language.AMLocalizedString(key: "vieww_all", value: ""), for: .normal)
        
        tableVwData.separatorStyle = .none
        
        paymentVw.layer.cornerRadius = 10
        paymentVw.clipsToBounds = true
        paymentVw.layer.shadowOffset = CGSize(width:1, height:1 )
        paymentVw.layer.shadowOpacity = 0.5
        paymentVw.layer.shadowRadius = 1
        paymentVw.clipsToBounds = false
        
        holdedAmount.layer.cornerRadius = 10
        holdedAmount.clipsToBounds = true
        holdedAmount.layer.shadowOffset = CGSize(width:1, height:1 )
        holdedAmount.layer.shadowOpacity = 0.5
        holdedAmount.layer.shadowRadius = 1
        holdedAmount.clipsToBounds = false
        
        couponsVw.layer.cornerRadius = 10
        couponsVw.clipsToBounds = true
        couponsVw.layer.shadowOffset = CGSize(width:1, height:1 )
        couponsVw.layer.shadowOpacity = 0.5
        couponsVw.layer.shadowRadius = 1
        couponsVw.clipsToBounds = false
        
        myReferralsVw.layer.cornerRadius = 10
        myReferralsVw.clipsToBounds = true
        myReferralsVw.layer.shadowOffset = CGSize(width:1, height:1 )
        myReferralsVw.layer.shadowOpacity = 0.5
        myReferralsVw.layer.shadowRadius = 1
        myReferralsVw.clipsToBounds = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noTrasactionsLbl.isHidden = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            
            let transactionsData = data as! NSDictionary
            tokenKey = transactionsData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = transactionsData["user_id"] as? String ?? ""
            print(userId)
            
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            
        }
        getTransHistory()
    }
    
    static func getInstance() -> MyWalletViewController {
        
        let storyboard = UIStoryboard(name: "MyWallet", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyWalletViewController") as! MyWalletViewController
    }
    
    // MARK:- IB ACTIONS
    @IBAction func onTapMyReferralBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            let rvc = ReferralViewController.getInstance()
            self.navigationController?.pushViewController(rvc, animated: true)
        }
        
    }
    
    @IBAction func onTapCopsBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let plvc = PlansViewController.getInstance()
            self.navigationController?.pushViewController(plvc, animated: true)
        }
        
    }
    
    @IBAction func onTapPaymentBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let pcvc = PaymentCardViewController.getInstance()
            self.navigationController?.pushViewController(pcvc, animated: true)
        }
        
    }
    
    @IBAction func holdedAmount(_ sender: Any) {
        DispatchQueue.main.async {
            let pcvc = HoldedAmountViewController.getInstance()
            self.navigationController?.pushViewController(pcvc, animated: true)
        }
        
        
    }
    @IBAction func onTapVwAllBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            let rtvc = RecentTransactionsViewController.getInstance()
            self.navigationController?.pushViewController(rtvc, animated: true)        }
    }
    
    func getStripeKey(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(commonconfig)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING ======>"  + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print("Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                            
                                if let data = json["data"] as? [String:Any]{
                                    if data["payment_is_live"] as! String == "1"{
                                        // STRIPE KEY
                                        StripeAPI.defaultPublishableKey = "pk_live_XS8g8vKcegq4oYNTenGzVr5Z00ELfZvd6B"
                                    }else{
                                        // STRIPE KEY
                                        StripeAPI.defaultPublishableKey = "pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk"
                                    }
                                }
                            }
                            else
                            {
                               
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Add Card/ Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
}

//MARK: - UITABLEVIEW DELEGATE & DATASOURCE
extension MyWalletViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return transHistory.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWalletTableViewCell") as! MyWalletTableViewCell
        
        paymentMethodstr = transHistory[indexPath.row]["th_payment_method"] as? String ?? ""
        the_TypeStr = transHistory[indexPath.row]["th_type"] as? String ?? ""
        print("See The ====>\(the_TypeStr)")
        
        if paymentMethodstr == "wallet" {
            
            // Wallet
            
            if the_TypeStr == "debit" {
                
                cell.minutesLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "minutes_deducted", value: "")),transHistory[indexPath.row]["th_payment_method"] as? String ?? "")
                
                cell.minsLbl.text = String(format: "%@ Min",transHistory[indexPath.row]["th_credit_debit_minutes"] as? String ?? "")
            }
            
        } else {
            
            // Card
            
            if the_TypeStr == "debit" {
                
                cell.minutesLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "amount_deducted", value: "")),transHistory[indexPath.row]["last_four_digit"] as? String ?? "")
                
            } else {
                
                cell.minutesLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "amount_added", value: "")),transHistory[indexPath.row]["last_four_digit"] as? String ?? "")
            }
            
            cell.minsLbl.text = String(format: "%@ €",transHistory[indexPath.row]["transaction_amt"] as? String ?? "")
        }
        
        cell.closingMinsLbl.text = String(format: "%@ %@ min",(L102Language.AMLocalizedString(key: "closing_balance", value: "")),transHistory[indexPath.row] ["th_closing_minutes"]as? String ?? "")
        
        let dateStr = transHistory[indexPath.row]["created_at"]as? String ?? ""
        let strArr = dateStr.components(separatedBy: " ")
        
        let date1 = strArr[0]
        cell.dateLbl.text = date1
        
        cell.selectionStyle = .none
        cell.tableArrayVw.layer.cornerRadius = 5
        cell.tableArrayVw.clipsToBounds = true
        
        cell.tableArrayVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.tableArrayVw.layer.shadowOpacity = 1
        cell.tableArrayVw.layer.shadowOffset = CGSize.zero
        cell.tableArrayVw.layer.shadowRadius = 1
        cell.tableArrayVw.clipsToBounds = false
        
        return cell
    }
    
}

//MARK: - MY WALLET API
extension MyWalletViewController{
    
    func getTransHistory() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(MY_WALLET)"
            
            let params = [  "user_id": userId,
                            "token_key": tokenKey ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" MyWallet Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.availableMinsLbl.text = json["available_minutes"] as? String ?? ""
                                if let data = json["data"] as? [[String : Any]] {
                                    self.transHistory = data
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.tableVwData.delegate = self
                                        self.tableVwData.dataSource = self
                                        self.tableVwData.reloadData()
                                    }
                                }
                                
                            }else{
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        self.noTrasactionsLbl.isHidden = false
                                        self.noTrasactionsLbl.text = (L102Language.AMLocalizedString(key: "no_transactions", value: ""))
                                        
                                        //UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("MyWallet Error:\(err.localizedDescription)")
                    
                }
            }
            
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
}



