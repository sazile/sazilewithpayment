//
//  MyWalletTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 27/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class MyWalletTableViewCell: UITableViewCell {

    @IBOutlet weak var minsLbl: UILabel!
    @IBOutlet weak var closingMinsLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var minutesLbl: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var tableArrayVw: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
