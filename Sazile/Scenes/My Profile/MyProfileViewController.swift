//
//  MyProfileViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import SDWebImage

class MyProfileViewController: UIViewController {
    
    //MARK: -  OUTLETS
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var emailAddressLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var upDatesDoucmentsBtn: UIButton!
    @IBOutlet weak var EMAILtf: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var choiceImg: UIImageView!
    @IBOutlet weak var updateProfileBtn: UIButton!
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet var profileVw: UIView!
    @IBOutlet weak var profileImgVw: UIImageView!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var deleteAccountBtn: UIButton!

    var imgData = NSData()
    var dataDict : [String:Any] = [:]
    var tokenKey = String()
    var userId = String()
    
    var govtIDStr = String()
    var licenceIdStr = String()
    var massageID = Int()

    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isForImageBool = false
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "my_profile", value: ""))
        profileImgVw.layer.cornerRadius = profileImgVw.frame.size.height / 2
        profileImgVw.clipsToBounds = true
        
        profileVw.layer.borderWidth = 5
        profileVw.layer.borderColor = UIColor.white.cgColor
        profileVw.layer.cornerRadius = profileVw.frame.size.height / 2
        profileVw.clipsToBounds = true
        
        updateProfileBtn.layer.shadowColor = UIColor.lightGray.cgColor
        updateProfileBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        updateProfileBtn.layer.shadowOpacity = 3.0
        updateProfileBtn.layer.shadowRadius = 0.0
        updateProfileBtn.layer.masksToBounds = false
        updateProfileBtn.layer.cornerRadius = updateProfileBtn.frame.size.height / 2
        
        changePasswordBtn.layer.shadowColor = UIColor.lightGray.cgColor
        changePasswordBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        changePasswordBtn.layer.shadowOpacity = 3.0
        changePasswordBtn.layer.shadowRadius = 0.0
        changePasswordBtn.layer.masksToBounds = false
        changePasswordBtn.layer.cornerRadius = updateProfileBtn.frame.size.height / 2
        
        upDatesDoucmentsBtn.layer.shadowColor = UIColor.lightGray.cgColor
        upDatesDoucmentsBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        upDatesDoucmentsBtn.layer.shadowOpacity = 3.0
        upDatesDoucmentsBtn.layer.shadowRadius = 0.0
        upDatesDoucmentsBtn.layer.masksToBounds = false
        upDatesDoucmentsBtn.layer.cornerRadius = upDatesDoucmentsBtn.frame.size.height / 2
        
        
        deleteAccountBtn.layer.shadowColor = UIColor.lightGray.cgColor
        deleteAccountBtn.layer.shadowOffset = CGSize(width:0, height: 2.5)
        deleteAccountBtn.layer.shadowOpacity = 3.0
        deleteAccountBtn.layer.shadowRadius = 0.0
        deleteAccountBtn.layer.masksToBounds = false
        deleteAccountBtn.layer.cornerRadius = deleteAccountBtn.frame.size.height / 2
        
        self.nameLbl.text = (L102Language.AMLocalizedString(key: "name", value: ""))
        self.emailAddressLbl.text = (L102Language.AMLocalizedString(key: "email_address", value: ""))
        self.phoneNumberLbl.text = (L102Language.AMLocalizedString(key: "phone_number", value: ""))
        self.updateProfileBtn.setTitle(L102Language.AMLocalizedString(key: "update_profile", value: ""), for: .normal)
        self.changePasswordBtn.setTitle(L102Language.AMLocalizedString(key: "change_passwordss", value: ""), for: .normal)
        self.upDatesDoucmentsBtn.setTitle(L102Language.AMLocalizedString(key: "updates_documentsT", value: ""), for: .normal)
        
        deleteAccountBtn.setTitle(L102Language.AMLocalizedString(key: "delete_account", value: ""), for: .normal)
    }
    
    func sellectphoto() {
        
        let alert = UIAlertController(title: (L102Language.AMLocalizedString(key: "choose_image", value: "")), message: "", preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)

        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: UIAlertAction.Style.default, handler: { _ in
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "photo_library", value: "")),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.photoLibrary()
                                        
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "cancel1", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
        }))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isForImageBool{
            
            isForImageBool = false
             
        } else{
            
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            
            if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
                
                let myprofiledata = data as! NSDictionary
                
                print(myprofiledata)
                
                tokenKey = myprofiledata["token_key"] as? String ?? ""
                print(tokenKey)
                userId = myprofiledata["user_id"] as? String ?? ""
                print(userId)
               
            }
             getProfile()
        }
       
    }
    
    static func getInstance() -> MyProfileViewController {
        
        let storyboard = UIStoryboard(name: "MyProfile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
    }
    
    func configureProfileData() {
        
        if let data = UserDefaults.standard.value(forKey: "PROFILE_RESPONSE") {
            
            let profileData = data as! NSDictionary
            
            print(profileData)
            
            self.nameTF.text = profileData["name"] as? String
            self.EMAILtf.text = profileData["email"] as? String
            self.mobileNumberTF.text = profileData["phone"] as? String
            guard let image = profileData["profile_pic"] as? String else { return  }
            
            profileImgVw.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "user_profile"))
        }
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onClickedPwBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let cpvc = ChangePasswordViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: cpvc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onTapUpadeDocumentsBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let chVC = UpdateDocumentsViewController.getInstance()
            //            chVC.govtIDStr = self.govtIDStr
            //            chVC.licenceIdStr = self.licenceIdStr
            self.navigationController?.pushViewController(chVC, animated: true)
        }
        
    }
    
    @IBAction func onTapUpdateProfileBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            if self.nameTF.hasText {
                
                if self.EMAILtf.hasText {
                    
                    if AppHelper.isValidEmail(with: self.EMAILtf.text) {
                        isForImageBool = true
                       self.setprofile()
                        
                    } else {
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "email_not_valid", value: "")))
                    }
                    
                } else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "email_required", value: "")))
                }
                
            } else {
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "name_required", value: "")))
                
            }
        }
    }
    
    @IBAction func onTapCameraBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isForImageBool = true
            self.sellectphoto()
        }
    }
    
    @IBAction func deleteAccountBtn(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: (L102Language.AMLocalizedString(key: "delete_accountmessage", value: "")), preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)

        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "yes", value: "")), style: UIAlertAction.Style.default, handler: { _ in
//            self.openCamera()
            self.deleteAccount()
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "cancel1", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
        }))
        
        
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - UIPICKERVIEW DELEGATE UI NAVIGATIONCONTROLLER DELEGATE

extension MyProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .photoLibrary
            
            self.present(imagePickerController, animated: true, completion: nil)
            
        }
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
            
            let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            
            let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
            profileImgVw.image = chosenImage

            imgData = data!
            
            // print("BEGIN:Sazile ======> Image Data : \(imgData)")
            
            picker.dismiss(animated: true)
            
        } else {
            
            print("BEGIN:Sazile ======> Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - GET PROFILE API

extension MyProfileViewController{
    
    func getProfile() {
        
        if Reachability.isNetworkAvailable(){
            
            
            let urlString = "\(BASEURL)\(GETPROFILEOF_USERBY_ID)"
            
            let params = ["user_id": userId,
                          "token_key" : tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Get Profile Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json as? [String : Any] {
                                    
                                    self.dataDict = (data["data"] as! NSDictionary) as! [String : Any]
                                    print(self.dataDict)
                                    
                                    self.govtIDStr = self.dataDict["government_id"] as? String ?? ""
                                    print(self.govtIDStr)
                                    
                                    self.licenceIdStr = self.dataDict["licence_bsr"] as? String ?? ""
                                    print(self.licenceIdStr)
                                    
                                    UserDefaults.standard.set(self.dataDict, forKey: "PROFILE_RESPONSE")
                                    self.configureProfileData()
                                }
                                
                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Profile Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    //MARK: - SET PROFILE API
    func setprofile() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(SETPROFILEOF_USERBY_ID)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "name": nameTF.text ?? "",
                          "email": EMAILtf.text ?? "",
                          "phone": mobileNumberTF.text ?? ""
                
                
                
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if self.imgData.count > 0 {
                    
                    let data : Data = self.imgData as Data
                    
                    MultipartFormData.append(data, withName: "profile_pic", fileName: "image.png", mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Set Profile Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json as? [String : Any] {
                                    
                                    self.dataDict = (data["data"] as! NSDictionary) as! [String : Any]
                                    print(self.dataDict)
                                    
                                    UserDefaults.standard.set(self.dataDict, forKey: "PROFILE_RESPONSE")
                                    self.configureProfileData()
                                    self.getProfile()
                                   
                                }
                                
                                self.massageID = json["message_id"] as? Int ?? 0

                                if self.massageID == 118 {
                                    DispatchQueue.main.async {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "profile_update", value: "")), buttonTitle: "Ok") {
                                            self.navigationController?.popViewController(animated: true)
                                            
                                        }
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                }

                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Profile Error:\(err.localizedDescription)")
                    
                }
            }
            
        }else{
            
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    func deleteAccount() {
        
        if Reachability.isNetworkAvailable(){
            
            
            let urlString = "\(BASEURL)\(delete_Account)"
            
            let params = ["user_id": userId,
                          "token_key" : tokenKey
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Get Profile Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                let defaults = UserDefaults.standard
                                defaults.set(false, forKey: "IS_LOGGED_IN")
                                defaults.synchronize()

                                self.resetUserDefaults()

                                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                
                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Profile Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}


