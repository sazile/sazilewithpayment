//
//  CouponsViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class CouponsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var couponsTableVw: UITableView!
        var couponsArr = [String]()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutDesign()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    static func getInstance() -> InboxViewController {
        
        let storyboard = UIStoryboard(name: "Inbox", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InboxViewController") as! InboxViewController
    }
    
    func layoutDesign(){

    }
    
    //MARK: - UITABLEVIEW DELEGATE & DATASOURCE
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponsTableViewCell") as? CouponsTableViewCell
        
         return cell!
     }

 }
