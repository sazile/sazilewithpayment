//
//  CouponsTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class CouponsTableViewCell: UITableViewCell {

   
    @IBOutlet weak var codeCopyBtn: UIButton!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var couponCodeLbl: UILabel!
    @IBOutlet weak var percentageLbl: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
