//
//  AddPaymentViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class AddPaymentViewController: UIViewController {

    @IBOutlet weak var paymentVw: UIView!
    @IBOutlet weak var proceedBtn: UIButton!
    @IBOutlet weak var highMoneyBtn: UIButton!
    @IBOutlet weak var mdmMoneyBtn: UIButton!
    @IBOutlet weak var moneyBtn: UIButton!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
         title = "Add Payments"
        
               highMoneyBtn.layer.borderWidth = 1.0
               highMoneyBtn.layer.borderColor = UIColor.white.cgColor
               highMoneyBtn.layer.cornerRadius = highMoneyBtn.frame.size.height/2
               highMoneyBtn.clipsToBounds = true
        
               mdmMoneyBtn.layer.borderWidth = 1.0
               mdmMoneyBtn.layer.borderColor = UIColor.white.cgColor
               mdmMoneyBtn.layer.cornerRadius = mdmMoneyBtn.frame.size.height/2
               mdmMoneyBtn.clipsToBounds = true
               
               moneyBtn.layer.borderWidth = 1.0
               moneyBtn.layer.borderColor = UIColor.white.cgColor
               moneyBtn.layer.cornerRadius = moneyBtn.frame.size.height/2
               moneyBtn.clipsToBounds = true
                  
               proceedBtn.layer.borderWidth = 1.0
               proceedBtn.layer.borderColor = UIColor.white.cgColor
               proceedBtn.layer.cornerRadius = proceedBtn.frame.size.height/2
               proceedBtn.clipsToBounds = true
        
              paymentVw.layer.cornerRadius = 10
              //paymentVw.clipsToBounds = true
        
                paymentVw.layer.shadowColor = UIColor.darkGray.cgColor
                paymentVw.layer.shadowOffset = CGSize(width:2.0, height:2.0 )
                paymentVw.layer.shadowOpacity = 1.0
                paymentVw.layer.shadowRadius = 2
                paymentVw.clipsToBounds = false

       
    }
    
    static func getInstance() -> AddPaymentViewController {
        
        let storyboard = UIStoryboard(name: "AddPayment", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddPaymentViewController") as! AddPaymentViewController
    }
   
}
