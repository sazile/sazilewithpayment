//
//  Slide.swift
//  Sazile
//
//  Created by Harjit Singh on 27/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class Slide: UIView {
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var slideImgVw: UIImageView!
}
