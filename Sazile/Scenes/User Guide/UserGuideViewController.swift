//
//  UserGuideViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class UserGuideViewController: UIViewController {
    
    //MARK: -  OUTLETS
    @IBOutlet weak var getStartedBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bgVw: UIView!
    
    @IBOutlet weak var scrollVw: UIScrollView! {
        
        didSet {
            
            scrollVw.delegate = self
        }
    }
    
    var slides:[Slide] = []
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getStartedBtn.setTitle((L102Language.AMLocalizedString(key: "get_started", value: "")), for: .normal)
        getStartedBtn.layer.cornerRadius = getStartedBtn.frame.size.height/2
        getStartedBtn.clipsToBounds = true
         setUpScrollView()
    }
    static func getInstance() -> UserGuideViewController {
        
        let storyboard = UIStoryboard(name: "UserGuide", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "UserGuideViewController") as! UserGuideViewController
    }
    
    func setUpScrollView() {
        
        scrollVw.alwaysBounceHorizontal = false
        scrollVw.alwaysBounceVertical = false
        scrollVw.bounces = false
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)

        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        bgVw.bringSubviewToFront(pageControl)
    }
    
    //MARK: -  IBACTIONS
    @IBAction func onClickGetStartedBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension UserGuideViewController : UIScrollViewDelegate {
    
    //MARK: -  UIScrollView Delegate
    
    func setupSlideScrollView(slides : [Slide]) {
        
        scrollVw.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: scrollVw.frame.height)
        scrollVw.contentSize = CGSize(width: scrollVw.frame.width * CGFloat(slides.count), height: scrollVw.frame.height)
        scrollVw.isPagingEnabled = true
       
        for i in 0 ..< slides.count {
            
            slides[i].frame = CGRect(x: scrollVw.frame.width * CGFloat(i), y: 0, width: scrollVw.frame.width, height: scrollVw.frame.height)
            scrollVw.addSubview(slides[i])
        }
    }
    
    //MARK: - SLIDES
    
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.slideImgVw.image = UIImage(named: "scooter_one.png")
        slide1.titleLbl.text = (L102Language.AMLocalizedString(key: "scan_to_ridee", value: ""))
        slide1.descLbl.text = (L102Language.AMLocalizedString(key: "scan_qr_code", value: ""))//"Scan the QR Code on top of the scooter to unlock the ride."
        
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.slideImgVw.image = UIImage(named: "scooter_two.png")
        slide2.titleLbl.text = (L102Language.AMLocalizedString(key: "safe_riding", value: ""))//()"Safe Riding"
        slide2.descLbl.text = (L102Language.AMLocalizedString(key: "own_helmet", value: ""))//"Bring your own helmet to stay safe while your ride."
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.slideImgVw.image = UIImage(named: "scooter_three.png")
        slide3.titleLbl.text = (L102Language.AMLocalizedString(key: "foot_placement", value: ""))//"Foot Placement"
        slide3.descLbl.text = (L102Language.AMLocalizedString(key: "place_both_feet", value: ""))//"Place both feet on footboard while riding."

        return [slide1, slide2, slide3]
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}
