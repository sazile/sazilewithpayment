//
//  SideMenuViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var logoutLbl: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var menuTblVw: UITableView!
    @IBOutlet weak var logoutStopVw: UIView!
    @IBOutlet weak var logoutStopLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var okBtn: UIButton!
    var imgData = NSData()
    var titlesArr = [String]()
    var imagesArr = [String]()
    var isEnglishSelected = Bool()
    var refreshControl = UIRefreshControl()
    var tokenKey = String()
    var userId = String()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoutStopVw.layer.cornerRadius = 10
        logoutStopVw.clipsToBounds = true
        logoutStopVw.layer.shadowOffset = CGSize(width:2, height:2 )
        logoutStopVw.layer.shadowColor = UIColor.black.cgColor
        logoutStopVw.layer.shadowOpacity = 2
        logoutStopVw.layer.shadowOffset = CGSize.zero
        logoutStopVw.layer.shadowRadius = 1
        logoutStopVw.clipsToBounds = false
        
        okBtn.layer.shadowColor = UIColor.lightGray.cgColor
        okBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        okBtn.layer.shadowOpacity = 3.0
        okBtn.layer.shadowRadius = 0.0
        okBtn.layer.masksToBounds = false
        okBtn.layer.cornerRadius = okBtn.frame.size.height / 2
        
        nsNotifications()
        imgVw.layer.cornerRadius = imgVw.frame.size.height / 2
        imgVw.clipsToBounds = true
        layoutDesign()
        // menuTblVw.reloadData()
        
        // refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender: )), for: UIControl.Event.valueChanged)
        menuTblVw.addSubview(refreshControl) // not required when using UITableViewController
       
    }
    
    @objc func refresh(sender:AnyObject)
        
    {
        // Updating your data here...
        
        self.menuTblVw.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.logoutStopVw.isHidden = true
        self.backBtn.isHidden = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            let collectionData = data as! NSDictionary
            tokenKey = collectionData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = collectionData["user_id"] as? String ?? ""
            print(userId)
            
        }
        
        
        configureLoginData()
        
        if (UserDefaults.standard.value(forKey: "CurrentLanguage") as? String) != nil {
            
            let currentLang : String = UserDefaults.standard.value(forKey: "CurrentLanguage") as? String ?? ""
            print("Current Language : \(currentLang)")
            
            if currentLang == "en" {
                
                
                isEnglishSelected = true
                
            } else {
                
                isEnglishSelected = false
                
            }
            
            menuTblVw.reloadData()
            
        }
        
        if let data = UserDefaults.standard.value(forKey: "PROFILE_RESPONSE") {
            
            let profileData = data as! NSDictionary
            
            print(profileData)
            
            //self.imgVw.text = profileData["name"] as? String
            self.emailLbl.text = profileData["email"] as? String
            self.nameLbl.text = profileData["name"] as? String
            guard let image = profileData["profile_pic"] as? String else { return  }
            
            imgVw.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "user_profile"))
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        layoutDesign()
    }
    
    //MARK: - PROFILE DATA
    @objc func configureLoginData() {
        
        if let logindata = UserDefaults.standard.value(forKey: "LOGIN_DETAILS_RESPONSE") {
            
            let userData = logindata as! NSDictionary
            
            print(userData)
            self.nameLbl.text = userData["fname"] as? String
            self.emailLbl.text = userData["email"] as? String
            
            guard let image = userData["profile_pic"] as? String else { return  }
            
            imgVw.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "user_profile"))
        }
    }
    
    //MARK: LAYOUT DESIGN
    @objc func layoutDesign() {
        
        imagesArr = ["home-icon","inbox","profile-icon","trip","wallet-icon","amount","language-icon","guide","terms_conditions","privacy_policy","user_agreement"]
        
        titlesArr = [L102Language.AMLocalizedString(key: "home", value: ""), L102Language.AMLocalizedString(key: "inbox", value: ""), L102Language.AMLocalizedString(key: "my_profile", value: ""), L102Language.AMLocalizedString(key: "my_trips", value: ""), L102Language.AMLocalizedString(key: "my_wallet", value: "my_wallet"),
                     L102Language.AMLocalizedString(key: "Quotients_Summary", value: "Quotients_Summary"),
                     L102Language.AMLocalizedString(key: "change_language",value: ""), L102Language.AMLocalizedString(key: "guide",value: ""),L102Language.AMLocalizedString(key: "terms1", value: ""),      L102Language.AMLocalizedString(key: "privacy2", value: ""),L102Language.AMLocalizedString(key: "user3", value: "")]
        self.okBtn.setTitle((L102Language.AMLocalizedString(key: "okay", value: "")), for: .normal)
        self.logoutStopLbl.text = (L102Language.AMLocalizedString(key: "you_must_rental_stop", value: ""))

        self.logoutLbl.text = (L102Language.AMLocalizedString(key: "Sign_Out", value: ""))
        
        menuTblVw.separatorStyle = .none
        
        menuTblVw.delegate = self
        menuTblVw.dataSource = self
        menuTblVw.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        getnotificationTimer()
        
    }
    
    //MARK: NOTIFICATION CENTER
    @objc func nsNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.layoutDesign), name: NSNotification.Name(LOCALIZATIONREFRESH), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.configureLoginData), name: NSNotification.Name(REFRESHPROFILEDATA), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifications), name: NSNotification.Name(NOTIFICATIONREFRESH), object: nil)
    }
    
    @objc func notifications() {
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            let collectionData = data as! NSDictionary
            tokenKey = collectionData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = collectionData["user_id"] as? String ?? ""
            print(userId)
        }
        getNotificationsCountSideMenuAPI()
    }
    //MARK: - LOGOUT ALERT
    func showSimpleAlert() {
        
        let alert = UIAlertController(title: (L102Language.AMLocalizedString(key: "Sign_Out", value: "")), message: (L102Language.AMLocalizedString(key: "want_to_logout", value: "")), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "no", value: "")), style: UIAlertAction.Style.default, handler: { _ in
            
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "yes", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
//                                        UserDefaults.standard.set(false, forKey: "IS_LOGGED_IN")
//                                        UserDefaults.standard.synchronize()
                                        self.getlogouAPI()
                                        //                                        self.resetUserDefaults()
                                        //
                                        //                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        //                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - SELECTLANGUAGE
    func selectLanguage() {
        
        let alert = UIAlertController(title: L102Language.AMLocalizedString(key: "", value: ""), message: L102Language.AMLocalizedString(key: "change_language",value: ""), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: L102Language.AMLocalizedString(key: "english", value: ""), style: UIAlertAction.Style.default, handler: { _ in
            L102Language.setAppleLAnguageTo(lang: "en")
            
            NotificationCenter.default.post(name: Notification.Name(LOCALIZATIONREFRESH), object: nil)
            
            
        }))
        
        alert.addAction(UIAlertAction(title:L102Language.AMLocalizedString(key: "french", value: ""),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        L102Language.setAppleLAnguageTo(lang: "fr")
                                        NotificationCenter.default.post(name: Notification.Name(LOCALIZATIONREFRESH), object: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title:L102Language.AMLocalizedString(key: "can_cel", value: ""),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getnotificationTimer(){
        
        if notificationTimer != nil{
            notificationTimer!.invalidate()
            notificationTimer = nil
        }
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onClickLogoutBtn(_ sender: UIButton) {
        
        showSimpleAlert()
    }
    
    @IBAction func onTapOkBtn(_ sender: UIButton) {
        let mainViewController = self.sideMenuController
        self.logoutStopVw.isHidden = true
          self.backBtn.isHidden = true
        mainViewController?.hideLeftView(animated: true, completionHandler: nil)

    }
}


extension SideMenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - UITABLEVIEW DELEGATE & DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titlesArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if IS_IPHONE {
            
            return 50
            
        } else {
            
            return 70
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuCell = menuTblVw.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        
        
        menuCell.selectionStyle = .none
        
        if indexPath.row == 1 {
            
            if let inboxStr = notificationsCount["total_notification"] as? String {
                
                if inboxStr == "0"{
                    
                    menuCell.badgeVw.badge(text: nil)
                    
                } else {
                    menuCell.badgeVw.badge(text: inboxStr)
                    
                }
            }
            
        }
        
        menuCell.menuTitleLbl.text = titlesArr[indexPath.row]
        menuCell.menuTitleImgVw.image = UIImage(named: imagesArr[indexPath.row])
        
        return menuCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            
            
            let mainViewController = self.sideMenuController
            
            if indexPath.row == 0 {
                
                NotificationCenter.default.post(name: Notification.Name(GETNEARBYSCOOTERS), object: nil)
                
                
            } else if indexPath.row == 1 {
                
                let iVc = InboxViewController.getInstance()
                self.navigationController?.pushViewController(iVc, animated: true)
                
            } else if indexPath.row == 2 {
                
                let mVc = MyProfileViewController.getInstance()
                self.navigationController?.pushViewController(mVc, animated: true)
                
            } else if indexPath.row == 3 {
                
                let MVC = MyTripsViewController.getInstance()
                self.navigationController?.pushViewController(MVC, animated: true)
                
            } else if indexPath.row == 4 {
                
                let mWVC = MyWalletViewController.getInstance()
                self.navigationController?.pushViewController(mWVC, animated: true)
                
            } else if indexPath.row == 5 {
                
                let ugvc = QuotientsSummaryViewController.getInstance()
                self.navigationController?.pushViewController(ugvc, animated: true)
                
            }
            else if indexPath.row == 6 {
                
                self.selectLanguage()
                
            }else if indexPath.row == 7 {
                
                let ugvc = UserGuideViewController.getInstance()
                self.navigationController?.pushViewController(ugvc, animated: true)
            }  else if indexPath.row == 8 {
                
                DispatchQueue.main.async {
                    let ppvc = PrivicyPolicyViewController.getInstance()
                    let navigationController = UINavigationController(rootViewController: ppvc)
                    ppvc.isFromTermsofservice = true
                    ppvc.navigationController?.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
                    
                }
                
            } else if indexPath.row == 9 {
                
                DispatchQueue.main.async {
                    let ppvc = PrivicyPolicyViewController.getInstance()
                    let navigationController = UINavigationController(rootViewController: ppvc)
                    ppvc.navigationController?.modalPresentationStyle = .fullScreen
                    ppvc.isFromPrivacyPolicy = true
                    self.present(navigationController, animated: true, completion: nil)
                }
                
                
            } else if indexPath.row == 10 {
                
                DispatchQueue.main.async {
                    let ppvc = PrivicyPolicyViewController.getInstance()
                    let navigationController = UINavigationController(rootViewController: ppvc)
                    ppvc.navigationController?.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
                }
                
            }
            
            mainViewController?.hideLeftView(animated: true, completionHandler: nil)
        }
    }
    
    func getNotificationsCountSideMenuAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(NOTIFICATIONS_COUNT)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          
                          
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Notifications count Sidemenu Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            print(err)
                            //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                DispatchQueue.main.async {
                                    
                                    self.menuTblVw.reloadData()
                                }
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Notifications count Error:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
        }
    }
    
    @objc func getlogouAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(LOGOUT_API)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          
                          
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" logout Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            self.hideActivityIndicator()
                            print(err)
                            //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if (status_code == "1") {
                                                               
                                if let error = json["message"] as? String {

                                    ERROR_MESSAGE = error

                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {

                                        let defaults = UserDefaults.standard

                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()

                                        self.resetUserDefaults()

                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()

                                    } else {

                                        let defaults = UserDefaults.standard
                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()

                                        self.resetUserDefaults()

                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                    }
                                }
                                
                            } else if (status_code == "2")  {
                                
                                self.hideActivityIndicator()
                                self.logoutStopVw.isHidden = false
                                self.backBtn.isHidden = false
                                
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("LogoutError:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
            //UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
        }
    }
    
}

