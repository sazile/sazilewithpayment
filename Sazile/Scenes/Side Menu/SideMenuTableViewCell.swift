//
//  SideMenuTableViewCell.swift
//  Sazile
//
//  Created by Vodnala Venu on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var badgeVw: UIView!
    @IBOutlet weak var menuTitleImgVw: UIImageView!
    @IBOutlet weak var menuTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
