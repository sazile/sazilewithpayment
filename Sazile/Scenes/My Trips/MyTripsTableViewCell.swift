//
//  MyTripsTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class MyTripsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var rightVw: UIView!
    @IBOutlet weak var leftVw: UIView!
    @IBOutlet weak var tableVwCellVw: UIView!
    @IBOutlet weak var roundCircleBtn: UIButton!
    @IBOutlet weak var lineVw: UIView!
    @IBOutlet weak var roundCircleBtnRight: UIButton!
    @IBOutlet weak var addressLblLeft: UILabel!
    @IBOutlet weak var addressLblRight: UILabel!
    @IBOutlet weak var dateImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var clockImg: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var distanceImg: UIImageView!
    @IBOutlet weak var remainingTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
