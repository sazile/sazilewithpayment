//
//  MyTripsViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import FittedSheets
class MyTripsViewController: UIViewController {
    
    @IBOutlet weak var noRideLbl: UILabel!
    @IBOutlet weak var prograssVw: ProgressBarView!
    @IBOutlet weak var milesLbl: UILabel!
    @IBOutlet weak var tableVwArray: UITableView!
    @IBOutlet weak var numberDaysVw: UIView!
    @IBOutlet weak var numberDaysLbl: UILabel!
    @IBOutlet weak var totalRidesLbl: UILabel!
    @IBOutlet weak var totalDaysLbl: UILabel!
    @IBOutlet weak var lastDaysLbl: UILabel!
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var recentTripsLbl: UILabel!
    @IBOutlet weak var milesVw: UIView!
    
    @IBOutlet weak var totalRideLbl: UILabel!
    @IBOutlet weak var lastThirtyDaysLbl: UILabel!
    @IBOutlet weak var mileLbl: UILabel!
    
    @IBOutlet weak var daysLbl: UILabel!
    
    var rideHistory : [[String: Any]] = []
    var tokenKey = String()
    var userId = String()
    var addressArr = [String]()
    var dateInt = NSInteger()
    var rideDeductCard = String()
    var floatstr = String()
    var rideId = String()
    var rideHistrory : [String : Any] = [:]

    var value:Float = 25/30
    var profr = NSInteger()
    var dateDict : NSDictionary = [:]
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVwArray.separatorStyle = .none
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "my_trips", value: ""))
        self.totalRideLbl.text = (L102Language.AMLocalizedString(key: "total_rides", value: ""))
        self.daysLbl.text = (L102Language.AMLocalizedString(key: "total_days_active", value: ""))
        self.lastThirtyDaysLbl.text = (L102Language.AMLocalizedString(key: "thirty_days", value: ""))
        self.recentTripsLbl.text = (L102Language.AMLocalizedString(key: "recent_trips", value: ""))
        self.viewAllBtn.setTitle(L102Language.AMLocalizedString(key: "vieww_all", value: ""), for: .normal)
      //  self.mileLbl.text = (L102Language.AMLocalizedString(key: "miles", value: ""))

        prograssVw.layer.cornerRadius = prograssVw.frame.size.height / 2
        prograssVw.clipsToBounds = true
        
        milesVw.layer.borderWidth = 1
        milesVw.layer.borderColor = UIColor.white.cgColor
        milesVw.layer.cornerRadius = milesVw.frame.size.height / 2
        milesVw.clipsToBounds = true
        
        milesVw.layer.borderColor = UIColor.white.cgColor
        milesVw.layer.cornerRadius = milesVw.frame.size.height / 2
        milesVw.clipsToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noRideLbl.isHidden = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let rideHistroryData = data as! NSDictionary
            
            tokenKey = rideHistroryData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = rideHistroryData["user_id"] as? String ?? ""
            print(userId)
            getRideHistory()
        }
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    static func getInstance() -> MyTripsViewController {
        
        let storyboard = UIStoryboard(name: "MyTrips", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyTripsViewController") as! MyTripsViewController
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onDidTapclickedBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let rhvw = RideHistoryViewController.getInstance()
            self.navigationController?.pushViewController(rhvw, animated: true)
        }
    }
}

//MARK: - UITABLEVIEW DELEGATE & DATASOURCE
extension MyTripsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rideHistory.count
        
    }
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTripsTableViewCell") as! MyTripsTableViewCell
        
        cell.addressLblLeft.text = String(format: "%@", rideHistory[indexPath.row]["ride_start_location"] as? String ?? "")
        cell.addressLblRight.text = String(format: "%@",  rideHistory[indexPath.row]["ride_end_location"] as? String ?? "")
//        cell.distanceLbl.text = String(format: "Distance: %.3f KM", rideHistory[indexPath.row] ["ride_distance"] as! Float)
        cell.timeLbl.text = String(format: "%@", rideHistory[indexPath.row] ["ride_time"]as? String ?? "")
        let dateStr = rideHistory[indexPath.row]["ride_start_time"]as? String ?? ""
        rideDeductCard = rideHistory[indexPath.row]["ride_payment_type"] as? String ?? ""
        
        if let totalRides = rideHistory[indexPath.row]["ride_distance"] as? Double {
            
            let num = NSNumber(value: totalRides)
            let formatter : NumberFormatter = NumberFormatter()
            formatter.numberStyle = .decimal
            let str = formatter.string(from: num)!
            print(str)
            //self.floatstr = totalRides
            cell.distanceLbl.text = String(format: "Distance: %@ KM", str as? String ?? "")
            
            
        }
        
        
        
      //  cell.distanceLbl.text = String(format: "Distance: %.f KM", rideHistory[indexPath.row] ["ride_distance"] as! String)


        let strArr = dateStr.components(separatedBy: " ")
        let date1 = strArr[0]
        cell.dateLbl.text = date1
        cell.selectionStyle = .none
        
        if rideDeductCard == "wallet" {
            
            cell.remainingTimeLbl.text = String(format: "%@min", rideHistory[indexPath.row] ["debit_minutes"]as? String ?? "")

        } else if rideDeductCard == "card" {
            
            cell.remainingTimeLbl.text = String(format: "%@", rideHistory[indexPath.row] ["ride_cost"]as? String ?? "")

            
        }
        
        cell.leftVw.layer.cornerRadius = cell.leftVw.frame.size.height/2
        cell.leftVw.clipsToBounds = true
        cell.rightVw.layer.cornerRadius = cell.rightVw.frame.size.height/2
        cell.rightVw.clipsToBounds = true
        
        cell.tableVwCellVw.layer.cornerRadius = 5
        cell.tableVwCellVw.clipsToBounds = true
        
        cell.tableVwCellVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.tableVwCellVw.layer.shadowRadius = 1
        cell.tableVwCellVw.layer.shadowOffset = CGSize.zero
        
        cell.tableVwCellVw.layer.shadowOpacity = 1
        cell.tableVwCellVw.clipsToBounds = false
        return cell
    }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
            rideId = rideHistory[indexPath.row]["ride_id"] as? String ?? ""
                getRideDetails()
//            let riVC = RideDetailsViewController.getInstance()
//            let navigationController = UINavigationController(rootViewController: riVC)
//            riVC.rideId = rideHistory[indexPath.row]["ride_id"] as? String ?? ""
//            navigationController.modalPresentationStyle = .fullScreen
//            self.present(navigationController, animated: true, completion: nil)
        }
    
}

//MARK: - MY TRIPS API
extension MyTripsViewController {
    
    func getRideHistory() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(MYTRIPS)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" RideHistory Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.numberDaysLbl.text = String(format: "%@ %@", json["total_days_active"]as? String ?? "",(L102Language.AMLocalizedString(key: "days", value: "")))
                                
                                let key = json["total_days_active"] as? String ?? ""
                                let myInt = Int(key) ?? 0
                                let dbl = Double(myInt)
                                let value = dbl/30.0
                                self.prograssVw.progress = Float(value)
                                
                            } else {
                                self.noRideLbl.isHidden = false
                                self.noRideLbl.text = (L102Language.AMLocalizedString(key: "no_ride_found", value: ""))
                                
                            }
                            
                            if let totalRides = json["total_ride"] as? Double {
                                
                                let string = String(totalRides)

                               // self.milesLbl.text = String(totalRides)
                                self.milesLbl.text = String(format: "%@ %@", string,(L102Language.AMLocalizedString(key: "miles", value: "")))
                            }
                          
                            if let data = json["data"] as? [[String : Any]] {
                                self.rideHistory = data
                                
                                print(self.rideHistory)
                                
                                DispatchQueue.main.async {
                                    self.tableVwArray.delegate = self
                                    self.tableVwArray.dataSource = self
                                    self.tableVwArray.reloadData()
                                }
                              
                                
                            } else
                                
                            {

                                UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                        
                    }
                case .failure(let err):
                    
                    print("rideHistory Error:\(err.localizedDescription)")
                    
                }
            }
        }else{

             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
        func getRideDetails() {
            
            if Reachability.isNetworkAvailable(){
                
                let urlString = "\(BASEURL)\(RIDEHISTRORY_DETAILS)"
                
                let params = [
                    "user_id": userId,
                    "token_key": tokenKey,
                    "ride_id" : rideId
                ]
                
                #if DEBUG
                
                print("BEGIN SAZILE======>URLSTRING" + urlString)
                print("BEGIN SAZILE=====Parameters>\(params)")
                
                #endif
                
                self.showActivityIndicator()
                
                ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                    for (key,values) in params{
                        MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                    
                    switch result{
                        
                    case .success(let upload, _,_):
                        
                        upload.responseJSON { response in
                            
                            #if DEBUG
                            
                            print(" RideDetails Response : \(response)")
                            
                            #endif
                            
                            if let err = response.error {
                                
                                self.hideActivityIndicator()
                                print(err)
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                                
                                return
                            }
                            
                            if let result = response.result.value {
                                
                                let json = result as! NSDictionary
                                
                                let status_code : String = json["success"] as? String ?? ""
                                
                                if(status_code == "1") {
                                    isFromDismissRideDetailsVwBool = true

                                    if let data = json ["data"] as? [String : Any] {
                                        
                                        print(self.rideHistrory)
                                        
                                        self.rideHistrory = data
                                        
                                        print("RIDEDETAILSDICT: =====>\(self.rideHistrory)")
                                        UserDefaults.standard.set(self.rideHistrory, forKey: "RIDE_DETAILS")
                                        print("sample btn dasmnsamcac")
                                        let controller = SheetViewController(controller: UIStoryboard(name: "RideDetails", bundle: nil).instantiateViewController(withIdentifier: "RideDetailsViewController"), sizes: [.halfScreen, .fullScreen])
                                        self.present(controller, animated: false, completion: nil)
                                        
                                        
                                    }
                                    
                                } else {
                                   // isFromDismissRideDetailsVwBool = true

                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else {
                                            
                                            UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                        }
                                    }
                                }
                                
                                self.hideActivityIndicator()
                            }
                        }
                        
                    case .failure(let err):
                        
                        print("RideDetails Error:\(err.localizedDescription)")
                    }
                }
            }else{
                
                 UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
    //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
            }
        }
}


