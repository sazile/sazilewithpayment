//
//  LoginViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 24/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import ADCountryPicker
import FirebaseAuth

class LoginViewController: UIViewController {
    
    //MARK:-  OUTLETS
    @IBOutlet var andLbl: UILabel!
    @IBOutlet weak var forgotPasswordbtn: UIButton!
    @IBOutlet weak var iAgreeLbl: UILabel!
    @IBOutlet weak var signInContinueLbl: UILabel!
    @IBOutlet weak var loginTitleLbl: UILabel!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var openCountryCodeBtn: UIButton!
    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var selectCountryBtn: UIButton!
    @IBOutlet weak var checkBoxImgVw: UIImageView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var termsOfServiceBtn: UIButton!
    @IBOutlet weak var privacyPolicyBtn: UIButton!
    @IBOutlet weak var userAgreementBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var dropDownImgVw: UIImageView!
    @IBOutlet weak var imgSecureEye: UIImageView!

    var dailingCode = String()
    var ischeckbox = Bool()
    var dataDict : [String:Any] = [:]
    
    var mobileNumberWithCountryCodeStr = String()
    var VERIFICATIONID = String()
    var userId = String()
    var FCMTOKEN = String()
    var isRegisteredSuccessfully = Bool()
    var massageID = Int()
    var massageID1 = Int()

    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        isCountrySelectedForLogIn = false
        ischeckbox = false
        
        userAgreementBtn.setTitle(L102Language.AMLocalizedString(key: "user3", value: ""), for: .normal)
        privacyPolicyBtn.setTitle(L102Language.AMLocalizedString(key: "privacy2", value: ""), for: .normal)
        termsOfServiceBtn.setTitle(L102Language.AMLocalizedString(key: "terms_of_service", value: ""), for: .normal)
        loginBtn.setTitle(L102Language.AMLocalizedString(key: "login_simple", value: ""), for: .normal)
        self.forgotPasswordbtn.setTitle(L102Language.AMLocalizedString(key: "forgot_password", value: ""), for: .normal)
        self.loginTitleLbl.text = (L102Language.AMLocalizedString(key: "login_simple", value: ""))
        self.signInContinueLbl.text = (L102Language.AMLocalizedString(key: "sign_in", value: ""))
        self.mobileNumberTF.placeholder = (L102Language.AMLocalizedString(key: "mobile_number", value: ""))
        self.passwordTF.placeholder = (L102Language.AMLocalizedString(key: "passwordd", value: ""))
        self.iAgreeLbl.text = (L102Language.AMLocalizedString(key: "continuing_means_you_ve_read_and_agreed_to", value: ""))
        self.andLbl.text = (L102Language.AMLocalizedString(key: "and", value: ""))
        
        layoutDesign()
        mobileNumberTF.delegate = self
        
        isFromLoginToForgot = false

        self.mobileNumberTF.text = ""
        self.passwordTF.text = ""
        self.checkBoxImgVw.image = UIImage(named: "box-shape")
        
        if isCountrySelectedForLogIn {
            isCountrySelectedForLogIn = false
        } else {
            countryTF.text = "YT +262"
            countryImg.image = UIImage(named: "YT")
            dailingCode = "+262"
        }

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if (UserDefaults.standard.value(forKey: "CurrentLanguage") as? String) != nil {
            
            let currentLang : String = UserDefaults.standard.value(forKey: "CurrentLanguage") as? String ?? ""
            print("Current Language : \(currentLang)")
            
            if currentLang == "en" {
                
                termsOfServiceBtn.titleLabel?.font = termsOfServiceBtn.titleLabel?.font.withSize(13)
                privacyPolicyBtn.titleLabel?.font = privacyPolicyBtn.titleLabel?.font.withSize(13)
                
            } else {
                
                termsOfServiceBtn.titleLabel?.font = termsOfServiceBtn.titleLabel?.font.withSize(9)
                privacyPolicyBtn.titleLabel?.font = privacyPolicyBtn.titleLabel?.font.withSize(9)
            }
        }
        
        if isFromLoginToForgot == true {
            
            self.mobileNumberTF.text = ""
            self.passwordTF.text = ""
            self.checkBoxImgVw.image = UIImage(named: "box-shape")
            
            if isCountrySelectedForLogIn {
                
                isCountrySelectedForLogIn = false
                
            } else {
                countryTF.text = "YT +262"
                
                countryImg.image = UIImage(named: "YT")
                dailingCode = "+262"
            }
        }

        let fcmToken : String = UserDefaults.standard.value(forKey: "fcm_token") as? String ?? ""

        if fcmToken.isEmpty {
            
            FCMTOKEN = ""
            
        } else {
            
            FCMTOKEN = fcmToken
        }
    }
    
    //MARK: - BACK ACTION
    @objc func back(sender: UIBarButtonItem) {
        
        if isRegisteredSuccessfully {
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
            
        } else {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    static func getInstance() -> LoginViewController {
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    }
    
    // MARK: - LAYOUT DESIGN
    func layoutDesign() {
        
        let imgVw = UIImage(named: "sazileLogo")
        let navLogo = UIImageView(image:imgVw)
        self.navigationItem.titleView = navLogo
        
        loginBtn.layer.shadowColor = UIColor.lightGray.cgColor
        loginBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        loginBtn.layer.shadowOpacity = 3.0
        loginBtn.layer.shadowRadius = 0.0
        loginBtn.layer.masksToBounds = false
        loginBtn.layer.cornerRadius = loginBtn.frame.size.height / 2
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onTapForgotPasswordBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let fpVC = ForgotPasswordViewController.getInstance()
            isFromLoginToForgot = true
            isCountrySelectedForLogIn = false 
            self.navigationController?.pushViewController(fpVC, animated: true)
        }
    }
    
    @IBAction func onTapSelectCountryBtn(_ sender: UIButton) {
        let picker = ADCountryPicker(style: .grouped)
        picker.searchBarBackgroundColor = UIColor.white
        picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = (L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        isFromLoginToForgot = false
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
    }
    
    @IBAction func onTapCheckBoxBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            if self.ischeckbox {
                self.ischeckbox = false
                self.checkBoxImgVw.image = UIImage(named: "box-shape")
        }else{
                self.ischeckbox = true
                self.checkBoxImgVw.image = UIImage(named: "checkbox-tick")
        }
    }
}
    
    @IBAction func onTapTermsOfServiceBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isCountrySelectedForLogIn = true
            let ppvc = PrivicyPolicyViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            ppvc.navigationController?.modalPresentationStyle = .fullScreen
            ppvc.isFromTermsofservice = true
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapPrivacyPolicyBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isCountrySelectedForLogIn = true
            let ppvc = PrivicyPolicyViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            ppvc.navigationController?.modalPresentationStyle = .fullScreen
            ppvc.isFromPrivacyPolicy = true
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapUserAgreementBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let ppvc = PrivicyPolicyViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            ppvc.navigationController?.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        
    }
    @IBAction func onTapLoginBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.hideKeyBoard()
            self.mobileNumberTF.text = self.mobileNumberTF.text?.trimmingCharacters(in: .whitespaces)
            self.passwordTF.text = self.passwordTF.text?.trimmingCharacters(in: .whitespaces)
            
            if self.countryTF.hasText {
                
                if self.mobileNumberTF.hasText {
                    
                    if self.passwordTF.hasText {
                        
                        if self.ischeckbox {
                            
                            self.userLoginAPI()
                            
                        } else {
                            
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "you_must_conditions", value: "")))
                        }
                        
                    } else {
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "password", value: "")))
                    }
                    
                } else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_phone_no", value: "")))
                }
                
            } else {
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "", value: "")))//"Please select country")
            }
        }
        
    }
    
    @IBAction func onTapSecureUnsecure(_ sender: UIButton) {
        sender.tintColor = .clear
        if sender.isSelected == false{
            imgSecureEye.image = #imageLiteral(resourceName: "eyeUnsecure")
            sender.isSelected = true
            passwordTF.isSecureTextEntry = false
        }else{
            imgSecureEye.image = #imageLiteral(resourceName: "eyeSecure")
            sender.isSelected = false
            passwordTF.isSecureTextEntry = true
        }

    }
    
}

//MARK: - COUNTRYPICKER DELEGATE
extension LoginViewController: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        countryTF.text = String(format: "%@ %@", code,dialCode)
        
        countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode
        
        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")
        
        isCountrySelectedForLogIn = true
        
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - LoginAPI
extension LoginViewController {
    
    func userLoginAPI(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(LOGIN)"
            
            let params = [ "mobile_number": mobileNumberTF.text!,
                           "password": passwordTF.text!,
                           "user_agreement": "true",
                           "dailing_code": dailingCode,
                           "fcm_id" : FCMTOKEN
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result {
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" LogIn Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json as? [String : Any] {
                                    
                                    self.dataDict = (data["data"] as! NSDictionary) as! [String : Any]
                                    print(self.dataDict)
                                   
                                    UserDefaults.standard.set(self.dataDict, forKey: "LOGIN_RESPONSE")
                                    
                                    if let phoneVerifiedStr = self.dataDict["phone_verified"] as? String {
                                        
                                        let phoneVerifiedInt = Int(phoneVerifiedStr)
                                        
                                        if phoneVerifiedInt == 1 {
                                            
                                            DispatchQueue.main.async {
                                                
                                                UserDefaults.standard.set(true, forKey: "IS_LOGGED_IN")
                                                UserDefaults.standard.synchronize()
                                                
                                                let hvc = MainViewController.getInstance()
                                                hvc.setup(type: 1)
                                                self.navigationController?.pushViewController(hvc, animated: true)
                                            }
                                            
                                        } else {
                                            
                                            self.mobileNumberAuthenticationWithFirebase()
                                        }
                                    }
                                }
                                
                            } else {
                                
                                self.hideActivityIndicator()
                                self.massageID = json["message_id"] as? Int ?? 0
                                self.massageID1 = json["message_id"] as? Int ?? 0
                                print("MESSAGE ID : =====> \(self.massageID)")
                                if self.massageID == 23 {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "mobile_number_incorrect", value: "")) , buttonTitle: "OK") {
                                        
                                        
                                    }
                                    
                                } else if self.massageID1 == 19 {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "your_account_deactvited_by_admin", value: "")), buttonTitle: "OK") {
                                        
                                      }
                                    
                                } else {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: json["message"] as? String ?? "", buttonTitle: "OK") {
                                        
                                    }
                                    
                                }
                                    
                                
                                //                                UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "please_confirm_mail", value: "")))
                                //
                                UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                            }
                            
                            //self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Login Error:\(err.localizedDescription)")
                }
            }
            
        } else {
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    func mobileNumberAuthenticationWithFirebase() {
        Auth.auth().languageCode = "en"
        mobileNumberWithCountryCodeStr = "\(dailingCode) " + "\(self.mobileNumberTF.text!)"
           
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumberWithCountryCodeStr, uiDelegate: nil) { (verificationID, error) in
            
            if (error != nil) {
                
                self.hideActivityIndicator()
                
                ERROR_MESSAGE = error?.localizedDescription ?? ""
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                
                return
                
            } else {
                
                if verificationID != nil {
                    
                    self.VERIFICATIONID = verificationID ?? ""
                    print("Verification Id From Firebase : \(self.VERIFICATIONID)")
                    
                    self.hideActivityIndicator()
                    
                    DispatchQueue.main.async {
                        
                        let oVc = OTPViewController.getInstance()
                        oVc.firVerificationId = self.VERIFICATIONID
                        oVc.mobileNumberStr = self.mobileNumberWithCountryCodeStr
                        self.navigationController?.pushViewController(oVc, animated: true)
                    }
                }
            }
        }
    }
    
}

//MARK: - UITEXTFIELD DELEGATE

extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                
                return false
        }
        
        if mobileNumberTF.text?.count == 0 && string == "0" {

            return false
        }


        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 15
    }
}
