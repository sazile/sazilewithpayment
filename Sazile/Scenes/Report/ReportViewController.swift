//
//  ReportViewController.swift
//  Sazile
//
//  Created by harjitsingh on 23/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos

class ReportViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableVwHeightLayout: NSLayoutConstraint!
    var scooterListArr : [[String: Any]] = []
    var scooterNumStr = String()
    
    var reportdict : [String: Any] = [:]
    var userId = String()
    var tokenKey = String()
    var deviceId = String()
    var reportimgData = NSData()
    var reportType = String()
    var chargingPrbmBool = Bool()
    var scooterPrbmBool = Bool()
    var bikePrbmBool = Bool()
    var parkingPrbmBool = Bool()
    var idStr = String()
    var massageID = Int()
    var array = [String]()
    var maxHeight: CGFloat = UIScreen.main.bounds.size.height

    
    //MARK: -  OUTLETS
    @IBOutlet weak var selectScooterLbl: UILabel!
    @IBOutlet weak var scooterNumbersVw: UIView!
    @IBOutlet weak var scooterNumberTblVw: UITableView!
    @IBOutlet weak var selectScootersLbl: UILabel!
    @IBOutlet weak var reportmissingLbl: UILabel!
    
    @IBOutlet weak var imgLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var messageTextVw: UITextView!
    @IBOutlet weak var pathImgVw: UILabel!
    @IBOutlet weak var imageUploadVw: UIView!
    @IBOutlet weak var textVw: UITextView!
    @IBOutlet weak var reportPrbmsVw: UIView!
    @IBOutlet weak var reportMissingVw: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var parkingPrbm: UIButton!
    @IBOutlet weak var scooterPrbmbtn: UIButton!
    @IBOutlet weak var bikePbrmBtn: UIButton!
    @IBOutlet weak var chargingPrbmBtn: UIButton!
    
    @IBOutlet weak var prbmImgVw: UIImageView!
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        scooterNumberTblVw.estimatedRowHeight = 44

        scooterNumberTblVw.separatorStyle = .none
        
        array = ["1","2","3","4","5","6"]
       
        self.bikePbrmBtn.setTitle((L102Language.AMLocalizedString(key: "scooter_problem", value: "")), for: .normal)
        self.parkingPrbm.setTitle((L102Language.AMLocalizedString(key: "another_problem", value: "")), for: .normal)
        self.chargingPrbmBtn.setTitle((L102Language.AMLocalizedString(key: "charging_issues", value: "")), for: .normal)
        self.scooterPrbmbtn.setTitle((L102Language.AMLocalizedString(key: "parking_problems", value: "")), for: .normal)
        self.reportmissingLbl.text = (L102Language.AMLocalizedString(key: "report_missingg", value: ""))
        self.reportMissingVw.isHidden = true
        self.uploadBtn.setTitle((L102Language.AMLocalizedString(key: "upload", value: "")), for: .normal)
        self.submitBtn.setTitle((L102Language.AMLocalizedString(key: "submitt", value: "")), for: .normal)
        self.selectScooterLbl.text = (L102Language.AMLocalizedString(key: "select_scooter", value: ""))
        self.selectScootersLbl.text = (L102Language.AMLocalizedString(key: "select_scooter", value: ""))
        
        textVw.layer.borderWidth = 1
        textVw.layer.borderColor = UIColor.gray.cgColor
        textVw.layer.cornerRadius = 2
        textVw.clipsToBounds = true
        
        imageUploadVw.layer.borderWidth = 1
        imageUploadVw.layer.borderColor = UIColor.gray.cgColor
        imageUploadVw.layer.cornerRadius = 2
        imageUploadVw.clipsToBounds = true
        
        reportPrbmsVw.layer.cornerRadius = 5
        reportPrbmsVw.clipsToBounds = true
        
        reportMissingVw.layer.cornerRadius = 5
        reportMissingVw.clipsToBounds = true
        
        uploadBtn.layer.shadowColor = UIColor.lightGray.cgColor
        uploadBtn.layer.shadowOffset = CGSize(width:0.0, height: 1.5 )
        uploadBtn.layer.shadowOpacity = 3.0
        uploadBtn.layer.shadowRadius = 0.0
        uploadBtn.layer.masksToBounds = false
        uploadBtn.layer.cornerRadius = uploadBtn.frame.size.height / 2
        
        submitBtn.layer.shadowColor = UIColor.lightGray.cgColor
        submitBtn.layer.shadowOffset = CGSize(width:0.0, height: 2.5 )
        submitBtn.layer.shadowOpacity = 3.0
        submitBtn.layer.shadowRadius = 0.0
        submitBtn.layer.masksToBounds = false
        submitBtn.layer.cornerRadius = submitBtn.frame.size.height / 2
        
        chargingPrbmBtn.layer.cornerRadius = chargingPrbmBtn.frame.size.height/2
        chargingPrbmBtn.clipsToBounds = true
        
        bikePbrmBtn.layer.cornerRadius = bikePbrmBtn.frame.size.height/2
        bikePbrmBtn.clipsToBounds = true
        
        scooterPrbmbtn.layer.cornerRadius = scooterPrbmbtn.frame.size.height/2
        scooterPrbmbtn.clipsToBounds = true
        
        parkingPrbm.layer.cornerRadius = parkingPrbm.frame.size.height/2
        parkingPrbm.clipsToBounds = true
        
        scooterNumbersVw.layer.cornerRadius = 10
        scooterNumbersVw.clipsToBounds = true
        scooterNumbersVw.layer.shadowOffset = CGSize(width:2, height:2 )
        scooterNumbersVw.layer.shadowColor = UIColor.black.cgColor
        scooterNumbersVw.layer.shadowOpacity = 2
        scooterNumbersVw.layer.shadowOffset = CGSize.zero
        scooterNumbersVw.layer.shadowRadius = 1
        scooterNumbersVw.clipsToBounds = false
        // dynamicLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        print("Device ID : \(deviceId)")
        
     
        self.backBtn.isHidden = false
        self.scooterNumbersVw.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            let reportData = data as! NSDictionary
            tokenKey = reportData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = reportData["user_id"] as? String ?? ""
            print(userId)
            
            
            
        }
    }
    
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//
//        tableVwHeightLayout.constant = scooterNumberTblVw.contentSize.height
//    }

    
    func dynamicLayout() {
        
        if scooterListArr.count > 0 {
            
            tableVwHeightLayout.constant = 175

            self.tableVwHeightLayout.constant = self.scooterNumberTblVw.contentSize.height
          //  self.tableVwHeightLayout.constant = self.scooterNumberTblVw.constant + 170
           // self.popUpTbleVw.isScrollEnabled = false
            
            if self.tableVwHeightLayout.constant > self.view.frame.size.height {
            
             //   veDynamicTop.constant = 30
                
                self.tableVwHeightLayout.constant = self.view.frame.size.height - 150
                self.tableVwHeightLayout.constant = self.scooterNumberTblVw.contentSize.height - 100

               // self.popUpTbleVw.isScrollEnabled = true
            }
        }
        else {
            
            self.tableVwHeightLayout.constant = 0
            self.tableVwHeightLayout.constant = 160
        }
    }
    //MARK: - TABLEVIEW DELEGATE DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scooterListArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportTableViewCell") as! ReportTableViewCell
        
       cell.scooterNumbersLbl.text = String(format: "%@",scooterListArr[indexPath.row]["name"] as? String ?? "")
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        scooterNumStr = scooterListArr[indexPath.row]["name"] as? String ?? ""
        idStr = scooterListArr[indexPath.row]["id"] as? String ?? ""
        
        self.selectScooterLbl.text = scooterNumStr
        print(scooterNumStr)
        self.scooterNumbersVw.isHidden = true
        self.selectScooterLbl.isHidden = false

    
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableVwHeightLayout.constant = scooterNumberTblVw.contentSize.height
    }

    
    func sellectphoto() {
        
        let alert = UIAlertController(title: (L102Language.AMLocalizedString(key: "choose_image", value: "")), message: "", preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)

        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: UIAlertAction.Style.default, handler: {(_: UIAlertAction!) in
            
            self.openCamera()
            
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "photo_library", value: "")),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.photoLibrary()
                                        
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "cancel1", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    static func getInstance() -> ReportViewController {
        
        let storyboard = UIStoryboard(name: "Report", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
    }
    
    //MARK: - IBACTIONS
    @IBAction func onTapSelectScooterBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.scooterNumbersVw.isHidden = false
            self.selectScooterLbl.isHidden = true
        }
    }
    
    
    @IBAction func onTapUploadBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isForSelectImageReportingBool = true
            self.sellectphoto()
        }
        
    }
    
    @IBAction func onTapSubmitBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            if self.idStr.isEmpty {
            
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "select_device_id", value: "")))
            
        } else {
            
                if self.textVw.hasText {
                
                    if self.chargingPrbmBool {
                    
                        self.reportType = "charging_issue"
                    
                    } else if self.bikePrbmBool {
                    
                        self.reportType = "bike_problem"
                    
                    } else if self.scooterPrbmBool {
                    
                        self.reportType = "scooter_problem"
                    
                } else {
                    
                        self.reportType = "parking_problem"
                }
                
                self.getReportMissing()
                
            } else {
                
                    UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "enter_message", value: "")))
            }
        }
    }
}
    @IBAction func onTapCnlBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isForSelectImageReportingBool = false
            self.dismiss(animated: true, completion: nil)
            self.backBtn.isHidden = true
        }
        
    }
    
    @IBAction func onTapCancelBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isForSelectImageReportingBool = false
            self.dismiss(animated: true, completion: nil)
            self.backBtn.isHidden = true
        }
        
    }
    
    @IBAction func onTapChargingPrbmBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            self.chargingPrbmBool = true
            self.bikePrbmBool = false
            self.scooterPrbmBool = false
            self.parkingPrbmBool = false
            self.reportPrbmsVw.isHidden = true
            self.reportMissingVw.isHidden = false
            self.backBtn.isHidden = false
            self.getSelectScooterAPI()
        }
    }
    
    @IBAction func onTapBikePrbmBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            self.bikePrbmBool = true
            self.chargingPrbmBool = false
            self.scooterPrbmBool = false
            self.parkingPrbmBool = false
            self.reportPrbmsVw.isHidden = true
            self.reportMissingVw.isHidden = false
            self.backBtn.isHidden = false
            self.getSelectScooterAPI()
        }
    }
    
    @IBAction func onTapScooterPrbmBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            self.scooterPrbmBool = true
            self.chargingPrbmBool = false
            self.bikePrbmBool = false
            self.parkingPrbmBool = false
            self.reportPrbmsVw.isHidden = true
            self.reportMissingVw.isHidden = false
            self.backBtn.isHidden = false
            self.getSelectScooterAPI()
        }
    }
    
    @IBAction func onTapParkingPrbm(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.parkingPrbmBool = true
            self.chargingPrbmBool = false
            self.bikePrbmBool = false
            self.scooterPrbmBool = false
            self.reportPrbmsVw.isHidden = true
            self.reportMissingVw.isHidden = false
            self.backBtn.isHidden = false
            self.getSelectScooterAPI()
        }
    }
    
}

//MARK: - UIIMAGE CONTROLLER DELEGATE NAVIGATION CONTROLLER DELEGATE
extension ReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.modalPresentationStyle = .fullScreen
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
            
        }
    }
    
    //  Converted to Swift 5.1 by Swiftify v5.1.30744 - https://objectivec2swift.com/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
            
//            var filename = ""
//            if let url = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
//                let assets = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
//                if let firstAsset = assets.firstObject,
//                    let firstResource = PHAssetResource.assetResources(for: firstAsset).first {
//                    filename = firstResource.originalFilename
//                    print("namefafaf\(filename)")
//                    pathImgVw.text = filename
//                }
//            }
            let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            //licenseBgVw.image = chosenImage
            
            let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
            
             reportimgData = data!
             prbmImgVw.image = chosenImage
            
            picker.dismiss(animated: true)
            
        } else {
            
            print("BEGIN:Sazile ======> Something went wrong")
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

//user_id:1
//device_id:2
//missing_msg: report message
//picture: image file(optional)
//type: charging_issue / bike_problem / scooter_problem / parking_problem
//token_key:

//MARK: REPORT MISSSING API
extension ReportViewController{
    func getReportMissing() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(DEVICEMISSING_REPORT)"
            
            let params = [  "user_id": userId,
                            "token_key": tokenKey,
                            "device_id": idStr,
                            "missing_msg": messageTextVw.text ?? "",
                            "type": reportType
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                if self.reportimgData.count > 0 {
                    
                    let data : Data = self.reportimgData as Data
                    
                    MultipartFormData.append(data, withName: "picture", fileName: "image.png", mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Report Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                
                                if self.massageID == 129 {
                                    
                                    DispatchQueue.main.async {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "missing_report", value: "")), buttonTitle: "Ok") {
                                            
                                            isForSelectImageReportingBool = false
                                            self.backBtn.isHidden = true
                                            self.dismiss(animated: true, completion: nil)
                                            
                                        }
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")

                                    
                                }

                                
                            }
                            else
                            {
                                UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Report Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    //MARK: - SELECT SCOOTER API
    func getSelectScooterAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            
            let urlString = "\(BASEURL)\(DEVICE_LIST)"
            
            let params = ["user_id": userId,
                          "token_key" : tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print("Scooter Data Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                if let data = json["scooter_list"] as? [[String : Any]] {
                                    self.scooterListArr = data
                                    
                                }
                                
                                DispatchQueue.main.async {
                                    self.scooterNumberTblVw.delegate = self
                                    self.scooterNumberTblVw.dataSource = self
                                    self.scooterNumberTblVw.reloadData()
                                }
                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Scooter Data Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}




