//
//  ReportTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh Mac on 16/12/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tableviewDynamicHeight: NSLayoutConstraint!
    @IBOutlet weak var scooterNumbersLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
