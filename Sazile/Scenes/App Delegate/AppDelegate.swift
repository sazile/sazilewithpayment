//
//  AppDelegate.swift
//  Sazile
//
//  Created by Harjit Singh on 24/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import GoogleMaps
import Stripe
import Firebase
import CoreData
import UserNotifications

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var didTapNotification = String()
    var dateStr1 = String()
    var rideID = String()
    var notificationdataDict : [String: Any] = [:]
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
              BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)

        IQKeyboardManager.shared.enable = true
        
        // STRIPE KEY
        //pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk
        //sk_test_FxFxMi6vw7c3yBGWMRQn6diq00Kmt0XZZu
//"pk_live_XS8g8vKcegq4oYNTenGzVr5Z00ELfZvd6B"
        
        //GMS SERVICES
        GMSServices.provideAPIKey("AIzaSyBXF_hdwpP_JPI0KTbFgnif3SJtn7_IA-w")
        
        // GMSServices.provideAPIKey("AIzaSyB8IusgJkXg9o_IoHM_cNmA6vNi13X5IVg")
        
        if UserDefaults.standard.bool(forKey: "IS_LOGGED_IN") == true {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(rootVc, animated: true)
            let nav = UINavigationController(rootViewController: rootVc)
            nav.isNavigationBarHidden = false
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
            
        }
        
        
        let currentLang = UserDefaults.standard.value(forKey: "CurrentLanguage")
        
        if !(currentLang != nil) {
            
            L102Language.setAppleLAnguageTo(lang: "en")
            
        } else {
            
            L102Language.setAppleLAnguageTo(lang: currentLang as! String)
        }
        
        
        if UserDefaults.standard.bool(forKey: "IS_LOGGED_IN") {
            
            DispatchQueue.main.async {
                
                let hvc = MainViewController.getInstance()
                hvc.setup(type: 1)
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(hvc, animated: true)
            }
        }
        
        
        // UINavigationBar.appearance().barTintColor = .blue
        UINavigationBar.appearance().barTintColor = UIColor(red: 20.0 / 255.0, green: 40.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        registerAppForNotifications()
        FirebaseApp.configure()
        Messaging.messaging()
        // Notifications
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        getFcmToken()
        return true
    }
    
    func getFcmToken()  {
           UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                  if settings.authorizationStatus == .authorized {
                      // Already authorized
                  }
                  else {
                      // Either denied or notDetermined
                      UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                          (granted, error) in
                            // add your own
                          UNUserNotificationCenter.current().delegate = self
        
                          
                          DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5000)) {
                              InstanceID.instanceID().instanceID { (result, error) in
                              // Get called
                              if let error = error {
                                  print("Error fetching remote instance ID: \(error)")
                              } else if let result = result {
                               
                                  print("Remote instance ID token: \(result.token)")
                                  let token = Messaging.messaging().fcmToken
                                    
                                    print("FCM token: \(token ?? "")")
                                UserDefaults.standard.set(token, forKey:"fcm_token")
                                  //self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
                              }}
                          }
                      }
                  }
              }
    }
    func setUpStripe(strStripeKey:String){
        StripeAPI.defaultPublishableKey = strStripeKey
        //pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Sazile")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - FIREBASE DELEGATE METHODS
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        // If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        #if DEBUG
        print("FCM TOKEN ====> \(fcmToken)")
        #endif
        
        UserDefaults.standard.setValue(fcmToken, forKey: "fcm_token")
        UserDefaults.standard.synchronize()
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    //MARK: -  Register / UnRegister User Notifications
    
    func registerAppForNotifications() {
        
        //Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func UnRegisterAppForNotifications() {
        
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    //MARK: -  UNUserNotificationCenter Delegate
    
    // called when a notification is delivered when app is in foreground.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Will Present Notification Response : \(notification)")
        
        
        print("GOT A NOTIFICATION")
        
        //        let alertController = UIAlertController(title: "Notification", message: "" as? String, preferredStyle: .alert)
        //        //alertController.view.tintColor = .lightBlue
        //        let dismissAction = UIAlertAction(title: "Ok", style: .default)
        //        alertController.addAction(dismissAction)
        //        alertController.present(alertController, animated: true)
        
        let userInfo = notification.request.content.userInfo
        
        print("User Info : \(userInfo)")
        completionHandler([.sound, .alert, .badge])
        
        let text = userInfo["text"] as? String ?? ""
        dateStr1 = userInfo["created_at"] as? String ?? ""
        rideID = userInfo["ride_id"] as? String ?? ""
        
        //        if let data = userInfo as? [String : Any] {
        //            self.notificationdataDict = data
        //            print("NOTIFICATIONDATADICT: =====> \(notificationdataDict)")
        //
        //        }
        //        UserDefaults.standard.setValue(notificationdataDict, forKey: "NOTIFICATION_RESPONSE")
        //        UserDefaults.standard.synchronize()
        //      //  UserDefaults.standard.set(userInfo, forKey: "NOTIFICATION_RESPONSE")
        //
        //          print("DATE12345: ======> \(dateStr1)")
        
        if text == "Termes et conditions" {
            
            willPresentUserSessionExpired()
            //            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            //            appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
            
        }  else if text == "Accord de l'utilisateur" {
            
            willPresentUserSessionExpired()
            
            //            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            //            appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
            
        } else if text == "Your account has been disabled by admin" {
            
            willPresentUserSessionExpired()
            
            //            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            //            appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
            
        } else if text == "Confidentialité et politique" {
            
            willPresentUserSessionExpired()
            
            //            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            //            appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
            
            //        } else if  text == "Congratulation ! Your documents have been verified successfully." {
            //
            //
        } else if text == "Your ride has been stopped" {
            
            let pPVC = PopUpViewController.getInstance()
           // np.isFromTermsofservice = true
            pPVC.rideIdStop = self.rideID
            isForScooterStopadmin = true
            let navigationController = UINavigationController(rootViewController: pPVC)
            navigationController.modalPresentationStyle = .fullScreen
            self.window?.rootViewController?.present(navigationController, animated: true, completion: nil)

        }
        
    }
    
    // called to let your app know which action was selected by the user for a given notification.
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Did Receive Notification Response : \(response)")
        
        print("DID TAPPED ON A NOTIFICATION")
        
        let userInfo = response.notification.request.content.userInfo
        print("User Info : \(userInfo)")
        let text = userInfo["text"] as? String ?? ""
        let title = userInfo["title"] as? String ?? ""
        
        
        if text == "Termes et conditions" {
            
            didReceiveUserSessionExpired()
            
            //            let np = PushNotificationsViewController.getInstance()
            //            np.isFromTermsofservice = true
            //            (self.window?.rootViewController as? UINavigationController)?.pushViewController(np, animated: true)
        }
        else if text == "Accord de l'utilisateur" {
            
            didReceiveUserSessionExpired()
            //
            //            let np = PushNotificationsViewController.getInstance()
            //            np.userAgreement = true
            //            (self.window?.rootViewController as? UINavigationController)?.pushViewController(np, animated: true)
            //
        } else if text == "Your account has been disabled by admin"{
            
            willPresentUserSessionExpired()
            
        } else if title == "Account alert" {
            
            DispatchQueue.main.async {
                
                if UserDefaults.standard.bool(forKey: "IS_LOGGED_IN") {
                    
                    //                    let nVc = MessageViewController.getInstance()
                    //                    let main = self.window?.rootViewController as? MainViewController
                    //                    main?.setup(type: 1)
                    //                    (main?.parent as? UINavigationController)?.pushViewController(nVc, animated: true)
                    
                    let mvc = MessageViewController.getInstance()
                    (self.window?.rootViewController as? UINavigationController)?.pushViewController(mvc, animated: true)
                    
                } else {
                    
                    self.willPresentUserSessionExpired()
                    //                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    //                    appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                }
            }
            
        } else {
            
            didReceiveUserSessionExpired()
            
            //            let np = PushNotificationsViewController.getInstance()
            //            np.isFromPrivacyPolicy = true
            //            (self.window?.rootViewController as? UINavigationController)?.pushViewController(np, animated: true)
            //
            //
            //        _ = center
            //        _ = response
            //        completionHandler()
        }
        
        
        func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification notification: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
            #if DEBUG
            print("Received push notification: \(notification), identifier: \(identifier ?? "")")
            // iOS 8
            #endif
            completionHandler()
        }
        
        
        // MARK: - NOTIFICATION DELEGATES
        
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            
            let strDeviceToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
            
            Messaging.messaging().apnsToken = deviceToken
            
            #if DEBUG
            // print("DEVICE TOKEN = \(strDeviceToken)\n")
            #endif
            
            UserDefaults.standard.setValue(strDeviceToken, forKey: "device_token")
            UserDefaults.standard.synchronize()
        }
        
        
        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            
            #if DEBUG
            print("\(error)")
            #endif
        }
        
    }
    
}

extension AppDelegate {
    
    func willPresentUserSessionExpired() {
        
        let defaults = UserDefaults.standard
        
        defaults.set(false, forKey: "IS_LOGGED_IN")
        defaults.synchronize()
        
        resetUserDefaults()
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
    }
    
    
    func didReceiveUserSessionExpired() {
        
        let defaults = UserDefaults.standard
        
        defaults.set(false, forKey: "IS_LOGGED_IN")
        defaults.synchronize()
        
        resetUserDefaults()
    }
    
    func resetUserDefaults() {
        
        notificationTimer?.invalidate()
        notificationTimer = nil
        
        print("BEGIN:SAZILE ======> userDefaults successfully removed from the app")
        
        var langStr = String()
        
        langStr = UserDefaults.standard.value(forKey: "CurrentLanguage") as! String
        print("Language Str : \(langStr)")
        
        DispatchQueue.main.async {
            
            let instance = InstanceID.instanceID()
            instance.deleteID { (error) in
                
                if error != nil {
                    
                    ERROR_MESSAGE = error?.localizedDescription ?? ""
                    print("Instance ID Error : \(ERROR_MESSAGE)")
                    
                } else {
                    
                    print("FCM TOKEN Deleted Successfully");
                }
            }
        }
        
        let userDefaults = UserDefaults.standard
        let dict = userDefaults.dictionaryRepresentation()
        
        dict.keys.forEach { key in
            
            userDefaults.removeObject(forKey: key)
        }
        
        userDefaults .synchronize()
        
        
        // Re-saving into UserDefaults
        
        userDefaults .set(langStr, forKey: "CurrentLanguage")
        userDefaults .synchronize()
    }
}

extension AppDelegate {
    // This method handles opening custom URL schemes (e.g., "your-app://")
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        let stripeHandled = StripeAPI.handleURLCallback(with: url)
        if (stripeHandled) {
            return true
        } else {
            // This was not a Stripe url – handle the URL normally as you would
        }
        return false
    }

    // This method handles opening universal link URLs (e.g., "https://example.com/stripe_ios_callback")
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool  {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                let stripeHandled = StripeAPI.handleURLCallback(with: url)
                if (stripeHandled) {
                    return true
                } else {
                    // This was not a Stripe url – handle the URL normally as you would
                }
            }
        }
        return false
    }
}
