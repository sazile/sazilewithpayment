//
//  RideHistoryViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 01/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//user_id:1
//token_key:1xpMVOq00TOWcgwFKgZIAkGrxIK9lUzVTWxFiglqtKDvFdEKtN
//ride_id:566



import UIKit
import FittedSheets

class RideHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK: -  OUTLETS
    @IBOutlet weak var rideHstyTblVw: UITableView!
    @IBOutlet weak var noRideLbl: UILabel!
    
    var rideHistrory : [[String : Any]] = []
    var tokenKey = String()
    var userId = String()
    var isFormPagenation = Bool()
    var ispagesAvailable = Bool()
    var page = NSInteger()
    var totalPages = NSInteger()
    var rideDeductCard = String()
    var rideId = String()
    var rideDetails : [String : Any] = [:]

    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rideHstyTblVw.separatorStyle = .none
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "ride_history", value: ""))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noRideLbl.isHidden = true
        page = 1
        isFormPagenation = false
        ispagesAvailable = true
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let rideHistroryData = data as! NSDictionary
            
            tokenKey = rideHistroryData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = rideHistroryData["user_id"] as? String ?? ""
            print(userId)
            getRideHistory()
        }
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    static func getInstance() -> RideHistoryViewController {
        
        let storyboard = UIStoryboard(name: "RideHistory", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RideHistoryViewController") as! RideHistoryViewController
    }
    
    //MARK: - UITABLEVIEW DELEGATE & DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rideHistrory.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
        
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideHistoryTableViewCell") as! RideHistoryTableViewCell
        
        cell.addressLeftLbl.text = String(format: "%@", rideHistrory[indexPath.row]["ride_start_location"] as? String ?? "")
        
        cell.addressRightLbl.text = String(format: "%@ ",  rideHistrory[indexPath.row]["ride_end_location"] as? String ?? "")
        
        cell.minsLbl.text = String(format: "%@", rideHistrory[indexPath.row] ["ride_time"]as? String ?? "")
         cell.distanceKmLbl.text = String(format: "Distance:%@ KM", rideHistrory[indexPath.row] ["ride_distance"] as? String ?? "")
       // cell.remainingLbl.text = String(format: "%@", rideHistrory[indexPath.row] ["ride_time"]as? String ?? "")
        rideDeductCard = rideHistrory[indexPath.row]["ride_payment_type"] as? String ?? ""
        
        let dateStr = rideHistrory[indexPath.row]["ride_end_time"]as? String ?? ""
        let strArr = dateStr.components(separatedBy: " ")
        let date1 = strArr[0]
        //        let date2 = strArr[1]
        //        let date3 = strArr[2]
        cell.dateLbl.text = date1
        
        if rideDeductCard == "wallet" {
            
            cell.remainingLbl.text = String(format: "%@min", rideHistrory[indexPath.row] ["debit_minutes"]as? String ?? "")
            
        } else if rideDeductCard == "card" {
            
            cell.remainingLbl.text = String(format: "%@", rideHistrory[indexPath.row] ["ride_cost"]as? String ?? "")
            
            
        }
        
        cell.selectionStyle = .none
        cell.leftVw.layer.cornerRadius = cell.leftVw.frame.size.height/2
        cell.leftVw.clipsToBounds = true
        
        cell.rightVw.layer.cornerRadius = cell.rightVw.frame.size.height/2
        cell.rightVw.clipsToBounds = true
        
        cell.histroryVw.layer.cornerRadius = 5
        cell.histroryVw.clipsToBounds = true
        
        cell.histroryVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.histroryVw.layer.shadowOpacity = 1
        cell.histroryVw.layer.shadowOffset = CGSize.zero
        cell.histroryVw.layer.shadowRadius = 1
        cell.histroryVw.clipsToBounds = false
        
        return cell
    }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            rideId = rideHistrory[indexPath.row]["ride_id"] as? String ?? ""
            self.getRideDetails()

    //        let riVC = RideDetailsViewController.getInstance()
    //        let navigationController = UINavigationController(rootViewController: riVC)
    //        riVC.rideId = rideHistrory[indexPath.row]["ride_id"] as? String ?? ""
    //        navigationController.modalPresentationStyle = .fullScreen
    //        self.present(navigationController, animated: true, completion: nil)
        }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == rideHistrory.count - 1 {
            
            if totalPages == 1 {
                
                ispagesAvailable = false
            }
            
            if ispagesAvailable == true {
                
                page = page + 1
                isFormPagenation = true
                
                
                if totalPages == page {
                    
                    ispagesAvailable = false
                    
                }
                
                getRideHistory()
            }
        }
        
    }
}

//MARK: - RIDE HISTORY API
extension RideHistoryViewController {
    
    func getRideHistory() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(RIDE_HISTORY_BYUSER)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "limit": String(format: "%ld", Int(10)),
                "page": String(format: "%ld", Int(page))
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Ride History Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                let pages = json["total_page"] as? String ?? ""
                                
                                self.totalPages = NSInteger(pages) ?? 0
                                
                                if self.isFormPagenation {
                                    
                                    if let data = json["data"] as? [[String : Any]] {
                                        
                                        self.rideHistrory.append(contentsOf: data)
                                        
                                        DispatchQueue.main.async {
                                            
                                            self.rideHstyTblVw.reloadData()
                                        }
                                    }
                                    
                                } else {
                                    
                                    if let data = json ["data"] as? [[String : Any]] {
                                        self.rideHistrory = data
                                        print(self.rideHistrory)
                                        DispatchQueue.main.async {
                                            self.rideHstyTblVw.delegate = self
                                            self.rideHstyTblVw.dataSource = self
                                            self.rideHstyTblVw.reloadData()
                                            
                                        }
                                    }
                                    
                                }
                                
                            } else {
                                
                                

                                if !self.isFormPagenation {
                                    
                                }
                                
                                self.ispagesAvailable = false
                                self.page = self.page - 1
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        self.noRideLbl.isHidden = false
                                        self.noRideLbl.text = (L102Language.AMLocalizedString(key: "no_ride_found", value: ""))
                                      //  UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("rideHistory Error:\(err.localizedDescription)")
                }
            }
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
        func getRideDetails() {
            
            if Reachability.isNetworkAvailable(){
                
                let urlString = "\(BASEURL)\(RIDEHISTRORY_DETAILS)"
                
                let params = [
                    "user_id": userId,
                    "token_key": tokenKey,
                    "ride_id" : rideId
                ]
                
                #if DEBUG
                
                print("BEGIN SAZILE======>URLSTRING" + urlString)
                print("BEGIN SAZILE=====Parameters>\(params)")
                
                #endif
                
                self.showActivityIndicator()
                
                ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                    for (key,values) in params{
                        MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                    
                    switch result{
                        
                    case .success(let upload, _,_):
                        
                        upload.responseJSON { response in
                            
                            #if DEBUG
                            
                            print(" RideDetails Response : \(response)")
                            
                            #endif
                            
                            if let err = response.error {
                                
                                self.hideActivityIndicator()
                                print(err)
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                                
                                return
                            }
                            
                            if let result = response.result.value {
                                
                                let json = result as! NSDictionary
                                
                                let status_code : String = json["success"] as? String ?? ""
                                
                                if(status_code == "1") {
                                    isFromDismissRideDetailsVwBool = true

                                    if let data = json ["data"] as? [String : Any] {
                                        
                                        self.rideDetails = data
                                         print(self.rideDetails)
                                        print("RIDEDETAILSDICT: =====>\(self.rideDetails)")
                                        UserDefaults.standard.set(self.rideDetails, forKey: "RIDE_DETAILS")
                                        print("sample btn dasmnsamcac")
                                        let controller = SheetViewController(controller: UIStoryboard(name: "RideDetails", bundle: nil).instantiateViewController(withIdentifier: "RideDetailsViewController"), sizes: [.halfScreen, .fullScreen])
                                        self.present(controller, animated: false, completion: nil)
                                    }
                                    
                                } else {
                                   // isFromDismissRideDetailsVwBool = true

                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else {
                                            
                                            UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                        }
                                    }
                                }
                                
                                self.hideActivityIndicator()
                            }
                        }
                        
                    case .failure(let err):
                        
                        print("RideDetails Error:\(err.localizedDescription)")
                    }
                }
            }else{
                
                 UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
    //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
            }
        }
}

