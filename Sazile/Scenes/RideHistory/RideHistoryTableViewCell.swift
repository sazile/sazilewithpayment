//
//  RideHistoryTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 01/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class RideHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var remainingLbl: UILabel!
    @IBOutlet weak var distanceKmLbl: UILabel!
    @IBOutlet weak var distanceImg: UIImageView!
    @IBOutlet weak var minsLbl: UILabel!
    @IBOutlet weak var clockImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateImg: UIImageView!
    @IBOutlet weak var addressRightLbl: UILabel!
    @IBOutlet weak var addressLeftLbl: UILabel!
    @IBOutlet weak var rightVw: UIView!
    @IBOutlet weak var leftVw: UIView!
    @IBOutlet weak var histroryVw: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
