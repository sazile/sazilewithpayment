//
//  PushNotificationsViewController.swift
//  Sazile
//
//  Created by Harjit Singh Mac on 25/12/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class PushNotificationsViewController: UIViewController {
    var isFromPrivacyPolicy = Bool()
    var isFromTermsofservice = Bool()
    var userAgreement = Bool()
    
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var pushNotificationsWebVw: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        acceptBtn.layer.shadowColor = UIColor.lightGray.cgColor
        acceptBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        acceptBtn.layer.shadowOpacity = 3.0
        acceptBtn.layer.shadowRadius = 0.0
        acceptBtn.layer.masksToBounds = false
        acceptBtn.layer.cornerRadius = acceptBtn.frame.size.height / 2
        self.acceptBtn.setTitle((L102Language.AMLocalizedString(key: "okay", value: "")), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpUI()
    }
    
    static func getInstance() -> PushNotificationsViewController {
        
        let storyboard = UIStoryboard(name: "PushNotifications", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PushNotificationsViewController") as! PushNotificationsViewController
    }
    
    @IBAction func onTapAcceptBtn(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()

        //self.navigationController?.popViewController(animated: true)
        //self.navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        
        if pushNotificationsWebVw.isLoading {
            
            pushNotificationsWebVw .stopLoading()
            self.hideActivityIndicator()
        }
    }
    
    @objc func onClickCancelBtn() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - CONFIGUREVIEW
    func configureView() {
        
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor .white]
        
        pushNotificationsWebVw.scrollView.bounces = false
        pushNotificationsWebVw.delegate = self 
        
        if isFromTermsofservice {
            
            //navigationItem.title = "Terms of Service"
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "terms1", value: ""))
            
        } else if isFromPrivacyPolicy {
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "privacy2", value: ""))
            
            // navigationItem.title = "Privacy Policy"
            
        } else {
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "user3", value: ""))
            
            // navigationItem.title = "User Agreement"
        }
        
        loadWebVw()
    }
    
    func setUpUI() {
        
        //navigationController?.showNavigationBar()
        //navigationController?.navigationBar.applyGradient()
    }
    
    //MARK: - LOADVIEW
    func loadWebVw () {
        
        if Reachability.isNetworkAvailable() {
            
            self.showActivityIndicator()
            
            var webUrlStr: String? = nil
            
            if isFromTermsofservice {
                
                webUrlStr = "\(BASEURL)\(WEBVIEW_TERMS_CODITIONS)"
                
            } else if isFromPrivacyPolicy {
                
                webUrlStr = "\(BASEURL)\(WEBVIEW_PRIVICY_POLICY)"
                
            } else if userAgreement{
                
                webUrlStr = "\(BASEURL)\(WEBVIEW_USER_AGREEMENTS)"
            }
            
            if let url = URL(string: webUrlStr ?? "") {
                
                var urlRequest: URLRequest? = nil
                
                urlRequest = URLRequest(url: url)
                
                if let urlRequest = urlRequest {
                    
                    pushNotificationsWebVw.loadRequest(urlRequest)
                }
            }
            
        } else {
            
            self.hideActivityIndicator()
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: INTERNET_MSG)
        }
    }
}

//MARK: -  UIWebView Delegate
extension PushNotificationsViewController: UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.hideActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        self.hideActivityIndicator()
        
        // ERROR_MESSAGE = error.localizedDescription
        // UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
    }
}
