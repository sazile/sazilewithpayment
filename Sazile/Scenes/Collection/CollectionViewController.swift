//
//  CollectionViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 01/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
class CollectionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var collectionTblVw: UITableView!
    var page = NSInteger()
    var totalpages = NSInteger()
    var ispagesAvailable = Bool()
    var isFormPagenation = Bool()
    var notificationsArr : [[String: Any]] = []
    var tokenKey = String()
    var userId = String()
    var dateStr = String()
    var massageID = Int()
    var safeZoneStr = String()
    var totalPages = NSInteger()
    var nofificationType = String()

    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionTblVw.separatorStyle = .none
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "alert_messages", value: ""))
       // self.noMassgesLbl.text = (L102Language.AMLocalizedString(key: "no_messages_yet", value: ""))

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        page = 1
        ispagesAvailable = true
        isFormPagenation = false

        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            let collectionData = data as! NSDictionary
            tokenKey = collectionData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = collectionData["user_id"] as? String ?? ""
            print(userId)
            
        }
        getcollections()
    }
    
    static func getInstance() -> CollectionViewController {
        
        let storyboard = UIStoryboard(name: "Collection", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CollectionViewController") as! CollectionViewController
    }
    //MARK: - TABLEVIEW DELEGATE DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionTableViewCell") as! CollectionTableViewCell
        // cell.nameLbl.text = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")
       // cell.safeZoneLbl.text = String(format: "%@",notificationsArr[indexPath.row]["notification_title"] as? String ?? "")
        nofificationType = String(format: "%@",notificationsArr[indexPath.row]["notification_type"] as? String ?? "")

        print("NOTIFICATION_TYPE: ===== > \(self.nofificationType)")
        
        let dateStr = notificationsArr[indexPath.row]["created_date"] as? String ?? ""
        let strArr = dateStr.components(separatedBy: " ")
        let date1 = strArr[0]
        cell.dateLbl.text = date1
        
        
        if self.massageID == 76 {
                   
            if self.nofificationType == "1" {
                
                cell.safeZoneLbl.text = (L102Language.AMLocalizedString(key: "safe_zone", value: ""))
                 cell.nameLbl.text = (L102Language.AMLocalizedString(key: "you_are_safe", value: ""))

                
            } else if self.nofificationType == "2"  {
                
                cell.safeZoneLbl.text = (L102Language.AMLocalizedString(key: "risk_zone", value: ""))
                cell.nameLbl.text = (L102Language.AMLocalizedString(key: "you_are_in_risk_zone", value: ""))
                
                
            } else {
                
                cell.safeZoneLbl.text = String(format: "%@",notificationsArr[indexPath.row]["notification_title"] as? String ?? "")
                cell.nameLbl.text = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")
                
            }

               } else {
            
                   cell.safeZoneLbl.text = String(format: "%@",notificationsArr[indexPath.row]["notification_title"] as? String ?? "")
                   cell.nameLbl.text = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")

               }
        
        cell.selectionStyle = .none
        cell.congrtsVw.layer.cornerRadius = 5
        cell.congrtsVw.clipsToBounds = true
        
        cell.congrtsVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cell.congrtsVw.layer.shadowOpacity = 1
        cell.congrtsVw.layer.shadowOffset = CGSize.zero
        cell.congrtsVw.layer.shadowRadius = 1
        cell.congrtsVw.clipsToBounds = false
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        nofificationType = String(format: "%@",notificationsArr[indexPath.row]["notification_type"] as? String ?? "")

        if self.massageID == 76 {
            
            if self.nofificationType == "1" {
                
                safeZoneStr = (L102Language.AMLocalizedString(key: "you_are_safe", value: ""))
                let szVC = SafeZoneViewController.getInstance()
                szVC.safeZoneStr = self.safeZoneStr
                szVC.massageID = self.massageID
                print(safeZoneStr )
                self.navigationController?.pushViewController(szVC, animated: true)

                
            } else if self.nofificationType == "2" {
                
                safeZoneStr = (L102Language.AMLocalizedString(key: "you_are_in_risk_zone", value: ""))
                let szVC = SafeZoneViewController.getInstance()
                szVC.safeZoneStr = self.safeZoneStr
                szVC.massageID = self.massageID
                print(safeZoneStr )
                self.navigationController?.pushViewController(szVC, animated: true)

            }
//            } else {
//
//
//                safeZoneStr = notificationsArr[indexPath.row]["text"] as? String ?? ""
//                let szVC = SafeZoneViewController.getInstance()
//                szVC.safeZoneStr = self.safeZoneStr
//                print(safeZoneStr )
//                self.navigationController?.pushViewController(szVC, animated: true)
//
//            }
            
        } else {
            
            safeZoneStr = notificationsArr[indexPath.row]["text"] as? String ?? ""
            let szVC = SafeZoneViewController.getInstance()
            szVC.safeZoneStr = self.safeZoneStr
            print(safeZoneStr )
            self.navigationController?.pushViewController(szVC, animated: true)
        }
//           safeZoneStr = notificationsArr[indexPath.row]["text"] as? String ?? ""
//           let dVF = SafeZoneViewController.getInstance()
//           dVF.safeZoneStr = self.safeZoneStr
//           print(safeZoneStr )
//           self.navigationController?.pushViewController(dVF, animated: true)
       }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == notificationsArr.count - 1 {
            
            if totalpages == 1 {
                
                ispagesAvailable = false
            }
            
            if ispagesAvailable == true {
                
                page = page + 1
                isFormPagenation = true
                
                
                if totalpages == page {
                    
                    ispagesAvailable = false
                    
                }
                
              //  getcollections()
                
            }
        }
        
    }
}


//MARK: - COLLECTION API
extension CollectionViewController{
    
    func getcollections() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(NOTIFICATIONS)"
            
            let params = [
                "user_id":  userId,
                "token_key": tokenKey,
                "page" : String(format: "%ld", Int(page)),
                "limit" : String(format: "%ld", Int(10))
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Collection Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                
                                let pages = json["total_page"] as? String ?? ""
                                
                                self.totalpages = NSInteger(pages) ?? 0
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                print("MESSAGEID: ===== > \(self.massageID)")
                                if self.isFormPagenation {

                                    if let data = json["data"] as? [[String : Any]] {
                                        
                                        self.notificationsArr.append(contentsOf: data)
                                        
                                        DispatchQueue.main.async {
                                            
                                            self.collectionTblVw.reloadData()
                                        }
                                    }
                                    
                                } else {
                                    
                                   
                                    if let data = json["data"] as? [[String : Any]] {
                                        self.notificationsArr = data

                                            DispatchQueue.main.async {
                                                self.collectionTblVw.delegate = self
                                                self.collectionTblVw.dataSource = self
                                                self.collectionTblVw.reloadData()
                                                
                                        }
                                    }
                                    
                                }
                                
                            } else {
                               
                                if !self.isFormPagenation {
                                    
                                }
                                
                                self.ispagesAvailable = false
                                self.page = self.page - 1
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                if self.massageID == 77 {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "no_notifications", value: "")))


                                } else {
                                    
                                    if let error = json["message"] as? String {
                                        
                                        ERROR_MESSAGE = error
                                        
                                        if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                            
                                            self.userSessionExpired()
                                            
                                        } else {
                                            
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                        }
                                    }
                                    
                                }


                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Collection Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}


