//
//  ReferralViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class ReferralViewController: UIViewController {
    
    var referralHistory : [[String : Any]] = []
    var tokenKey = String()
    var userId = String()
    
    @IBOutlet weak var minsEarned: UILabel!
    @IBOutlet weak var referralCodeLb: UILabel!
    @IBOutlet weak var yourReferrelcodeLbl: UILabel!
    @IBOutlet weak var getRefferalLbl: UILabel!
    @IBOutlet weak var numberOfFrdsLbl: UILabel!
    @IBOutlet weak var minsLbl: UILabel!
    @IBOutlet weak var refferFrdsLbl: UILabel!
    @IBOutlet weak var shereBtn: UIButton!
    @IBOutlet weak var friendsJoinedVw: UIView!
    @IBOutlet weak var referralCodeVw: UIView!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "referral", value: ""))
        self.getRefferalLbl.text = (L102Language.AMLocalizedString(key: "get_referral_code", value: ""))
        self.yourReferrelcodeLbl.text = (L102Language.AMLocalizedString(key: "your_referral_code", value: ""))
        self.shereBtn.setTitle(L102Language.AMLocalizedString(key: "share_noww", value: ""), for: .normal)
        self.minsEarned.text = (L102Language.AMLocalizedString(key: "minutes_earned", value: ""))

        friendsJoinedVw.layer.cornerRadius = 10
        friendsJoinedVw.clipsToBounds = true
        friendsJoinedVw.layer.shadowOffset = CGSize(width:2, height:2 )
        friendsJoinedVw.layer.shadowColor = UIColor.black.cgColor
        friendsJoinedVw.layer.shadowOpacity = 2
        friendsJoinedVw.layer.shadowOffset = CGSize.zero
        friendsJoinedVw.layer.shadowRadius = 1
        friendsJoinedVw.clipsToBounds = false
        
        referralCodeVw.layer.borderWidth = 1
        referralCodeVw.layer.borderColor = UIColor.white.cgColor
        referralCodeVw.layer.cornerRadius = 10
        referralCodeVw.clipsToBounds = true
        
        shereBtn.layer.shadowColor = UIColor.lightText.cgColor
        shereBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        shereBtn.layer.shadowOpacity = 3.0
        shereBtn.layer.shadowRadius = 0.0
        shereBtn.layer.masksToBounds = false
        shereBtn.layer.cornerRadius = shereBtn.frame.size.height / 2
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            let referralData = data as! NSDictionary
            tokenKey = referralData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = referralData["user_id"] as? String ?? ""
            print(userId)
            getReferrals()
        }
    }
    
    static func getInstance() -> ReferralViewController {
        
        let storyboard = UIStoryboard(name: "Referral", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReferralViewController") as! ReferralViewController
    }
    
    // MARK:- IB ACTIONS
    @IBAction func onTapShareBtn(_ sender: UIButton) {
        
               DispatchQueue.main.async {
            
            let message = ""
            let items = [message]
            
            let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
              //  if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            if IS_IPHONE {
                
                self.present(activityController, animated: true, completion: nil)
                
            } else {
                
                let popOver = activityController.popoverPresentationController
                if popOver != nil {
                    popOver?.sourceView = activityController.view
                    popOver?.sourceRect = CGRect(x: 0, y: 0, width: 0, height: 0)
                    
                    self.present(activityController, animated: true)
                }
            }
        }
    }
    @IBAction func onTapReferralBtn(_ sender: UIButton) {
        
        let invc = InviteViewController.getInstance()
        self.navigationController?.pushViewController(invc, animated: true)
        
    }
    
}

//MARK: - REFERRAL API
extension ReferralViewController{
    
    func getReferrals() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(REFERRAL_HISTORY)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Referal Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            self.referralCodeLb.text = String(format: "%@",json["referral_code"] as? String ?? "")
                            self.numberOfFrdsLbl.text = String(format: "%@%@", json["total_joined_friends"] as? String ?? "",(L102Language.AMLocalizedString(key: "freinds_joined", value: "")))
                            
                            self.minsLbl.text = String(format: "%@min",json["total_earning"] as? String ?? "")
                            
                            self.refferFrdsLbl.text = String(format: "%@ %@ %@ %@ %@",
                                                             (L102Language.AMLocalizedString(key: "refer", value: "")),json["reffer_friend"] as? String ?? "",(L102Language.AMLocalizedString(key: "earn", value: ""))
                                ,json["referral_earn_minutes"] as? String ?? "",(L102Language.AMLocalizedString(key: "free_minutes", value: "")))
                            
                            if(status_code == "1") {
                                
                                if let data = json["data"] as? [[String : Any]] {
                                    
                                    self.referralHistory = data
                                    
                                }
                                
                            }
                            else
                            {
                                self.numberOfFrdsLbl.text = (L102Language.AMLocalizedString(key: "freinds_joined00", value: ""))
                                
                                self.minsLbl.text = (L102Language.AMLocalizedString(key: "0min", value: ""))
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        //                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("referral Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
}


