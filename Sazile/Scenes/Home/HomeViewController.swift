//
//  HomeViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.

//token_key=VXTbmDXV78CXISQT1jChZtMWG6fiZMBVKv67943gcGeVz4OAor
//user_id=34
//nearby_range=2 | ( In KM )
//lat=31.308621808420494
//lon=75.57907104492188


import UIKit
import LGSideMenuController
import GoogleMaps
import GooglePlaces
import CoreLocation
import Toast_Swift
//import ARKit
var strHoldAmount : String?
class HomeViewController: UIViewController ,GMSMapViewDelegate, MapMarkerDelegate {
    func didTapReserveButton(data: NSDictionary) {
       print("tap on reserve button")
        DispatchQueue.main.async {
            self.zoneType = self.deviceDict["zone_type"] as? String ?? ""
            self.onlineStr = self.deviceDict["online_status"] as? String ?? ""
            print("ONLINE STATUS =====> \(self.onlineStr)")
            if let batteryStr = self.deviceDict["battery_range"] as? String {
                
                var dummyStr : String = batteryStr
                dummyStr.remove(at: dummyStr.index(before: dummyStr.endIndex))
                print("Dummy Str \(dummyStr)")
                let myString : String = dummyStr
                let int: Int? = Int(myString)
                
                if self.zoneType == "0" && int! <= 11 || self.zoneType == "1" && int! <= 11 || self.onlineStr == "off"{
                    
                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                        self.eleSootorVw.isHidden = true
                        self.scootorDetailsVw.isHidden = true
                    }
                    
                } else if self.zoneType == "1" && int! >= 11  {
                    
                    self.btn.isHidden = false
                    self.reserveScooter()
                    
                }
                    
                else if self.zoneType == "2" {
                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "red_zone", value: "")), buttonTitle: "Ok") {
                        self.eleSootorVw.isHidden = true
                        self.scootorDetailsVw.isHidden = true
                    }
                } else if self.zoneType == "0" {
                    
                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                        self.eleSootorVw.isHidden = true
                        self.scootorDetailsVw.isHidden = true
                    }
                }
            }
            
        }
    }
    
    
    private var infoWindow = MapMarkerWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    fileprivate var FirstlocationMarker : GMSMarker? = GMSMarker()
    
    var zonedataArr : [[String: Any]] = []
    var dataArrForZone : [[String: Any]] = []
    var zoneDataDict = NSDictionary()
    var zoneType = String()
    var lat : Double = 0.0
    var lon : Double = 0.0
    var dateStr = String()
    var onlineStr = String()
    var massageID = Int()
    var engineRiding = String()

    
    var lattiude : Double = 0.0
    var longitude : Double = 0.0
    //    var gmsPolygonLayer = GMSPolygonLayer()
    //    var gmsPolygon = GMSPolygon()
    var batteryStr = String()
    var tappedMarker : GMSMarker?
    var refreshControl = UIRefreshControl()
    
    var deviceListArr : [[String: Any]] = []
    var olddeviceListArr : [[String: Any]] = []
    var tokenKey = String()
    var userId = String()
    var devicedict = String()
    var cardId = String()
    var deviceId = String()
    var markerLat = String()
    var markerLon = String()
    var cardDataDict = NSDictionary()
    
    var locationManager = CLLocationManager()
    var currentLattitude = NSString()
    var currentLongitude = NSString()
    var markerForCurrentLocation = GMSMarker()
    var deviceMarker = GMSMarker()
    var deviceDict = NSDictionary()
    var deviceDict1 = [[String: Any]]()
    var updateLocation = CLLocationCoordinate2D()
    var clLocationCoOrdinate = CLLocationCoordinate2D()
    var addressData = NSArray()
    var value:Int = 25/100
    var distanceInt = 1000
    var nearestmarkerlatt = String()
    var nearestMarkerlon = String()
    var dateStr1 = String()
    
    var latStr = -18.7669
    var lonStr = 47.3023
    
    
    var legsArr : [[String: Any]] = []
    var user_polyline_lat : Double = 0.0
    var user_polyline_lng : Double = 0.0
    var store_polyline_lat : Double = 0.0
    var store_polyline_lng : Double = 0.0
    
    
    var nearLocationLattitude = NSString()
    var nearLocationLongitude = NSString()
    
    var isRiding = String()
    var ridingDeviceDict : NSDictionary = [:]
    var isReserverd = String()
    var reservedDeviceDict : NSDictionary = [:]
    var userDetailsDict : [String: Any] = [:]
    var getCardsArr : [[String: Any]] = []
    
    var countdownTimer: Timer!
    var totalTime = Int()
    var notificationTimeBool = Bool()
    var engineStarting : Timer? = Timer()
    var isFromDismissBool = Bool()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate //Singlton instance

    //MARK: -  OUTLETS
    
    @IBOutlet weak var ontapcaseBtn: UIButton!
    @IBOutlet weak var rideLbl: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var backgroundBtn: UIButton!
    @IBOutlet weak var reservingVw: UIView!
    @IBOutlet weak var stripePaymentVw: UIView!
    @IBOutlet weak var cardSaveBtn: UIButton!
    @IBOutlet weak var lastFourDigitsLbl: UILabel!
    @IBOutlet weak var currentAddressLbl: UILabel!
    @IBOutlet weak var currentAddrssVw: UIView!
    @IBOutlet weak var timerAgainVw: UIView!
    @IBOutlet weak var timerVw: UIView!
    @IBOutlet weak var gotItBtn: UIButton!
    @IBOutlet weak var limitTimerYouerVehicleLbl: UILabel!
    @IBOutlet weak var limitTimerLbl: UILabel!
    
    @IBOutlet weak var scanVw: UIView!
    @IBOutlet weak var timerLbl: UILabel!
    
    @IBOutlet weak var lastFourDigitVw: UIView!
    @IBOutlet weak var scooterBookingVw: UIView!
    @IBOutlet weak var scooterImgVw: UIImageView!
    @IBOutlet weak var numberPlateLbl: UILabel!
    @IBOutlet weak var batteryImgVw: UIImageView!
    @IBOutlet weak var batteryPercentageLbl: UILabel!
    @IBOutlet weak var markarImgVw: UIImageView!
    @IBOutlet weak var sppedLbl: UILabel!
    @IBOutlet weak var scooterLockVw: UIView!
    @IBOutlet weak var distnceKmLbl: UILabel!
    @IBOutlet weak var lockLbl: UILabel!
    @IBOutlet weak var sooterEndBtn: UIButton!
    
    @IBOutlet weak var zoneVw: UIView!
    @IBOutlet weak var zoneLbl: UILabel!
    @IBOutlet weak var zoneOkBtn: UIButton!
    @IBOutlet weak var dangerVw: UIView!
    @IBOutlet weak var dangerLbl: UILabel!
    @IBOutlet weak var dangerOkBtn: UIButton!
    @IBOutlet weak var zoneBtn: UIButton!
    
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var reserveLbl: UILabel!
    @IBOutlet weak var reserveVw: UIView!
    @IBOutlet weak var scooterAdressLbl: UILabel!
    @IBOutlet weak var scooterPerLbl: UILabel!
    @IBOutlet weak var scooterNUmLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
    @IBOutlet weak var scootorDetailsVw: UIView!
    @IBOutlet weak var eleSootorVw: UIView!
    @IBOutlet weak var serachBarVw: UIView!
    @IBOutlet weak var mapVw: GMSMapView!
    @IBOutlet weak var rideVw: UIView!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var distanceKMLbl: UILabel!
    
    
    @IBOutlet weak var reportBtn: UIButton!
    @IBOutlet weak var reportImgVw: UIImageView!
    @IBOutlet weak var batNameLbl: UILabel!
    @IBOutlet weak var speedLbl: UILabel!
    @IBOutlet weak var pricingLbl: UILabel!
    @IBOutlet weak var navigationBtn: UIButton!
    @IBOutlet weak var stripePaymentLbl: UILabel!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var reservingLbl: UILabel!
    @IBOutlet weak var scanNowLbl: UILabel!
    @IBOutlet weak var limitTimeVehicleLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var rtMissingLbl: UILabel!
    
    //    let path = GMSMutablePath()
    //    var polyline = GMSPolygon()
    //
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scooterBookingVw.layer.shadowOffset = CGSize(width:1, height:1 )
        scooterBookingVw.layer.shadowOpacity = 1.0
        scooterBookingVw.layer.shadowOffset = CGSize.zero
        scooterBookingVw.layer.shadowRadius = 1
        scooterBookingVw.clipsToBounds = false
        
        cardSaveBtn.layer.shadowColor = UIColor.lightGray.cgColor
        cardSaveBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        cardSaveBtn.layer.shadowOpacity = 3.0
        cardSaveBtn.layer.shadowRadius = 0.0
        cardSaveBtn.layer.masksToBounds = false
        cardSaveBtn.layer.cornerRadius = cardSaveBtn.frame.size.height / 2
        
        lastFourDigitVw.layer.borderWidth = 1
        lastFourDigitVw.layer.borderColor = UIColor.gray.cgColor
        lastFourDigitVw.clipsToBounds = true
        
        stripePaymentVw.layer.cornerRadius = 10
        stripePaymentVw.clipsToBounds = true
        stripePaymentVw.layer.shadowOffset = CGSize(width:2, height:2 )
        stripePaymentVw.layer.shadowColor = UIColor.black.cgColor
        stripePaymentVw.layer.shadowOpacity = 2
        stripePaymentVw.layer.shadowOffset = CGSize.zero
        stripePaymentVw.layer.shadowRadius = 1
        stripePaymentVw.clipsToBounds = false
        
        timerVw.layer.cornerRadius = 3
        timerVw.clipsToBounds = true
        timerVw.layer.shadowOffset = CGSize(width:2, height:2 )
        timerVw.layer.shadowColor = UIColor.black.cgColor
        timerVw.layer.shadowOpacity = 2
        timerVw.layer.shadowOffset = CGSize.zero
        timerVw.layer.shadowRadius = 1
        timerVw.clipsToBounds = false
        
        
        timerAgainVw.layer.cornerRadius = 10
        timerAgainVw.clipsToBounds = true
        timerAgainVw.layer.shadowOffset = CGSize(width:2, height:2 )
        timerAgainVw.layer.shadowColor = UIColor.black.cgColor
        timerAgainVw.layer.shadowOpacity = 2
        timerAgainVw.layer.shadowOffset = CGSize.zero
        timerAgainVw.layer.shadowRadius = 1
        timerAgainVw.clipsToBounds = false
        
        eleSootorVw.layer.cornerRadius = 3
        eleSootorVw.clipsToBounds = true
        eleSootorVw.layer.shadowOffset = CGSize(width:2, height:2 )
        eleSootorVw.layer.shadowColor = UIColor.black.cgColor
        eleSootorVw.layer.shadowOpacity = 2
        eleSootorVw.layer.shadowOffset = CGSize.zero
        eleSootorVw.layer.shadowRadius = 1
        eleSootorVw.clipsToBounds = false
        
        scootorDetailsVw.layer.cornerRadius = 3
        scootorDetailsVw.clipsToBounds = true
        scootorDetailsVw.layer.shadowOffset = CGSize(width:2, height:2 )
        scootorDetailsVw.layer.shadowColor = UIColor.black.cgColor
        scootorDetailsVw.layer.shadowOpacity = 2
        scootorDetailsVw.layer.shadowOffset = CGSize.zero
        scootorDetailsVw.layer.shadowRadius = 1
        scootorDetailsVw.clipsToBounds = false
        
        currentAddrssVw.layer.cornerRadius = 2
        currentAddrssVw.clipsToBounds = true
        currentAddrssVw.layer.shadowOffset = CGSize(width:2, height:2 )
        currentAddrssVw.layer.shadowColor = UIColor.black.cgColor
        currentAddrssVw.layer.shadowOpacity = 2
        currentAddrssVw.layer.shadowOffset = CGSize.zero
        currentAddrssVw.layer.shadowRadius = 1
        currentAddrssVw.clipsToBounds = false
        
        reservingVw.layer.cornerRadius = 10
        reservingVw.clipsToBounds = true
        reservingVw.layer.shadowOffset = CGSize(width:2, height:2 )
        reservingVw.layer.shadowColor = UIColor.black.cgColor
        reservingVw.layer.shadowOpacity = 2
        reservingVw.layer.shadowOffset = CGSize.zero
        reservingVw.layer.shadowRadius = 1
        reservingVw.clipsToBounds = false
        
        rideVw.layer.cornerRadius = rideVw.frame.size.height/2
        rideVw.clipsToBounds = true
        rideVw.layer.shadowOffset = CGSize(width:-2, height:-2 )
        rideVw.layer.shadowColor = UIColor.lightGray.cgColor
        rideVw.layer.shadowOpacity = 2.0
        rideVw.layer.shadowRadius = 3
        rideVw.clipsToBounds = false
        
        serachBarVw.layer.cornerRadius = serachBarVw.frame.size.height / 2
        serachBarVw.clipsToBounds = true
        
        gotItBtn.layer.shadowColor = UIColor.lightGray.cgColor
        gotItBtn.layer.shadowOffset = CGSize(width:0.0, height: 1.5 )
        gotItBtn.layer.shadowOpacity = 3.0
        gotItBtn.layer.shadowRadius = 0.0
        gotItBtn.layer.masksToBounds = false
        gotItBtn.layer.cornerRadius = gotItBtn.frame.size.height / 2
        
        reserveVw.layer.shadowColor = UIColor.lightGray.cgColor
        reserveVw.layer.shadowOffset = CGSize(width:0.0, height: 1.5 )
        reserveVw.layer.shadowOpacity = 3.0
        reserveVw.layer.shadowRadius = 0.0
        reserveVw.layer.masksToBounds = false
        reserveVw.layer.cornerRadius = reserveVw.frame.size.height / 2
        
        scooterLockVw.layer.shadowColor = UIColor.lightGray.cgColor
        scooterLockVw.layer.shadowOffset = CGSize(width:0, height: 2.0 )
        scooterLockVw.layer.shadowOpacity = 3.0
        scooterLockVw.layer.shadowRadius = 0.0
        scooterLockVw.layer.masksToBounds = false
        scooterLockVw.layer.cornerRadius = scooterLockVw.frame.size.height / 2
        
        scanVw.layer.shadowColor = UIColor.lightGray.cgColor
        scanVw.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        scanVw.layer.shadowOpacity = 3.0
        scanVw.layer.shadowRadius = 0.0
        scanVw.layer.masksToBounds = false
        scanVw.layer.cornerRadius = scanVw.frame.size.height / 2
        
        zoneVw.layer.shadowColor = UIColor.darkText.cgColor
        zoneVw.layer.shadowOpacity = 1
        zoneVw.layer.shadowOffset = CGSize.zero
        zoneVw.layer.shadowRadius = 20
        zoneVw.layer.cornerRadius = 5
        
        zoneOkBtn.layer.shadowColor = UIColor.lightGray.cgColor
        zoneOkBtn.layer.shadowOffset = CGSize(width:0, height: 2.0 )
        zoneOkBtn.layer.shadowOpacity = 3.0
        zoneOkBtn.layer.shadowRadius = 0.0
        zoneOkBtn.layer.masksToBounds = false
        zoneOkBtn.layer.cornerRadius = zoneOkBtn.frame.size.height / 2
        
        dangerVw.layer.shadowColor = UIColor.darkText.cgColor
        dangerVw.layer.shadowOpacity = 1
        dangerVw.layer.shadowOffset = CGSize.zero
        dangerVw.layer.shadowRadius = 20
        dangerVw.layer.cornerRadius = 5
        
        dangerOkBtn.layer.shadowColor = UIColor.lightGray.cgColor
        dangerOkBtn.layer.shadowOffset = CGSize(width:0, height: 2.0 )
        dangerOkBtn.layer.shadowOpacity = 3.0
        dangerOkBtn.layer.shadowRadius = 0.0
        dangerOkBtn.layer.masksToBounds = false
        dangerOkBtn.layer.cornerRadius = zoneOkBtn.frame.size.height / 2
        
        ontapcaseBtn.layer.shadowColor = UIColor.lightGray.cgColor
        ontapcaseBtn.layer.shadowOffset = CGSize(width:0, height: 2.0 )
        ontapcaseBtn.layer.shadowOpacity = 3.0
        ontapcaseBtn.layer.shadowRadius = 0.0
        ontapcaseBtn.layer.masksToBounds = false
        ontapcaseBtn.layer.cornerRadius = ontapcaseBtn.frame.size.height / 2
        
        self.infoWindow.delegate = self
        self.infoWindow = loadNiB()
        
        nsNotifications()
    }
    
    func loadNiB() -> MapMarkerWindow {
        let infoWindow = MapMarkerWindow.instanceFromNib()
        return infoWindow as! MapMarkerWindow
    }
    
    static func getInstance() -> HomeViewController {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // currentTime()
        
        //
        //      let currentDate = UserDefaults.standard.value(forKey: "NOTIFICATION_RESPONSE")
        //            print("Data12344556677: \(currentDate)")
        //
        //       // getNotificationsTimeAPI()
        //
        //        if let date = UserDefaults.standard.value(forKey: "NOTIFICATION_RESPONSE") {
        //            UserDefaults.standard.synchronize()
        //
        //            print("Data12344556677: \(date)")
        //            let notificationsDate = date as! NSDictionary
        //
        //            dateStr = notificationsDate["created_at"] as? String ?? ""
        //
        //            print("NOTIFICATIONS CURRENTTIME : ====> \(dateStr)")
        //
        //            if notificationTimeBool == true {
        //
        //                DispatchQueue.main.async {
        //
        //                    ERROR_MESSAGE = self.dateStr
        //
        //                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
        //
        //                        self.userSessionExpired()
        //
        //                    }
        //
        //                }
        //
        //            }
        //
        //        }
        //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNearByDevicesAPI), name: NSNotification.Name(GETNEARBYSCOOTERS), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getZoneDataApI), name: NSNotification.Name(GETNEARBYSCOOTERS), object: nil)
        
        self.countTimer()
        self.dangerVw.isHidden = true
        self.zoneVw.isHidden = true
        self.zoneBtn.isHidden = true
        self.currentAddrssVw.isHidden = false
        
        deviceId = deviceDict["device_id"] as? String ?? ""
        print(deviceId)
        
        setUpUI()
        
        self.btn.isHidden = true
        self.backgroundBtn.isHidden = true
        self.dismissBtn.isHidden = true
        self.reservingVw.isHidden = true
        self.stripePaymentVw.isHidden = true
        timerVw.isHidden = true
        timerAgainVw.isHidden = true
        // self.rideVw.isHidden = false
        // self.serachBarVw.isHidden = false
        scooterBookingVw.isHidden = true
        isForEndRideBool = false
        
        if isForSelectCardBool {
            
            istimerAgainBool = false
            
            if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
                
                print("Card Data : \(cardData)")
                
                self.cardDataDict = cardData as! NSDictionary
                print("Card Data : \(cardData)")
                self.btn.isHidden = false
                stripePaymentVw.isHidden = false
                isForSelectCardBool = false
                self.lastFourDigitsLbl.text = self.cardDataDict["card_last_four_digit"] as? String ?? ""
                self.cardId = self.cardDataDict["card_id"] as? String ?? ""
                
            } else {
                
                self.stripePaymentVw.isHidden = true
                self.btn.isHidden = true
                
                DispatchQueue.main.async {
                    
                    let pcVC = PaymentCardViewController.getInstance()
                    self.navigationController?.pushViewController(pcVC, animated: true)
                }
            }
            
        } else {
            
            if isForSelectImageReportingBool {
                
                
            } else {
                
                self.dismissBtn.isHidden = true
                self.eleSootorVw.isHidden = true
                self.scootorDetailsVw.isHidden = true
                
                if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
                    
                    let devices = data as! NSDictionary
                    
                    tokenKey = devices["token_key"] as? String ?? ""
                    print(tokenKey)
                    userId = devices["user_id"] as? String ?? ""
                    print(userId)
                    
                    checkCardDataInUserDefaults()
                }
                
                setUpLocationPermissions()
                
            }
        }
        self.getConfig()
        self.getZoneDataApI()
        
    }
    
    // Get configurations
    func getConfig() {
        if Reachability.isNetworkAvailable(){
                        let urlString = "\(BASEURL)\(get_configurations)"
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
            ]
            #if DEBUG
            print("BEGIN SAZILE======>URLSTRING ======>"  + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            #endif
            self.showActivityIndicator()
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                switch result{
                case .success(let upload, _,_):
                    upload.responseJSON { response in
                        #if DEBUG
                        print(" Add Card Response : \(response)")
                        #endif
                        if let err = response.error {
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            return
                        }
                        if let result = response.result.value {
                            let json = result as! NSDictionary
                            let status_code : String = json["success"] as? String ?? ""
                            if(status_code == "1") {
                                if let data = json["data"] as? [String : Any]{
                                    strHoldAmount = String(format: "%@", data["ini_hold_amount"] as! CVarArg)
                                    
                                    if data["payment_is_live"] as! String == "0" {
                                        self.appDelegate.setUpStripe(strStripeKey: "pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk")
                                    }else{
                                        self.appDelegate.setUpStripe(strStripeKey: "pk_live_XS8g8vKcegq4oYNTenGzVr5Z00ELfZvd6B")
                                    }
                                }else{
                                    UIAlertController.showAlert(vc: self, title: "", message: json["msg"] as? String ?? "")
                                }
                            }
                            else{
                                self.hideActivityIndicator()
                                UIAlertController.showAlert(vc: self, title: "", message: json["msg"] as? String ?? "")
                            }
                            self.hideActivityIndicator()
                        }
                    }
                case .failure(let err):
                    print("Add Card/ Error:\(err.localizedDescription)")
                }
            }
        }
        else{
            self.appDelegate.setUpStripe(strStripeKey: "pk_test_hdtArqxRIoY4I4J8YDa5pDxY00UpOsyLgk")
        }
    }
    
    
    func zoneTimer() {
        stopZoneTimer()
        zoneRedorNoneTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(getNearByDevicesAPI), userInfo: nil, repeats: true)
        
    }
    
    func countTimer() {
        
        notificationTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(getNotificationsCountAPI), userInfo: nil, repeats: true)
    }
    
    func engineStartingTimer() {
        
        engineStoptimer()
        engineStarting = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(engineStartingToast), userInfo: nil, repeats: true)

    }
    
    @objc func engineStartingToast() {
        
        self.view.makeToast ((L102Language.AMLocalizedString(key: "engine_starting", value: "")), duration: 0.5, position: .center, style: style)

        
    }
    
    func nsNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpUI), name: NSNotification.Name(LOCALIZATIONREFRESH), object: nil)
    }
    
    //    func currentTime() -> String {
    //        let date = Date()
    //        let calendar = Calendar.current
    //        let hour = calendar.component(.hour, from: date)
    //        let minutes = calendar.component(.minute, from: date)
    //        print("CURRENT TIME DATE : ======> \(hour)")
    //        print("CURRENT TIME DATE : ======> \(minutes)")
    //        return "\(hour):\(minutes)"
    //
    //
    //    }
    
    
    @objc func setUpUI() {
        
        if (UserDefaults.standard.value(forKey: "CurrentLanguage") as? String) != nil {
            
            let currentLang : String = UserDefaults.standard.value(forKey: "CurrentLanguage") as? String ?? ""
            print("Current Language : \(currentLang)")
            
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "nearby_e_scooter", value: ""))
            self.rideLbl.text = (L102Language.AMLocalizedString(key: "scan_to_ride", value: ""))
            self.stripePaymentLbl.text = (L102Language.AMLocalizedString(key: "stripe_payment", value: ""))
            self.changeBtn.setTitle((L102Language.AMLocalizedString(key: "change", value: "")), for: .normal)
            self.saveBtn.setTitle((L102Language.AMLocalizedString(key: "save", value: "")), for: .normal)
            self.navigationBtn.setTitle((L102Language.AMLocalizedString(key: "navigation", value: "")), for: .normal)
            //self.reportMissingLbl.text = (L102Language.AMLocalizedString(key: "report_missingg", value: ""))
            self.reservingLbl.text = (L102Language.AMLocalizedString(key: "reserving", value: ""))
            self.limitTimerYouerVehicleLbl.text = (L102Language.AMLocalizedString(key: "get_your_scooter", value: ""))
            self.gotItBtn.setTitle((L102Language.AMLocalizedString(key: "got_it", value: "")), for: .normal)
            self.minLbl.text = (L102Language.AMLocalizedString(key: "timer", value: ""))
            self.rtMissingLbl.text = (L102Language.AMLocalizedString(key: "report_missingg", value: ""))
            self.scanNowLbl.text = (L102Language.AMLocalizedString(key: "scan_nowW2", value: ""))
            self.speedLbl.text = (L102Language.AMLocalizedString(key: "speed", value: ""))
            self.batNameLbl.text = (L102Language.AMLocalizedString(key: "battery", value: ""))
            self.lockLbl.text = (L102Language.AMLocalizedString(key: "lock", value: ""))
            self.zoneLbl.text = (L102Language.AMLocalizedString(key: "zone0", value: ""))
            self.zoneOkBtn.setTitle((L102Language.AMLocalizedString(key: "okay", value: "")), for: .normal)
            self.dangerLbl.text = (L102Language.AMLocalizedString(key: "zone2", value: ""))
            self.dangerOkBtn.setTitle((L102Language.AMLocalizedString(key: "okay", value: "")), for: .normal)
            self.ontapcaseBtn.setTitle((L102Language.AMLocalizedString(key: "open_tap_case", value: "")), for: .normal)
            
        }
    }
    
    @objc func RefreshAPIForLocalization() {
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let devices = data as! NSDictionary
            
            tokenKey = devices["token_key"] as? String ?? ""
            print(tokenKey)
            userId = devices["user_id"] as? String ?? ""
            
            print(userId)
        }
        
        setUpLocationPermissions()
    }
    
    
    //MARK: - GOOGLEMAPS PERMISSIONS
    func setUpLocationPermissions() {
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch (CLLocationManager.authorizationStatus()) {
                

            case .notDetermined, .restricted, .denied:

                print("BEGIN:Sazile ======> No location service access")
                requestWhenInUseAuthorization()
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("BEGIN:Sazile ======> Location service access")
                
                determineMyCurrentLocation()
                
            @unknown default:
                
                fatalError()
            }
            
        } else {
            
            print("BEGIN:Sazile ======> Location services are not enabled")
            requestWhenInUseAuthorization()
        }
    }
    
    //MARK: - checkCardDataInUserDefaults
    func checkCardDataInUserDefaults() {
        
        if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
            
            self.cardDataDict = cardData as! NSDictionary
            
            if self.cardDataDict.count == 0 {
                
                getCards()
                
            } else {
                
            }
            
        } else {
            
            getCards()
        }
    }
    
    //MARK: - reserveScooter
    func reserveScooter() {
        
        let alert = UIAlertController(title: (L102Language.AMLocalizedString(key: "reserve_zebra", value: "")), message: (L102Language.AMLocalizedString(key: "want_to_reserve_this_zebra", value: "")), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "no", value: "")),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.btn.isHidden = true
                                        self.eleSootorVw.isHidden = true
                                        self.scootorDetailsVw.isHidden = true
                                        //self.currentAddrssVw.isHidden = false
                                        
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "yes", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        if let cardData = UserDefaults.standard.value(forKey: "DEFAULT_CARD_DATA") {
                                            
                                            self.cardDataDict = cardData as! NSDictionary
                                            
                                            if self.cardDataDict.count == 0 {
                                                
                                                DispatchQueue.main.async {
                                                    
                                                    let pcVC = PaymentCardViewController.getInstance()
                                                    self.navigationController?.pushViewController(pcVC, animated: true)
                                                }
                                                
                                            } else {
                                                
                                                self.lastFourDigitsLbl.text = self.cardDataDict["card_last_four_digit"] as? String ?? ""
                                                self.cardId = self.cardDataDict["card_id"] as? String ?? ""
                                                self.stripePaymentVw.isHidden = false
                                            }
                                            
                                        } else {
                                            
                                            DispatchQueue.main.async {
                                                
                                                let pcVC = PaymentCardViewController.getInstance()
                                                self.navigationController?.pushViewController(pcVC, animated: true)
                                            }
                                        }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: -  Determine My Current Location
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.desiredAccuracy = CLLocationAccuracy(kCLDistanceFilterNone)
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        let locationObj = locationManager.location
        let coordLat = locationObj?.coordinate.latitude
        let coordLong = locationObj?.coordinate.longitude
        
        //print("Lat : \(coordLat ?? 0.00)")
        //print("Long : \(coordLong ?? 0.00)")
        //
        currentLattitude = NSString(format: "%.6f", coordLat ?? 0.00)
        currentLongitude = NSString(format: "%.6f", coordLong ?? 0.00)
        
        ////
        //        currentLattitude = NSString(format: "%.6f", latStr)
        //        currentLongitude = NSString(format: "%.6f", lonStr)
        
        print("BEGIN:Sazile======> Current lattitude : \(currentLattitude)")
        print("BEGIN:Sazile======> Current longitude : \(currentLongitude)")
        clLocationCoOrdinate = CLLocationCoordinate2D(latitude: (locationObj?.coordinate.latitude) ?? 0.000, longitude: (locationObj?.coordinate.longitude) ?? 0.000)
        
        markerForCurrentLocation.position = clLocationCoOrdinate
        markerForCurrentLocation.title = ""
        markerForCurrentLocation.map = nil
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(coordLat ?? 0.00), longitude: CLLocationDegrees(coordLong ?? 0.00), zoom: Float(14))
        
//        do {
//            // Set the map style by passing the URL of the local file.
//            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "geojson") {
//                mapVw.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
//                
//            } else {
//                NSLog("Unable to find style.json")
//            }
//        } catch {
//            NSLog("One or more of the map styles failed to load. \(error)")
//        }
//        
        
        mapVw.animate(to: camera)
        mapVw.isMyLocationEnabled = true
        mapVw.settings.myLocationButton = true
        
        //mapVw.padding = UIEdgeInsets(top: 0, left: 0, bottom: 17, right: 5)
        
        DispatchQueue.main.async {
            self.mapVw.delegate = self
        }
        
        getCurrentLocationAddress()
        getNearByDevicesAPI()
        self.getZoneDataApI()
    }
    
    //MARK: -getCurrentLocationAddress
    func getCurrentLocationAddress(){
        
        let location = CLLocation(latitude: currentLattitude.doubleValue, longitude: currentLongitude.doubleValue) //changed!!!
        print(location)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks?[0]
                //print(pm?.locality! as Any)
                //print("Full Address : \(pm?.addressDictionary)")
                
                if let dict = pm?.addressDictionary?["FormattedAddressLines"] {
                    
                    self.addressData = (dict as? NSArray)!
                    
                    print("Address Dict : \(self.addressData)")
                    
                    self.currentAddressLbl.text = String(format: "%@", self.addressData.componentsJoined(by: ","))
                    print("ADDRESSDATA: =====> \(self.addressData)")
                    
                }
                
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    //MARK: - getZoneData
    func getZoneData(){
        
        for (index,value) in zonedataArr.enumerated() {
            
            //            print("BEGIN:SAZILE====>ZONEDATA:\(index)")
            //            print("BEGIN:SAZILE====>ZONEDATA:\(value)")
            
            if let dict = value as? NSDictionary {
                
                zoneDataDict = dict
                dataArrForZone = []
                dataArrForZone = zoneDataDict["data"] as! [[String : Any]]
                zoneType = zoneDataDict["zone_type"] as! String
                
                if zoneType == "blue" {
                    
                    //let path = GMSPolyline()
                    let path = GMSMutablePath()
                    var polygon = GMSPolygon()
                    
                    for (_,coordinater) in dataArrForZone.enumerated() {
                        
                        //                    print("BEGIN:SAZILE====>ZONEDATA:\(index)")
                        //                    print("BEGIN:SAZILE====>ZONEDATA:\(coordinater)")
                        
                        let latt = coordinater["lat"] as? String ?? ""
                        let lonn = coordinater["lng"] as? String ?? ""
                        
                        lat = Double(latt) ?? 0.0
                        lon = Double(lonn) ?? 0.0
                        
                        path.add(CLLocationCoordinate2D(latitude: lat, longitude: lon))
                        
                        
                    }
                    polygon = GMSPolygon(path: path)
                    
                    polygon.strokeWidth = 4.0
                    polygon.strokeColor = UIColor(red: 34.0/255.0, green: 35.0/255, blue: 239.0/255.0, alpha: 1.0)
                    polygon.fillColor = UIColor(red: 34.0/255.0, green: 74.0/255, blue: 203.0/255.0, alpha: 0.1)
                    polygon.map = mapVw
                    
                    
                } else if zoneType == "red" {
                    
                    let path = GMSMutablePath()
                    var polygon = GMSPolygon()
                    
                    for (_,coordinater) in dataArrForZone.enumerated() {
                        
                        //                    print("BEGIN:SAZILE====>ZONEDATA:\(index)")
                        //                    print("BEGIN:SAZILE====>ZONEDATA:\(coordinater)")
                        
                        let latt = coordinater["lat"] as? String ?? ""
                        let lonn = coordinater["lng"] as? String ?? ""
                        
                        lat = Double(latt) ?? 0.0
                        lon = Double(lonn) ?? 0.0
                        
                        path.add(CLLocationCoordinate2D(latitude: lat, longitude: lon))
                        
                    }
                    polygon = GMSPolygon(path: path)
                    polygon.strokeWidth = 4.0
                    polygon.strokeColor = UIColor(red: 255.0/255.0, green: 16.0/255.0, blue: 13.0/255.0, alpha: 1.0)
                    polygon.fillColor = UIColor(red: 245.0/255.0, green: 16.0/255.0, blue: 13.0/255.0, alpha: 0.2)
                    polygon.map = mapVw
                }
                
            }
            
        }
        
    }
    
    //MARK: -  RequestWhenInUseAuthorization
    func requestWhenInUseAuthorization() {
        
        let status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        if status == .denied {
            
            var title: String
            
            title = (status == .denied) ? "Location Services Off" : ""
            let message = "Turn on Location Services in Settings > Privacy to allow Maps to determine your current location"
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction (title: "Cancel", style: .destructive, handler: nil))
            
            alertController.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (action:UIAlertAction) in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("BEGIN:Sazile ======> Settings opened: \(success)")
                    })
                }
            }))
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if status == .notDetermined {
            
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //MARK: - IB ACTIONS
    
    
    @IBAction func onTapCaseBtn(_ sender: UIButton) {
        getTapCasBoxAp()
    }
    
    
    @IBAction func onTapDangerOkBtn(_ sender: UIButton) {
        self.dangerVw.isHidden = true
        self.zoneBtn.isHidden = true
        isFromDismissBool = true

    }
    
    @IBAction func onTapZoneOkbtn(_ sender: UIButton) {
        
        self.zoneVw.isHidden = true
        self.zoneBtn.isHidden = true
        self.isFromDismissBool = true
    }
    
    
    @IBAction func onTapBackGroundBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.rideVw.isHidden = true
            self.serachBarVw.isHidden = true
            self.timerAgainVw.isHidden = true
            self.backgroundBtn.isHidden = true
            istimerAgainBool = true

        }
        
    }
    
    @IBAction func onTapPaymentDismissBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.btn.isHidden = true
            self.stripePaymentVw.isHidden = true
        }
        
    }
    
    @IBAction func onTapChangeCardBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            let pcVC = PaymentCardViewController.getInstance()
            isForSelectCardBool = true
            self.navigationController?.pushViewController(pcVC, animated: true)
        }
        
    }
    
    @IBAction func onTapCardSaveBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            self.btn.isHidden = true
            self.getReservescooterAPI()
            self.stripePaymentVw.isHidden = true
            self.eleSootorVw.isHidden = true
            self.scootorDetailsVw.isHidden = true
            self.reservingVw.isHidden = false
        }
        
    }
    
    //MARK: - START TIMER
    func startTimer() {
        stopTimer()
        countdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopZoneTimer()
        stopTimer()
       // engineStartingTimer()
        engineStoptimer()
        //notificationTimer1()
    }
    
    
    func notificationTimer1(){
        
        notificationTimer?.invalidate()
        notificationTimer = nil

        
    }
    func stopZoneTimer(){
        
        zoneRedorNoneTimer?.invalidate()
        zoneRedorNoneTimer = nil
        
        
      //  engineStarting?.invalidate()
      //  engineStarting = nil

    }
    
    func engineStoptimer(){
        
        engineStarting?.invalidate()
          engineStarting = nil

    }
    
    func stopTimer(){
        
        if countdownTimer != nil{
            countdownTimer!.invalidate()
            countdownTimer = nil
        }
    }
    
    @objc func updateTime() {
        
        timerLbl.font = timerLbl.font.withSize(35)
        limitTimerLbl.font = limitTimerLbl.font.withSize(35)
        
        timerLbl.text = "\(timeFormatted(totalTime))"
        limitTimerLbl.text = "\(timeFormatted(totalTime))"
        timerLbl.textColor = UIColor.black
        limitTimerLbl.textColor = UIColor.black
        
        if totalTime != 0 {
            totalTime -= 1
            
            
        } else {
            
            endTimer()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+2.0) {
                self.timerVw.isHidden = true
                self.getNearByDevicesAPI()
                self.rideVw.isHidden = false
                self.serachBarVw.isHidden = false
                // self.currentAddrssVw.isHidden = false
            }
            
        }
        
    }
    
    
    func endTimer() {
        countdownTimer.invalidate()
        countdownTimer = nil
        timerLbl.font = timerLbl.font.withSize(15)
        limitTimerLbl.font = limitTimerLbl.font.withSize(15)
        self.timerLbl.text = (L102Language.AMLocalizedString(key: "sorry_time_out", value: ""))
        self.limitTimerLbl.text = (L102Language.AMLocalizedString(key: "sorry_time_out", value: ""))
        //timerLbl.font = UIFont(name: "",
        // size: 10)
        //timerLbl.text = "Sorry,Time Over!"
        timerLbl.textColor = UIColor.red
        
        //limitTimerLbl.font = UIFont(name: "",
        // size: 10)
        //limitTimerLbl.text = "Sorry,Time Over!"
        limitTimerLbl.textColor = UIColor.red
    }
    
    
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    
    @IBAction func onTapCurrentLocBtn(_ sender: UIButton) {
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch (CLLocationManager.authorizationStatus()) {
                
            case .notDetermined, .restricted, .denied:
                
                print("BEGIN:Sazile ======> No location service access")
                requestWhenInUseAuthorization()
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("BEGIN:Sazile ======> Location service access")
                locationManager = CLLocationManager()
                
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.desiredAccuracy = CLLocationAccuracy(kCLDistanceFilterNone)
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
                
                let locationObj = locationManager.location
                let coordLat = locationObj?.coordinate.latitude
                let coordLong = locationObj?.coordinate.longitude
                
                //print("Lat : \(coordLat ?? 0.00)")
                //print("Long : \(coordLong ?? 0.00)")
                
                //                currentLattitude = NSString(format: "%.6f", latStr)  as NSString
                //                currentLongitude = NSString(format: "%.6f", lonStr) as NSString
                //
                currentLattitude = NSString(format: "%.6f", coordLat ?? 0.00)
                currentLongitude = NSString(format: "%.6f", coordLong ?? 0.00)
                
                print("BEGIN:Sazile======> Current lattitude : \(currentLattitude)")
                print("BEGIN:Sazile======> Current longitude : \(currentLongitude)")
                
                clLocationCoOrdinate = CLLocationCoordinate2D(latitude: (locationObj?.coordinate.latitude) ?? 0.000, longitude: (locationObj?.coordinate.longitude) ?? 0.000)
                
                markerForCurrentLocation.position = clLocationCoOrdinate
                markerForCurrentLocation.title = ""
                markerForCurrentLocation.map = nil
                //markerForCurrentLocation.icon = UIImage(named: "map-marker-blueGradient")
                let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(coordLat ?? 0.00), longitude: CLLocationDegrees(coordLong ?? 0.00), zoom: Float(12))
                
                mapVw.animate(to: camera)
                mapVw.isMyLocationEnabled = true
                mapVw.settings.myLocationButton = true
                
                mapVw.delegate = self
                
                getCurrentLocationAddress()
                
            @unknown default:
                
                fatalError()
            }
            
        } else {
            
            print("BEGIN:Sazile ======> Location services are not enabled")
            requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func onTapGotItBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            self.timerAgainVw.isHidden = true
            self.backgroundBtn.isHidden = true
            self.rideVw.isHidden = true
            self.serachBarVw.isHidden = true
            istimerAgainBool = true
        }
        
    }
    
    @IBAction func onTapNavigationBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            let locManager = CLLocationManager()
            locManager.requestWhenInUseAuthorization()
            
            if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways){
                
                let locationObj = self.locationManager.location
                let coordLat = locationObj?.coordinate.latitude
                let coordLong = locationObj?.coordinate.longitude
//                self.currentLattitude = NSString(format: "%.6f", self.latStr)  as NSString
//                self.currentLongitude = NSString(format: "%.6f", self.lonStr) as NSString
//                
                //                self.currentLattitude = NSString(format: "%.6f", coordLat ?? 0.00)
                //                self.currentLongitude = NSString(format: "%.6f", coordLong ?? 0.00)
                
                let lattiiitude = Double(self.markerLat)
                let longiiitude = Double(self.markerLon)
                
                var alertController = UIAlertController()
                
                alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "show_in_googlemaps", value: "")), style: .default, handler: { (action:UIAlertAction) in
                
                    
                      if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
                        let googleMapsURLString = String(format:  "comgooglemaps://?saddr=&daddr=\(self.markerLat),\(self.markerLon)&directionsmode=walking")
                        print("Url : \(googleMapsURLString)")

                        if let url = URL(string: googleMapsURLString) {
                            UIApplication.shared.open(url)
                        }
                      }

                      else {
                             //Open in browser
//                        let origin = "\(self.currentLattitude),\(self.currentLongitude)"
//                        let destination = "\(self.markerLat),\(self.markerLon)"
                        //            UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(strLat),\(strLong)&daddr=\(strLat1),\(strLong2)&directionsmode=driving&zoom=14&views=traffic")!)
//                        32.27734960518425, 75.64886559657252
                        //currentLattitude
                            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=\(self.currentLattitude),\(self.currentLongitude)&daddr=\(self.markerLat),\(self.markerLon)&directionsmode=walking") {
                                               UIApplication.shared.open(urlDestination)
                                           }
                                }

                    
                }))
                
                alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "cancel1", value: "")), style: .destructive, handler: { (action:UIAlertAction) in
                    
                }))
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func onTapAgainReportMissingBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let rmVC = ReportViewController.getInstance()
            rmVC.deviceId = self.deviceId
            self.dismissBtn.isHidden = true
            self.backgroundBtn.isHidden = true
            self.present(rmVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onTapScanNowBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let ppvc = PopUpViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
            self.backgroundBtn.isHidden = true
            self.timerVw.isHidden = true
        }
        
    }
    
    @IBAction func onTapSerachBtn(_ sender: UIButton) {
        
        //https://api.tomtom.com/routing/1/calculateRoute/52.50931,13.42936:52.50274,13.43872/json?key=Your_API_Key
    
        DispatchQueue.main.async { [self] in
            
            print("device list----->",self.olddeviceListArr)
            
            var deviceListNewArr = self.olddeviceListArr
            if deviceListNewArr.count > 0{
                for i in 0..<deviceListNewArr.count{
                    let lat = Double("\(deviceListNewArr[i]["lat"] ?? 0.0)")
                    let lon = Double("\(deviceListNewArr[i]["lon"] ?? 0.0)")
                    let currentLat = Double("\(self.currentLattitude)")
                    let currentLon = Double("\(self.currentLongitude)")
                    let coordinate₀ = CLLocation(latitude: lat!, longitude: lon!)
                    let coordinate₁ = CLLocation(latitude: currentLat!, longitude: currentLon!)
                    
                    let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                    deviceListNewArr[i]["distance"] = distanceInMeters
                    print("device lsit new arr---->",deviceListNewArr[i])
                }
                let sortedResults = (deviceListNewArr as NSArray).sortedArray(using: [NSSortDescriptor(key: "distance", ascending: true)]) as! [[String:AnyObject]]
                deviceListNewArr.removeAll()
                deviceListNewArr = sortedResults
                
                
                print(deviceListNewArr)
                for i in 0..<deviceListNewArr.count{
                   // self.mapVw.clear()
                    self.zoneType = deviceListNewArr[0]["zone_type"] as? String ?? ""
                    if let batteryStr =  deviceListNewArr[0]["battery_range"] as? String {
                        
                        var dummyStr : String = batteryStr
                        dummyStr.remove(at: dummyStr.index(before: dummyStr.endIndex))
                        print("Dummy Str \(dummyStr)")
                        let myString : String = dummyStr
                        let batteryPercentage: Int? = Int(myString)
                        if  self.zoneType == "1" && batteryPercentage! >=  11{
                            // print("came")
                            self.nearestmarkerlatt = (deviceListNewArr[0]["lat"] as? String ?? "")
                            self.nearestMarkerlon = (deviceListNewArr[0]["lon"] as? String ?? "")
                            
                            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(Double(self.nearestmarkerlatt)!), longitude: CLLocationDegrees(Double(self.nearestMarkerlon)!), zoom: Float(15))
                            
                            let newPosition = CLLocationCoordinate2D(latitude: Double(self.nearestmarkerlatt)!, longitude: Double(self.nearestMarkerlon)!)
                            
                            self.mapVw.animate(to: camera)
                            self.locationMarker = GMSMarker(position: newPosition)
                          
                           
                            self.eleSootorVw.isHidden = false
                            
                            
                            self.scootorDetailsVw.isHidden = true
                            self.reserveLbl.text = String(format: "RESERVE%@",deviceListNewArr[i]["reserve_price"] as? String ?? "")
                            //self.currentAddrssVw.isHidden = true
                            self.dismissBtn.isHidden = false
                            
                        }
                    }
                }
                
                let Marker = GMSMarker(position: self.locationMarker!.position)
                
                locationMarker = Marker
                infoWindow.removeFromSuperview()
                infoWindow = loadNiB()
                
                guard let location = locationMarker?.position else {
                        print("locationMarker is nil")
                        return
                }
                // Pass the spot data to the info window, and set its delegate to self
               
                infoWindow.delegate = self
                // Configure UI properties of info window
                infoWindow.alpha = 0.9
                infoWindow.layer.cornerRadius = 12
                
                infoWindow.center = mapVw.projection.point(for: location)
                infoWindow.frame.origin.y -= 210
                
                infoWindow.reserveVw.layer.shadowColor = UIColor.lightGray.cgColor
                infoWindow.reserveVw.layer.shadowOffset = CGSize(width:0.0, height: 1.5 )
                infoWindow.reserveVw.layer.shadowOpacity = 3.0
                infoWindow.reserveVw.layer.shadowRadius = 0.0
                infoWindow.reserveVw.layer.masksToBounds = false
                infoWindow.reserveVw.layer.cornerRadius = infoWindow.reserveVw.frame.size.height / 2
//                self.mapVw.selectedMarker?.iconView = infoWindow
                
                if deviceListNewArr.count != 0 {
                    
                    self.deviceDict = (deviceListNewArr[0] as? NSDictionary)!
                    infoWindow.scooterNUmLbl.text = deviceDict["scooter_no"] as? String ?? ""
                    infoWindow.scooterPerLbl.text = deviceDict["battery_range"] as? String ?? ""
                    infoWindow.scooterAdressLbl.text = deviceDict["device_address"] as? String ?? ""
                    // self.distanceKMLbl.text = String(format: "%@ KM",deviceDict["distance"] as? String ?? "")
                    self.pricingLbl.text = String(format: "%@ %@ %@",(L102Language.AMLocalizedString(key: "pricing", value: "")),deviceDict["ride_price"] as? String ?? "",(L102Language.AMLocalizedString(key: "/Min", value: "")))
                    infoWindow.reserveLbl.text = String(format: "RESERVE %@",deviceDict["reserve_price"] as? String ?? "")
                    self.deviceId = deviceDict["device_id"] as? String ?? ""
                    self.markerLat = deviceDict["lat"] as? String ?? ""
                    self.markerLon = deviceDict["lon"] as? String ?? ""
                    
                    print("Marker data : \(self.deviceDict)")
                    
                    
                    let origin = "\(self.currentLattitude),\(self.currentLongitude)"
                    let destination = "\(self.markerLat),\(self.markerLon)"
                    
                    let url = "https://api.tomtom.com/routing/1/calculateRoute/\(origin):\(destination)/json?sectionType=pedestrian&key=NoRhgk7zl6CWRYEfroITth20hp9vvLC4"
                    
                    print("URl : \(url)")
                    
                    ServiceLayer.request(url).responseJSON { response in
                        
                        let result = response.result.value
                        
                        let json = result as? NSDictionary
                        
                        print("Json : \(json)")
                        
                        if let rArr = json?["routes"] as? [[String : Any]] {
                            
                            self.deviceListArr = (rArr as NSArray) as! [[String : Any]]
                            
                            print("Routes : \(self.deviceListArr)")
                            
                            if self.deviceListArr.count > 0 {
                                
                                let address : NSDictionary = self.deviceListArr[0] as NSDictionary
                                self.legsArr = (address["legs"]  as! [[String : Any]])
                                let coordinatesDict : NSDictionary = self.legsArr[0] as NSDictionary
                                let summryDict = coordinatesDict["summary"] as! NSDictionary
                                let distance = summryDict["lengthInMeters"] as? Int ?? 0
                                print("distancemeterssssss: \(distance)")
                                
                                let distanceKilometer = Double(distance)
                                let length = (distanceKilometer)/1000
                                let kilometers:Double = Double(length)
                                print("b: \(kilometers)") // b: 1.500000
                                print("distancemeterssssss: \(length)")
                                
                                self.infoWindow.distanceKMLbl.text = "\(String(format: "%.2f", kilometers)) km"
                                //String(format: "%@ km", kilometers)
                                
                                print("Routes Arr : \(self.deviceListArr)")
                                
                                print("Legs Arr : \(self.legsArr)")
                                
                                print("Coordinates Dict : \(coordinatesDict)")
                                
                                
                            } else {
                                
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "can't_find_way", value: "")))
                            }
                            
                            return
                        } else {
                            
                            self.distanceKMLbl.text = (L102Language.AMLocalizedString(key: "no_route_found", value: ""))
                        }
                    }
                }
                
                
                self.mapVw.addSubview(infoWindow)
                
                let origin = "\(self.currentLattitude),\(self.currentLongitude)"
                let destination = "\(self.nearestmarkerlatt),\(self.nearestMarkerlon)"
                //
                let url = "https://api.tomtom.com/routing/1/calculateRoute/\(origin):\(destination)/json?sectionType=pedestrian&key=NoRhgk7zl6CWRYEfroITth20hp9vvLC4"
                
                // let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBXF_hdwpP_JPI0KTbFgnif3SJtn7_IA-w"
                //
                print("URl : \(url)")
                
                ServiceLayer.request(url).responseJSON { response in
                    
                    let result = response.result.value
                    if result == nil{
                        return
                    }
                    
                    let json = result as! NSDictionary
                    
                    print("Json : \(json)")
                    
                    if let rArr = json["routes"] as? [[String : Any]] {
                        
                        self.deviceListArr = (rArr as NSArray) as! [[String : Any]]
                        
                        print("Routes : \(self.deviceListArr)")
                        
                        if self.deviceListArr.count > 0 {
                            
                            let address : NSDictionary = self.deviceListArr[0] as NSDictionary
                            self.legsArr = (address["legs"]  as! [[String : Any]])
                            let coordinatesDict : NSDictionary = self.legsArr[0] as NSDictionary
                            
                            let points =  coordinatesDict["points"] as! [[String : Any]]
                            print("points Arr : \(points)")
                            
                            print("Routes Arr : \(self.deviceListArr)")
                            
                            print("Legs Arr : \(self.legsArr)")
                            
                            print("Coordinates Dict : \(coordinatesDict)")
                            
                            let path = GMSMutablePath()
                            var polyline = GMSPolyline()
                            for (_,coordinater) in points.enumerated() {
                                
                                //                    print("BEGIN:SAZILE====>ZONEDATA:\(index)")
                                //                    print("BEGIN:SAZILE====>ZONEDATA:\(coordinater)")
                                
                                //                                let latt = coordinater["latitude"] as? String ?? ""
                                //                                let lonn = coordinater["longitude"] as? String ?? ""
                                //
                                self.lattiude = (coordinater["latitude"])  as? Double ?? 0.0
                                self.longitude = (coordinater["longitude"]) as? Double ?? 0.0
                                
                                path.add(CLLocationCoordinate2D(latitude: self.lattiude, longitude: self.longitude))
                                
                            }
                            polyline = GMSPolyline(path: path)
                            polyline.strokeWidth = 4.0
                            polyline.strokeColor = .blue
                            polyline.map = self.mapVw
                            
                            let userMarker = GMSMarker()
                            userMarker.position = CLLocationCoordinate2D(latitude: self.user_polyline_lat, longitude: self.user_polyline_lng)
                            userMarker.map = self.mapVw
                            
                            // End location
                            let storeMarker = GMSMarker()
                            CATransaction.begin()
                            CATransaction.setAnimationDuration(2.0)
                            storeMarker.position = CLLocationCoordinate2D(latitude: self.store_polyline_lat, longitude: self.store_polyline_lng)
                            // storeMarker.icon = UIImage (named: "map")
                            storeMarker.iconView = self.frameForMarker()
                            storeMarker.map = self.mapVw
                            
                        } else {
                            
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "can't_find_way", value: "")))
                        }
                        
                        
                        return
                    }
                }
            }
        }
    }
    
    @IBAction func onTapScooterLockBtn(_ sender: UIButton) {
        
        let ppvc = PopUpViewController.getInstance()
        let navigationController = UINavigationController(rootViewController: ppvc)
        navigationController.modalPresentationStyle = .fullScreen
        isForEndRideBool = true
        self.present(navigationController, animated: true, completion: nil)
        
        
        //        DispatchQueue.main.async {
        //            if self.zoneType == "1" {
        //
        //
        //
        //            } else if self.zoneType == "0" {
        //
        //                self.zoneVw.isHidden = false
        //
        //            } else if  self.zoneType == "2" {
        //
        //                self.dangerVw.isHidden = false
        //            }
        //        }
    }
    
    @IBAction func onTapDismissBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            self.dismissBtn.isHidden = true
            self.eleSootorVw.isHidden = true
            self.scootorDetailsVw.isHidden = true
            // self.currentAddrssVw.isHidden = false
        }
        
    }
    
    @IBAction func onTapReserveBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.zoneType = self.deviceDict["zone_type"] as? String ?? ""
            self.onlineStr = self.deviceDict["online_status"] as? String ?? ""
            print("ONLINE STATUS =====> \(self.onlineStr)")
            if let batteryStr = self.deviceDict["battery_range"] as? String {
                
                var dummyStr : String = batteryStr
                dummyStr.remove(at: dummyStr.index(before: dummyStr.endIndex))
                print("Dummy Str \(dummyStr)")
                let myString : String = dummyStr
                let int: Int? = Int(myString)
                
                if self.zoneType == "0" && int! <= 11 || self.zoneType == "1" && int! <= 11 || self.onlineStr == "off"{
                    
                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                        self.eleSootorVw.isHidden = true
                        self.scootorDetailsVw.isHidden = true
                    }
                    
                } else if self.zoneType == "1" && int! >= 11  {
                    
                    self.btn.isHidden = false
                    self.reserveScooter()
                    
                }
                    
                else if self.zoneType == "2" {
                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "red_zone", value: "")), buttonTitle: "Ok") {
                        self.eleSootorVw.isHidden = true
                        self.scootorDetailsVw.isHidden = true
                    }
                } else if self.zoneType == "0" {
                    
                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "device_offline", value: "")), buttonTitle: "Ok") {
                        self.eleSootorVw.isHidden = true
                        self.scootorDetailsVw.isHidden = true
                    }
                }
            }
            
        }
        
    }
    
    @IBAction func onTapReportMissingBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            let riVC = ReportViewController.getInstance()
            self.present(riVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapRideBtn(_ sender: Any) {
        DispatchQueue.main.async {
            let ppvc = PopUpViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            navigationController.modalPresentationStyle = .fullScreen
            isForScooterStopadmin = false
            self.present(navigationController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func onTapMenuBtn(_ sender: UIBarButtonItem) {
        
        DispatchQueue.main.async {
            
            self.showLeftViewAnimated(nil)
            
            NotificationCenter.default.post(name: Notification.Name(NOTIFICATIONREFRESH), object: nil)
            
            // self.getNotificationsCountAPI()
        }
    }
    
    //MARK: MARKER DATA
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.scootorDetailsVw.isHidden = true
        self.reserveBtn.isHidden = true
        
        if self.isRiding != "1" || self.isReserverd != "1" {
            var markerData : NSDictionary?
            if let data = marker.userData! as? NSDictionary {
                markerData = data
            }
            locationMarker = marker
            infoWindow.removeFromSuperview()
            infoWindow = loadNiB()
            
            guard let location = locationMarker?.position else {
                    print("locationMarker is nil")
                    return false
            }
            // Pass the spot data to the info window, and set its delegate to self
            infoWindow.spotData = markerData
            infoWindow.delegate = self
            // Configure UI properties of info window
            infoWindow.alpha = 0.9
            infoWindow.layer.cornerRadius = 12
            
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.frame.origin.y -= 210
            
            infoWindow.reserveVw.layer.shadowColor = UIColor.lightGray.cgColor
            infoWindow.reserveVw.layer.shadowOffset = CGSize(width:0.0, height: 1.5 )
            infoWindow.reserveVw.layer.shadowOpacity = 3.0
            infoWindow.reserveVw.layer.shadowRadius = 0.0
            infoWindow.reserveVw.layer.masksToBounds = false
            infoWindow.reserveVw.layer.cornerRadius = infoWindow.reserveVw.frame.size.height / 2
    
            // ----------------------
            let data = marker.userData
            self.dismissBtn.isHidden = false
            self.eleSootorVw.isHidden = false
          //  self.scootorDetailsVw.isHidden = false
            
            // self.currentAddrssVw.isHidden = true
            
            
            
            if let deviceData = data {
                
                self.deviceDict = (deviceData as? NSDictionary)!
                infoWindow.scooterNUmLbl.text = deviceDict["scooter_no"] as? String ?? ""
                infoWindow.scooterPerLbl.text = deviceDict["battery_range"] as? String ?? ""
                infoWindow.scooterAdressLbl.text = deviceDict["device_address"] as? String ?? ""
                // self.distanceKMLbl.text = String(format: "%@ KM",deviceDict["distance"] as? String ?? "")
                self.pricingLbl.text = String(format: "%@ %@ %@",(L102Language.AMLocalizedString(key: "pricing", value: "")),deviceDict["ride_price"] as? String ?? "",(L102Language.AMLocalizedString(key: "/Min", value: "")))
                infoWindow.reserveLbl.text = String(format: "RESERVE %@",deviceDict["reserve_price"] as? String ?? "")
                self.deviceId = deviceDict["device_id"] as? String ?? ""
                self.markerLat = deviceDict["lat"] as? String ?? ""
                self.markerLon = deviceDict["lon"] as? String ?? ""
                
                print("Marker data : \(self.deviceDict)")
                
                
                let origin = "\(self.currentLattitude),\(self.currentLongitude)"
                let destination = "\(self.markerLat),\(self.markerLon)"
                
                let url = "https://api.tomtom.com/routing/1/calculateRoute/\(origin):\(destination)/json?sectionType=pedestrian&key=NoRhgk7zl6CWRYEfroITth20hp9vvLC4"
                
                print("URl : \(url)")
                
                ServiceLayer.request(url).responseJSON { response in
                    
                    let result = response.result.value
                    
                    let json = result as? NSDictionary
                    
                    print("Json : \(json)")
                    
                    if let rArr = json?["routes"] as? [[String : Any]] {
                        
                        self.deviceListArr = (rArr as NSArray) as! [[String : Any]]
                        
                        print("Routes : \(self.deviceListArr)")
                        
                        if self.deviceListArr.count > 0 {
                            
                            let address : NSDictionary = self.deviceListArr[0] as NSDictionary
                            self.legsArr = (address["legs"]  as! [[String : Any]])
                            let coordinatesDict : NSDictionary = self.legsArr[0] as NSDictionary
                            let summryDict = coordinatesDict["summary"] as! NSDictionary
                            let distance = summryDict["lengthInMeters"] as? Int ?? 0
                            print("distancemeterssssss: \(distance)")
                            
                            let distanceKilometer = Double(distance)
                            let length = (distanceKilometer)/1000
                            let kilometers:Double = Double(length)
                            print("b: \(kilometers)") // b: 1.500000
                            print("distancemeterssssss: \(length)")
                            
                            self.infoWindow.distanceKMLbl.text = "\(String(format: "%.2f", kilometers)) km"
                            //String(format: "%@ km", kilometers)
                            
                            print("Routes Arr : \(self.deviceListArr)")
                            
                            print("Legs Arr : \(self.legsArr)")
                            
                            print("Coordinates Dict : \(coordinatesDict)")
                            
                            
                        } else {
                            
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "can't_find_way", value: "")))
                        }
                        
                        return
                    } else {
                        
                        self.distanceKMLbl.text = (L102Language.AMLocalizedString(key: "no_route_found", value: ""))
                    }
                }
            }
            self.mapVw.addSubview(infoWindow)
            self.mapVw.selectedMarker = marker
            return true
        }
        return false
    }
   
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.frame.origin.y -= 210
        }
    }
        
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    
}

extension HomeViewController : CLLocationManagerDelegate{
    
    //MARK: - CLLOCATIONMANAGER DELEGATE
     /* func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         
         print("Did Update Locations")
         
         let location: CLLocation = locations.last!
         print("Location: \(location)")
         
         if location != nil {
         
         //if !didFindLocation {
         
         print("didUpdateLocations called successfully")
         
         //didFindLocation = true
         //CATransaction.begin()
         //CATransaction.setValue(10.0, forKey: kCATransactionAnimationDuration)
         let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
         longitude: location.coordinate.longitude,
         zoom: 17.0)
         mapVw.animate(to: camera)
         
         lat = CGFloat(location.coordinate.latitude)
         lon = CGFloat(location.coordinate.longitude)
         
         currentLattitude = NSString(format: "%.6f", lat)
         currentLongitude = NSString(format: "%.6f", lon)
         
         let clLocationCoOrdinate = CLLocationCoordinate2D(latitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude))
         getAddressFromCoOrdinates(clLocationCoOrdinate)
         
         //getAddressFromLatLon(pdblLatitude: currentLattitude as String, pdblLongitude: currentLongitude as String)
         
         //CATransaction.commit()
         //}
         }
         }*/
//
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//
//        setUpLocationPermissions()
//
//    }




    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //  var locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //  print("locations = \(locValue.latitude) \(locValue.longitude)"
       // determineMyCurrentLocation()
        setUpLocationPermissions()
        print("Did Update Locations")
        
        let location: CLLocation = locations.last!
        print("Location: \(location)")

        if location != nil {
        
        //if !didFindLocation {
        
        print("didUpdateLocations called successfully")
        
        //didFindLocation = true
        //CATransaction.begin()
        //CATransaction.setValue(10.0, forKey: kCATransactionAnimationDuration)
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
        longitude: location.coordinate.longitude,
        zoom: 17.0)
        mapVw.animate(to: camera)
    //    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Update Location")

        }
        //        let newLocation = locations.last
        //
        //        mapVw.camera = GMSCameraPosition.camera(withTarget: newLocation!.coordinate, zoom: 14.0) // show your device location on map
        //
        //
        //        print("DETAREMINECURRENT LOCATION: ======> \(manager)")
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
//                   //check  location permissions
//            self.requestWhenInUseAuthorization()
//               }
        //  UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Update Location")
       // setUpLocationPermissions()
       // requestWhenInUseAuthorization()
       // determineMyCurrentLocation()
    }
    
   

    //MARK:  Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

    switch status {

    case .restricted:
    print("Location access was restricted.")
        
    case .denied:
    print("User denied access to location.")
    // Display the map using the default location.
    mapVw.isHidden = false
    setUpLocationPermissions()
    case .notDetermined:
    print("Location status not determined.")
     setUpLocationPermissions()
    case .authorizedAlways:
    manager.startUpdatingLocation()

    case .authorizedWhenInUse:
    print("Location status is OK.")
    determineMyCurrentLocation()

    @unknown default:
    fatalError()
    
    }
    }
 
}

//MARK: - DEVICE LIST API
extension HomeViewController {
    
    @objc func getNearByDevicesAPI(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(NEARBYDEVICES)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "nearby_range" : "",
                          "lat" : currentLattitude,
                          "lon" : currentLongitude
                
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            // self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Near By Device Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                if let data = json as? [String: Any] {
                                    
                                    self.userDetailsDict = (data["user_details"] as! NSDictionary)as! [String : Any]
                                    
                                    print(self.userDetailsDict)
                                    
                                    UserDefaults.standard.set(self.userDetailsDict, forKey: "LOGIN_DETAILS_RESPONSE")
                                    
                                    NotificationCenter.default.post(name: Notification.Name(REFRESHPROFILEDATA), object: nil)
                                }
                                
                                if let data = json["device_list"] as? [[String : Any]] {
                                    
                                    self.deviceListArr = data
                                    UserDefaults.standard.set(self.deviceListArr, forKey: "MARKER_DATA")
                                    
                                    self.olddeviceListArr = self.deviceListArr
                                    self.initializeDeviceLocations()
                                }
                                
                                if let reserved = json["is_reserved"] as? String {
                                    
                                    self.isReserverd = reserved
                                    self.getZoneDataApI()
                                    if self.isReserverd == "1" {
                                        
                                        if istimerAgainBool {
                                            
                                            self.rideVw.isHidden = true
                                            self.serachBarVw.isHidden = true
                                            self.timerAgainVw.isHidden = true
                                            self.backgroundBtn.isHidden = true
                                            
                                        } else {
                                            
                                            self.rideVw.isHidden = false
                                            self.serachBarVw.isHidden = false
                                            self.timerAgainVw.isHidden = false
                                            self.backgroundBtn.isHidden = false
                                            
                                            
                                        }
                                        self.reportBtn.isHidden = true
                                        self.reserveVw.isHidden = true
                                        // self.timerAgainVw.isHidden = false
                                        // self.backgroundBtn.isHidden = false
                                        self.timerVw.isHidden = false
                                        self.reservingVw.isHidden = true
                                        
                                        if let resDeviceDict = json["reserved_device"] as? [String : Any] {
                                            
                                            // print("Reserved Device Data : \(self.resDeviceDict)")
                                            
                                            self.reservedDeviceDict = resDeviceDict as NSDictionary
                                            
                                            print("Reserved Device Data : \(self.reservedDeviceDict)")
                                            if let Reserved = self.reservedDeviceDict["reserved_timer_remain"] as? String {
                                                
                                                // let timeArr = "02:30"
                                                let strArr = Reserved.components(separatedBy: ":")
                                                let minsStr = strArr[0]
                                                let secsStr = strArr[1]
                                                let valueMin:Int = Int(minsStr) ?? 0
                                                let valueSec:Int = Int(secsStr) ?? 0
                                                self.totalTime = valueMin * 60 + valueSec
                                                self.limitTimerLbl.text = String(self.totalTime)
                                                self.timerLbl.text = String(self.totalTime)
                                                print("Reserved Time :\(self.totalTime)")
                                                //print("remainingtime : \(self.limitTimerLbl.text)")
                                                self.startTimer()
                                                
                                            }
                                        }
                                        
                                    } else {
                                        
                                        self.reportBtn.isHidden = false
                                        self.rideVw.isHidden = false
                                        self.serachBarVw.isHidden = false
                                        
                                    }
                                }
                                
                                if let riding = json["is_riding"] as? String {
                                    
                                    self.isRiding = riding
                                    
                                    if self.isRiding == "1" {
                                        
                                        self.scooterBookingVw.isHidden = false
                                        self.serachBarVw.isHidden = true
                                        self.rideVw.isHidden = true
                                        
                                        self.reserveVw.isHidden = true
                                        self.reportImgVw.isHidden = false
                                        self.reportBtn.isHidden = false
                                        if let scooterNumber = self.deviceListArr[0]["scooter_no"] as? String {
                                            self.numberPlateLbl.text = scooterNumber
                                            
                                            print("Scooter Number : \(scooterNumber)")
                                        }
                                        
                                        if let ridDeviceDict = json["riding_device"] as? [String : Any] {
                                            
                                            self.ridingDeviceDict = ridDeviceDict as NSDictionary
                                            
                                            print("Riding Device Data : \(self.ridingDeviceDict)")
                                            
                                            self.engineRiding = self.ridingDeviceDict["engine_status"] as? String ?? ""
                                            
                                            if self.engineRiding == "Off" {
                                                
                                                self.engineStartingTimer()
                                                
                                            } else {
                                                
                                                self.engineStarting?.invalidate()
                                                self.engineStarting = nil
                                                
                                            }
                                            
                                            if let batteryRange = self.ridingDeviceDict["battery_range"] as? String {
                                                self.batteryPercentageLbl.text = batteryRange
                                                print("Battery Range : \(batteryRange)")
                                            }
                                            
                                            if let speed = self.ridingDeviceDict["speed"] as? String {
                                                self.distnceKmLbl.text = String(format: "%@ KM/HR",speed)
                                                print("Scooter speed : \(speed)")
                                            }
                                            
                                            self.zoneType = ridDeviceDict["zone_type"] as? String ?? ""
                                            if  self.zoneType == "1" {
                                                self.zoneTimer()
                                                self.dangerVw.isHidden = true
                                                self.zoneBtn.isHidden = true
                                                self.zoneVw.isHidden = true


                                            } else if self.zoneType == "0"{
                                                
                                                if self.isFromDismissBool {
                                                    
                                                    self.zoneVw.isHidden = true
                                                    self.zoneBtn.isHidden = true
                                                    
                                                } else {
                                                    
                                                    self.zoneVw.isHidden = false
                                                    self.zoneBtn.isHidden = false
                                                    
                                                }
                                               
                                                //self.mapVw.settings.setAllGesturesEnabled(false)
                                                // self.zoneVw.isHidden = true
                                                self.zoneTimer()
                                            } else if self.zoneType == "2" {
                                                
                                                if self.isFromDismissBool {
                                                    self.dangerVw.isHidden = true
                                                    self.zoneBtn.isHidden = true
                                                    //self.mapVw.settings.setAllGesturesEnabled(false)
                                                   // self.zoneTimer()

                                                    
                                                    
                                                } else {
                                                    self.dangerVw.isHidden = false
                                                    self.zoneBtn.isHidden = false
                                                    //self.mapVw.settings.setAllGesturesEnabled(false)
                                                   // self.zoneTimer()
                                                    
                                                }
                                                
                                                 self.zoneTimer()
                                                
                                            }
                                            
                                        }
                                        
                                    } else {
                                        
                                        // Show Search & QR Code Images, Buttons
                                        self.reportBtn.isHidden = false
                                        // self.serachBarVw.isHidden = false
                                        // self.rideVw.isHidden  = false
                                        self.reserveVw.isHidden = false
                                    }
                                }
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    let defaults = UserDefaults.standard
                                    
                                    defaults.set(false, forKey: "IS_LOGGED_IN")
                                    defaults.synchronize()
                                    
                                    self.resetUserDefaults()
                                    
                                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                    appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                    
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Near By Devices Error:\(err.localizedDescription)")
                    
                }
            }
        }
    }
    
    //MARK: - BOOK DEVICE SCOOTER API
    
    func getReservescooterAPI(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(BOOK_DEVICE_SCOOTOR)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "qr_code": deviceId,
                          "card_id": cardId
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            //self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Reserve Scooter Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            self.reserveVw.isHidden = true
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.getNearByDevicesAPI()
                                
                                //                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
                                //
                                //                                }
                                
                                //   self.deviceMarker.map = nil
                                //    self.mapVw.clear()
                                
                                if let data = json["device_list"] as? [[String : Any]] {
                                    
                                    self.deviceListArr = data
                                    self.olddeviceListArr = self.deviceListArr
                                }
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                        self.reservingVw.isHidden = true
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    self.reserveVw.isHidden = true
                    print("Reserve Scooter Error:\(err.localizedDescription)")
                    
                }
            }
        }
    }
    
    //MARK: -  Initialize Store Locations
    
    func initializeDeviceLocations() {
        
        var bounds = GMSCoordinateBounds()
        var location = CLLocationCoordinate2D()
        
        mapVw.clear()
        
        for i in 0..<deviceListArr.count {
            
            let dictionary = deviceListArr[i]
            
            let lat = Double(dictionary["lat"] as! String)
            let lon = Double(dictionary["lon"] as! String)
            
            location.latitude = lat ?? 0.00
            location.longitude = lon ?? 0.00
            
            // Creates a marker in the center of the map.
            deviceMarker = GMSMarker()
            
            deviceMarker.iconView = backGroundimgFrame(dictionary)
            deviceMarker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude)
            bounds = bounds.includingCoordinate(deviceMarker.position)
            
            //            if let value = dictionary["address"] {
            //
            //                print("BEGIN:Sazile ======> Address : \(value)")
            //            }
            
            deviceMarker.userData = deviceListArr[i]
            deviceMarker.map = mapVw
            deviceMarker.zIndex = Int32(Int(UInt32(i)))
        }
        
    }
    
    //MARK: - backGroundimgFrame
    func backGroundimgFrame(_ dict: [AnyHashable : Any]?) -> UIView? {
        
        let markerBgVw = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
        if let isDeviceOpen = dict?["battery_range"] as? String {
            
            var dummyStr : String = isDeviceOpen
            dummyStr.remove(at: dummyStr.index(before: dummyStr.endIndex))
            
            print("Dummy Str \(dummyStr)")
            let myString : String = dummyStr
            let int: Int? = Int(myString)
            
            // Successfully converted String to Int
            
            markerBgVw.layer.cornerRadius = 40
            markerBgVw.clipsToBounds = true
            
            let markerImgVw = UIImageView(frame: CGRect(x: 10, y: 0, width: 80, height: 80))
            
            markerBgVw.addSubview(markerImgVw)
            
            if int! >= 11 {
                
                markerImgVw.image = UIImage(named: "map")
                
            } else {
                
                markerImgVw.image = UIImage(named: "map-marker1")
            }
            
            
        }
        return markerBgVw
    }
    
    func frameForMarker() -> UIView? {
        
        let markerBgVw = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
        markerBgVw.layer.cornerRadius = 40
        markerBgVw.clipsToBounds = true
        
        let markerImgVw = UIImageView(frame: CGRect(x: 25, y: 25, width: 80, height: 80))
        
        markerBgVw.addSubview(markerImgVw)
        
        markerImgVw.image = UIImage(named: "map")
        
        return markerBgVw
    }
}


//MARK: - ZONE DATA API

extension HomeViewController{
    
    @objc func getZoneDataApI() {
        
        if Reachability.isNetworkAvailable(){
            
            
            let urlString = "\(BASEURL)\(ZONE_DATA)"
            
            let params = ["user_id": userId,
                          "token_key" : tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
         //   self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print("Zone Data Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json["zone_data"] as? [[String : Any]] {
                                    self.zonedataArr = data
                                    
                                    self.getZoneData()
                                }
                                
                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        let defaults = UserDefaults.standard
                                        
                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()
                                        
                                        self.resetUserDefaults()
                                        
                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                        
                                    } else {
                                        
                                        //  UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Zone Data Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
    //mark: notificationsAPI
    
    @objc func getNotificationsCountAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(NOTIFICATIONS_COUNT)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          
                          
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Notifications count Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            print(err)
                            //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                
                                if let data = json as? [String : Any] {
                                    
                                    notificationsCount = data
                                    
                                    if let notificationStr = notificationsCount["total_notification"] as? String {
                                        
                                        if notificationStr == "0" {
                                            
                                            self.menuBtn.badge(text: nil)
                                            
                                        }else{
                                            
                                            self.menuBtn.badge(text: notificationStr)
                                            
                                        }
                                    }
                                }
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        let defaults = UserDefaults.standard
                                        
                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()
                                        
                                        self.resetUserDefaults()
                                        
                                        notificationTimer?.invalidate()
                                        notificationTimer = nil

                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                        
                                    } else {
                                        
                                        //  UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Notifications count Error:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
            //UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
        }
    }
    /*
     @objc func getNotificationsTimeAPI() {
     
     if Reachability.isNetworkAvailable(){
     
     let urlString = "\(BASEURL)\(NOTIFICATION_TIME)"
     
     let params = ["user_id": userId,
     "token_key": tokenKey,
     
     
     ]
     
     #if DEBUG
     
     print("BEGIN SAZILE======>URLSTRING" + urlString)
     print("BEGIN SAZILE=====Parameters>\(params)")
     
     #endif
     
     
     ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
     for (key,values) in params{
     MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
     }
     }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
     
     switch result{
     
     case .success(let upload, _,_):
     
     upload.responseJSON { response in
     
     #if DEBUG
     
     print(" Notifications Time Response : \(response)")
     
     #endif
     
     if let err = response.error {
     
     print(err)
     UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
     
     return
     }
     
     if let result = response.result.value {
     
     let json = result as! NSDictionary
     
     let status_code : String = json["success"] as? String ?? ""
     
     if(status_code == "1") {
     
     
     } else {
     
     if let error = json["message"] as? String {
     
     ERROR_MESSAGE = error
     
     if ERROR_MESSAGE == INVALID_TOKEN_KEY {
     
     self.userSessionExpired()
     
     } else {
     
     UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
     }
     }
     }
     
     }
     }
     
     case .failure(let err):
     
     print("Notifications count Error:\(err.localizedDescription)")
     
     }
     }
     
     } else {
     //UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
     
     }
     }*/
    
    //MARK: - Card Data Saving Purpose
    
    func getCards() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(GET_CARDS_DEFULT)"
            
            let params = [  "user_id": userId,
                            "token_key": tokenKey
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key )
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Payment Card Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json["data"] as? [[String : Any]] {
                                    
                                    self.getCardsArr = data
                                    
                                    for index in self.getCardsArr {
                                        
                                        let dict = index as NSDictionary
                                        
                                        if let isDefaultCard = dict["is_default_card"] as? String {
                                            
                                            if isDefaultCard == "1" {
                                                print("IS DEFAULT CARD DICT : \(dict)")
                                                UserDefaults.standard.set(dict, forKey: "DEFAULT_CARD_DATA")
                                                
                                            } else {
                                                
                                                
                                            }
                                        }
                                    }
                                }
                                
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        let defaults = UserDefaults.standard
                                        
                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()
                                        
                                        self.resetUserDefaults()
                                        
                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                        
                                    } else {
                                        
                                        //UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                        }
                    }
                    
                case .failure(let err):
                    
                    print("cards Error:\(err.localizedDescription)")
                }
            }
            
        } else {
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))

          //  UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
    @objc func getTapCasBoxAp() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(TAP_CASE_BOX)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          
                          
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Tap case box Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            print(err)
                            //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                if self.massageID == 510 {
                                    
                                    self.view.makeToast ((L102Language.AMLocalizedString(key: "top_case_box", value: "")), duration: 2.0, position: .center, style: style)
                                    
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")

                                    
                                }


                                
                            } else {
                                
                                self.hideActivityIndicator()

                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        let defaults = UserDefaults.standard
                                        
                                        defaults.set(false, forKey: "IS_LOGGED_IN")
                                        defaults.synchronize()
                                        
                                        self.resetUserDefaults()
                                        
                                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                        appDelegate?.window?.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
                                        
                                    } else {
                                        
                                          UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            self.hideActivityIndicator()

                        }
                    }
                    
                case .failure(let err):
                    
                    print("Tap case box Error:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
            //UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
        }
    }
    
}





