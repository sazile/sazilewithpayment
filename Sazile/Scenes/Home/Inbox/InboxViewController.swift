//
//  InboxViewController.swift
//  Sazile
//
//  Created by Vodnala Venu on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class InboxViewController: UIViewController {
    
    @IBOutlet weak var inboxTableVw: UITableView!
    //MARK: -  OUTLETS
    var inboxArr = [String]()
    var refreshControl = UIRefreshControl()
    var tokenKey = String()
    var userId = String()
    var totalNTF = String()
    var alertMsgStr = String()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "inbox", value: ""))
        
        inboxTableVw.separatorStyle = .none
        layoutDesign()
        refreshControl.addTarget(self, action: #selector(refresh(sender: )), for: UIControl.Event.valueChanged)
           inboxTableVw.addSubview(refreshControl) // not required when using UITableViewController
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            let collectionData = data as! NSDictionary
            tokenKey = collectionData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = collectionData["user_id"] as? String ?? ""
            print(userId)
        }
        
      getNotificationsCountInboxAPI()
    layoutDesign()
        // self.navigationController?.setNavigationBarHidden(false, animated: ani)
        //self.navigationItem.hidesBackButton = false
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    static func getInstance() -> InboxViewController {
        
        let storyboard = UIStoryboard(name: "Inbox", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InboxViewController") as! InboxViewController
    }
    
    
    @objc func refresh(sender:AnyObject)
       
       {
           // Updating your data here...

           self.inboxTableVw.reloadData()
           self.refreshControl.endRefreshing()
       }
    
   func layoutDesign(){
        
        inboxArr = [L102Language.AMLocalizedString(key: "messages", value: ""),L102Language.AMLocalizedString(key: "alert_messages", value: "")]
    
           DispatchQueue.main.async {
//           self.inboxTableVw.delegate = self
//           self.inboxTableVw.dataSource = self
//           self.inboxTableVw.reloadData()
            
           }
        
    }
    
}


//MARK: - UITABLEVIEW DELEGATE & DATASOURCE
extension InboxViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inboxArr.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "InboxTableViewCell", for: indexPath) as! InboxTableViewCell

        cell.titleLbl.text = inboxArr[indexPath.row]
        
        if indexPath.row == 0 {
            
                    if totalNTF == "0" {
                        
                        cell.badgeVw.badge(text: nil)
                        
                    } else {
                        
                        cell.badgeVw.badge(text: totalNTF)
                    }
            
        }
        if indexPath.row == 1{
            
          //  if notificationsCount.count == 0 {
            
              
                    
                    if alertMsgStr == "0"{
                        
                        cell.badgeVw.badge(text: nil)
                    } else {
                        
                        cell.badgeVw.badge(text: alertMsgStr)
                    }
                }
                
           // }
        
        
        
        cell.selectionStyle = .none
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let mvc = MessageViewController.getInstance()
            navigationController?.pushViewController(mvc, animated: true)
            
        } else if indexPath.row == 1 {
            
            let cvc = CollectionViewController.getInstance()
            navigationController?.pushViewController(cvc, animated: true)
            
        }
    }
    
    @objc func getNotificationsCountInboxAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(NOTIFICATIONS_COUNT)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          
                          
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Notifications count Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            print(err)
                            //UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                
                                if let data = json as? [String : Any] {
                                    
                                    self.totalNTF = data["message_count"] as? String ?? ""
                                    self.alertMsgStr = data["alert_msg_count"] as? String ?? ""
                                    
                                    DispatchQueue.main.async {
                                        self.inboxTableVw.delegate = self
                                        self.inboxTableVw.dataSource = self
                                        self.inboxTableVw.reloadData()
                                        
                                    }
                                }
                            } else {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Notifications count Error:\(err.localizedDescription)")
                    
                }
            }
            
        } else {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
        }
    }
}
