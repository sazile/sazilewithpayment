//
//  InboxTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 26/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class InboxTableViewCell: UITableViewCell {

    @IBOutlet weak var badgeVw: UIView!
    @IBOutlet weak var cellBgVw: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellBgVw.layer.cornerRadius = 10
        cellBgVw.clipsToBounds = true
        cellBgVw.layer.shadowOffset = CGSize(width:1, height:1 )
        cellBgVw.center = self.center
        cellBgVw.layer.shadowRadius = 1
        cellBgVw.layer.shadowOffset = CGSize.zero
        cellBgVw.layer.shadowOpacity = 1
        cellBgVw.clipsToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
