//
//  MessageViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 01/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//Request Parameters
//user_id:1
//token_key:1xpMVOq00TOWcgwFKgZIAkGrxIK9lUzVTWxFiglqtKDvFdEKtN
//page:1
//limit:2



import UIKit

class MessageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var page = NSInteger()
    var tokenKey = String()
    var userId = String()
    var totalpages = NSInteger()
    var ispagesAvailable = Bool()
    var isFormPagenation = Bool()
    var documentsBool = Bool()
     var massageID = Int()
    var notificationsArr : [[String: Any]] = []
    
    var dateStr = String()
    var massageStr = String()
    
    //MARK: -  OUTLETS
    @IBOutlet weak var messageTblVw: UITableView!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        messageTblVw.rowHeight = UITableView.automaticDimension
        messageTblVw.separatorStyle = .none
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "messages", value: ""))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        //documentsBool = false
        page = 1
        isFormPagenation = false
        ispagesAvailable = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let messeagesData = data as! NSDictionary
            
            tokenKey = messeagesData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = messeagesData["user_id"] as? String ?? ""
            print(userId)
        }
        getmesseages()
        
    }
    
    static func getInstance() -> MessageViewController {
        
        let storyboard = UIStoryboard(name: "Message", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
    }
    
    
    //MARK: - TABLEVIEW DELEGATE DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell", for: indexPath) as! MessageTableViewCell

        
        let dateStr = notificationsArr[indexPath.row]["created_date"] as? String ?? ""
        let strArr = dateStr.components(separatedBy: " ")
        let date1 = strArr[0]
        cell.dateLbl.text = date1
        print("MASSAGE ID======> \(self.massageID)")
//        if self.massageID == 78 {
//
//            cell.verifiedLbl.text = (L102Language.AMLocalizedString(key: "documents_verified", value: ""))
//
//        } else {
//
//            cell.verifiedLbl.text = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")
//
//        }
        cell.verifiedLbl.text = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")
        cell.selectionStyle = .none
        cell.riskZoneVw.layer.cornerRadius = 5
        cell.riskZoneVw.clipsToBounds = true
        cell.riskZoneVw.layer.shadowOffset = CGSize(width:2, height:2 )
        cell.riskZoneVw.layer.shadowOpacity = 1.0
        cell.riskZoneVw.layer.shadowOffset = CGSize.zero
        cell.riskZoneVw.layer.shadowRadius = 1
        cell.riskZoneVw.clipsToBounds = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.massageID == 78 {
//          massageStr = (L102Language.AMLocalizedString(key: "documents_verified", value: ""))
            massageStr = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")
            let dVF = DocuVfViewController.getInstance()
            dVF.massageStr = self.massageStr
            print(massageStr)
            self.navigationController?.pushViewController(dVF, animated: true)
        } else {
            
            massageStr = String(format: "%@",notificationsArr[indexPath.row]["text"] as? String ?? "")
            let dVF = DocuVfViewController.getInstance()
            dVF.massageStr = self.massageStr
            print(massageStr)
            self.navigationController?.pushViewController(dVF, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == notificationsArr.count - 1 {
            
            if totalpages == 1 {
                
                ispagesAvailable = false
            }
            
            if ispagesAvailable == true {
                
                page = page + 1
                isFormPagenation = true
                
                
                if totalpages == page {
                    
                    ispagesAvailable = false
                    
                }
                
                getmesseages()
                
            }
        }
        
    }
}

//MARK: - MESSAGE LIST API
extension MessageViewController{
    func getmesseages() {
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(MESSEAGE)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "page" : String(format: "%ld", Int(page)),
                "limit" : String(format: "%ld", Int(10))
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" messages Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID = json["message_id"] as? Int ?? 0

                                self.totalpages = json["total_page"] as! NSInteger
                                
                                if self.isFormPagenation {
                                    
                                    if let data = json["data"] as? [[String : Any]] {
                                        
                                        self.notificationsArr.append(contentsOf: data)
                                        
                                        DispatchQueue.main.async {
                                            
                                            self.messageTblVw.reloadData()
                                        }
                                    }
                                    
                                } else {
                                    
                                    if let data = json["data"] as? [[String : Any]] {
                                        self.notificationsArr = data
                                        
                                        DispatchQueue.main.async {
                                            self.messageTblVw.delegate = self
                                            self.messageTblVw.dataSource = self
                                            self.messageTblVw.reloadData()
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                            }else{
                                if !self.isFormPagenation {
                                    
                                }
                                
                                self.ispagesAvailable = false
                                self.page = self.page - 1
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "No_messages", value: "")))
                                        //UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("messages Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
    
}


