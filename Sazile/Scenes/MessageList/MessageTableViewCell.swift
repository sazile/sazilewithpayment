//
//  MessageTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 16/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var successLbl: UILabel!
    @IBOutlet weak var verifiedLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var riskZoneVw: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
