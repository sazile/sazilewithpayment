//
//  PaymentViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import Stripe
class PaymentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var paymentTblVw: UITableView!
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paymentTblVw.delegate = self
        paymentTblVw.dataSource = self
        paymentTblVw.reloadData()

    }
    
    //MARK:- TABLE VIEW DELEGATE DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentViewTableViewCell") as! PaymentViewTableViewCell
        
           cell.selectionStyle = .none
           cell.paymenCpteVw.layer.cornerRadius = 10
           cell.paymenCpteVw.clipsToBounds = true
          
           cell.paymenCpteVw.layer.shadowOffset = CGSize(width:2.0, height:2.0 )
           cell.paymenCpteVw.layer.shadowOpacity = 1.0
           cell.paymenCpteVw.layer.shadowRadius = 3
           cell.paymenCpteVw.clipsToBounds = false
        
           return cell
    }
   
}
