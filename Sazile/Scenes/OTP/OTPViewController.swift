//
//  OTPViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 24/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import FirebaseAuth
import Alamofire
import Firebase


class OTPViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var didnotReceiveLbl: UILabel!
    @IBOutlet weak var enterDigitLbl: UILabel!
    @IBOutlet weak var verificationLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var SixthTf: UITextField!
    @IBOutlet weak var FifthTf: UITextField!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet var firstTF: UITextField!
    @IBOutlet var secondTF: UITextField!
    @IBOutlet var thirdTF: UITextField!
    @IBOutlet var fourthTF: UITextField!
    
    var responseData : [String : Any] = [:]
    var userId = String()
    var tokenKey = String()
    var otpStr = String()
    
    var firVerificationId = String()
    var firebaseToken = String()
    var isForForgotPassword = Bool()
    var mobileNumberStr = String()
    var VERIFICATIONID = String()
    var countryCode = String()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        self.didnotReceiveLbl.textAlignment = .center
        //        resendOTPBtn.contentHorizontalAlignment = .center
        self.verificationLbl.text = (L102Language.AMLocalizedString(key: "verification_title", value: ""))
        self.enterDigitLbl.text = (L102Language.AMLocalizedString(key: "enter_4_digit_code", value: ""))
        //self.phoneNumberLbl.text = (L102Language.AMLocalizedString(key: "", value: ""))
        self.didnotReceiveLbl.text = (L102Language.AMLocalizedString(key: "didn_t_receive_otp", value: ""))
        self.verifyBtn.setTitle(L102Language.AMLocalizedString(key: "verify", value: ""), for: .normal)
        self.resendOTPBtn.setTitle(L102Language.AMLocalizedString(key: "resend_otp", value: ""), for: .normal)
        
        
        layoutDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.phoneNumberLbl.text = String(format: "%@ %@",(L102Language.AMLocalizedString(key: "OTP_code_send_to", value: "")),mobileNumberStr)
        print("MOBILE NUMBER: ===== > \(self.mobileNumberStr)")

        firstTF.textAlignment = .center
        secondTF.textAlignment = .center
        thirdTF.textAlignment = .center
        fourthTF.textAlignment = .center
        FifthTf.textAlignment = .center
        SixthTf.textAlignment = .center
        
        if (UserDefaults.standard.value(forKey: "CurrentLanguage") as? String) != nil {
            
            let currentLang : String = UserDefaults.standard.value(forKey: "CurrentLanguage") as? String ?? ""
            print("Current Language : \(currentLang)")
            
            if currentLang == "en" {
                
                didnotReceiveLbl.font = didnotReceiveLbl.font.withSize(17)
                
            } else {
                
                didnotReceiveLbl.font = didnotReceiveLbl.font.withSize(13)
                
            }
        }
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let loginData = data as! NSDictionary
            userId = loginData["user_id"] as? String ?? ""
            print(userId)
            tokenKey = loginData["token_key"] as? String ?? ""
            print(tokenKey)

        }
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    static func getInstance() -> OTPViewController {
        
        let storyboard = UIStoryboard(name: "OTP", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
    }
    
    func layoutDesign() {
        
        let imgVw = UIImage(named: "sazileLogo")
        let navLogo = UIImageView(image:imgVw)
        self.navigationItem.titleView = navLogo
        
        verifyBtn.layer.shadowColor = UIColor.lightGray.cgColor
        verifyBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        verifyBtn.layer.shadowOpacity = 3.0
        verifyBtn.layer.shadowRadius = 0.0
        verifyBtn.layer.masksToBounds = false
        verifyBtn.layer.cornerRadius = verifyBtn.frame.size.height / 2
        IQKeyboardManager.shared.enable = false
        
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        FifthTf.delegate = self
        SixthTf.delegate = self
        
        
        firstTF.becomeFirstResponder()
        
        firstTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        secondTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        thirdTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        fourthTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        FifthTf.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        SixthTf.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        
    }
    
    
    //MARK: - IB ACTIONS
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if  text?.count == 1 {
            
            switch textField {
                
            case firstTF:
                secondTF.becomeFirstResponder()
                //firstTF.backgroundColor = .black
                
            case secondTF:
                thirdTF.becomeFirstResponder()
                //secondTF.backgroundColor = .black
                
            case thirdTF:
                fourthTF.becomeFirstResponder()
                //thirdTF.backgroundColor = .black
                
            case fourthTF:
                FifthTf.becomeFirstResponder()
            //fourthTF.backgroundColor = .black
            case FifthTf:
                SixthTf.becomeFirstResponder()
            case SixthTf:
                SixthTf.becomeFirstResponder()
                //SixthTf.resignFirstResponder()
                
                
                otpStr = String(format: "%@%@%@%@%@%@", firstTF.text ?? "", secondTF.text ?? "", thirdTF.text ?? "", fourthTF.text ?? "",FifthTf.text ?? "",SixthTf.text ?? "")
                print(" Entered OTP textfieleds ========\(otpStr)")
                
            default:
                
                break
            }
        }
        
        if  text?.count == 0 {
            
            switch textField {
                
            case firstTF:
                firstTF.becomeFirstResponder()
                //firstTF.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
                
            case secondTF:
                firstTF.becomeFirstResponder()
                //secondTF.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
                
            case thirdTF:
                secondTF.becomeFirstResponder()
                //thirdTF.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
                
            case fourthTF:
                thirdTF.becomeFirstResponder()
            //fourthTF.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
            case FifthTf:
                fourthTF.becomeFirstResponder()
            case SixthTf:
                FifthTf.becomeFirstResponder()
                
            default:
                
                break
            }
            
        } else {
            
        }
    }
    
    @IBAction func otpBtn(_ sender: UIButton) {
        
        
        
        //let storyboard = UIStoryboard(name: "scotorLocation", bundle: nil)
        //let slc = storyboard.instantiateViewController(withIdentifier: "ScotorLocationViewController")as?ScotorLocationViewController
        //self.navigationController?.pushViewController(slc!, animated: true)
        
    }
    
    @IBAction func onTapResendOTPBtn(_ sender: UIButton) {
        
        self.showActivityIndicator()
        
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumberStr, uiDelegate: nil) { (verificationID, error) in
            
            if (error != nil) {
                
                self.hideActivityIndicator()
                
                ERROR_MESSAGE = error?.localizedDescription ?? ""
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                
                return
                
            } else {
                
                if verificationID != nil {
                    
                    self.VERIFICATIONID = verificationID ?? ""
                    print("Verification Id From Firebase : \(self.VERIFICATIONID)")
                    
                    self.firstTF.text = ""
                    self.secondTF.text = ""
                    self.thirdTF.text = ""
                    self.fourthTF.text = ""
                    self.FifthTf.text = ""
                    self.SixthTf.text = ""
                    
                    self.otpStr = ""
                    
                    self.hideActivityIndicator()
                }
            }
        }
    }
    
    @IBAction func onTapVerifyBtn(_ sender: UIButton) {
        
        
        
        if otpStr.isEmpty {
            
            if otpStr.count == 6 {
                
                
                
            } else{
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "enter_otp", value: "")))
            }
            
        } else {
            
            self.showActivityIndicator()
            
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: firVerificationId,
                verificationCode: otpStr)
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                
                if error != nil {
                    
                    self.hideActivityIndicator()
                    
                    ERROR_MESSAGE = error?.localizedDescription ?? ""
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                    
                    return
                }
                
                // User is signed in
                
                let currentUser = Auth.auth().currentUser
                currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                    if error != nil {
                        
                        // Handle error
                        
                        self.hideActivityIndicator()
                        
                        ERROR_MESSAGE = error?.localizedDescription ?? ""
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                        
                        return
                    }
                    
                    // Send token to your backend via HTTPS
                    // ...
                    
                    guard let tokenFromFirebase = idToken else { return }
                    print("Token from Firebase : \(tokenFromFirebase)")
                    self.firebaseToken = tokenFromFirebase
                    
                    if self.isForForgotPassword {
                        
                        self.forgotDirectlyAPI()
                        
                    } else {
                        
                       self.verifyPhone()
                    }
                   
                   // self.getPhoneAPI()
                    
                    
                }
            }
        }
    }
}

extension OTPViewController {
    
    //MARK: - VERIFY PHONE API
    
    func verifyPhone() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(VERIFY_PHONE_DIRECTLY)"
            
            let params = [ "user_id": userId,
                          // "firebase_token": firebaseToken,
                           "token_key": tokenKey
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in

                switch result {
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        print("Response:: \(response)")

                        #if DEBUG
                        
                        print(" Verify Phone Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err.localizedDescription)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json as? [String : Any] {
                                    
                                    self.responseData = (data["user_detail"] as! NSDictionary) as! [String : Any]
                                    print(self.responseData)
                                    
                                    UserDefaults.standard.set(self.responseData, forKey: "LOGIN_RESPONSE")
                                    
                                    DispatchQueue.main.async {
                                        
//
//                                        if self.isForForgotPassword {
//
//                                            DispatchQueue.main.async {
//
//                                                let npVc = NewPasswordViewController.getInstance()
//                                                npVc.firToken = self.firebaseToken
//                                                self.navigationController?.pushViewController(npVc, animated: true)
//                                            }
//
//                                        } else {
                                            
                                            UserDefaults.standard.set(true, forKey: "IS_LOGGED_IN")
                                            UserDefaults.standard.synchronize()
                                            
                                            let hvc = MainViewController.getInstance()
                                            hvc.setup(type: 1)
                                            self.navigationController?.pushViewController(hvc, animated: true)
                                        }
                                    }
                             //   }
                                
                            } else {
                                
                                self.hideActivityIndicator()
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: json["message"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Verify Phone Error:\(err.localizedDescription)")
                }
            }
            
        } else {
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            
            // UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message:"Internet not avaliable")
        }
    }
    
}

//MARK: - RESEND API

extension OTPViewController {
    
    func forgotDirectlyAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(verify_password_directly)"
            
            let params = [
                
                "user_phone": mobileNumberStr,
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Forgot Api Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                
                               
                                

                                if let data = json as? [String : Any] {
                                    
                                    self.responseData = (data["user_detail"] as! NSDictionary) as! [String : Any]
                                    print(self.responseData)
                                    
                                    UserDefaults.standard.set(self.responseData, forKey: "LOGIN_RESPONSE")
                                    DispatchQueue.main.async {
                                        
                                        let npVc = NewPasswordViewController.getInstance()
                                        npVc.firToken = self.firebaseToken
                                        self.navigationController?.pushViewController(npVc, animated: true)
                                    }
                                    
                                    //                                    self.responseData = (data["data"] as! NSDictionary) as! [String : Any]
                                    //
                                    //                                    UserDefaults.standard.set(self.responseData, forKey: "LOGIN_RESPONSE")
                                    
                                }
                                
                            }else{
                                self.hideActivityIndicator()
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: json["message"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Forgot Api Error:\(err.localizedDescription)")
                }
            }
        }else{
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message:"Internet not avaliable")
        }
    }
    
    //MARK: - LOCK DETAILS API
    func getPhoneAPI(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(VERIFY_PHONE_DIRECTLY)"
                       
                       let params = [ "user_id": userId,
                                    "token_key": tokenKey,
                                   // "firebase_token": firebaseToken,
                                    //  "token_2020":firebaseToken
                       ]
            #if DEBUG
            
            print("BEGIN 24food======>URLSTRING" + urlString)
            print("BEGIN 24food=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.request(urlString, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
                .validate(statusCode: 200..<600)
                .responseJSON { response in
                    print("Result:: \(response)")

                    switch response.result
                    {
                    case .failure(let error):
                        if let data = response.data {
                            print("Print failure Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                            self.hideActivityIndicator()
                        }
                      //  print("ERROR: ====> \(error)")
                        
                    case .success(let value):
                        if let data = response.data {
                            print("Print  Sucess Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                            self.hideActivityIndicator()
                            print("SUCCESSDATA: =====> \(data)")
                           // {
                                
                              //  self.responseData = (data["user_detail"] as! NSDictionary) as! [String : Any]
                                print(self.responseData)
                                
                                UserDefaults.standard.set(self.responseData, forKey: "LOGIN_RESPONSE")
                                
                                DispatchQueue.main.async {
                                    
                                    if self.isForForgotPassword {
                                        
                                        DispatchQueue.main.async {
                                            
                                            let npVc = NewPasswordViewController.getInstance()
                                            npVc.firToken = self.firebaseToken
                                            self.navigationController?.pushViewController(npVc, animated: true)
                                        }
                                        
                                    } else {
                                        
                                        UserDefaults.standard.set(true, forKey: "IS_LOGGED_IN")
                                        UserDefaults.standard.synchronize()
                                        
                                        let hvc = MainViewController.getInstance()
                                        hvc.setup(type: 1)
                                        self.navigationController?.pushViewController(hvc, animated: true)
                                    }
                                }
                            }
                            
                        }
                        self.hideActivityIndicator()
                      //  print("LOCK DETAILS RESPONSE: ====> \(value)")
                        
               
            }
            
        } else {
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
}

