//
//  RideDetailsViewController.swift
//  Sazile
//
//  Created by harjitsingh on 21/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class RideDetailsViewController: UIViewController {
    
    @IBOutlet weak var navRideTitleLbl: UILabel!
    @IBOutlet weak var thakYouPaymentLbl: UILabel!
    @IBOutlet weak var startRTLbbl: UILabel!
    @IBOutlet weak var endRTLbl: UILabel!
    @IBOutlet weak var grandTotalMinsLbl: UILabel!
    @IBOutlet weak var grandTotalLbl: UILabel!
    @IBOutlet weak var subMinsLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var walletMinsLbl: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var rideCostMinsLbl: UILabel!
    @IBOutlet weak var rideCostLbl: UILabel!
    @IBOutlet weak var billingDetailsLbl: UILabel!
    @IBOutlet weak var minsLbl: UILabel!
    @IBOutlet weak var kmLBL: UILabel!
    @IBOutlet weak var totalMinsLbl: UILabel!
    @IBOutlet weak var totalKMLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    
    var scooterBarBtn = UIBarButtonItem()
    var rideHistrory : [String : Any] = [:]
    var tokenKey = String()
    var userId = String()
    var rideId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.navigationItem.title = (L102Language.AMLocalizedString(key: "riding_details", value: ""))
        self.startRTLbbl.text = (L102Language.AMLocalizedString(key: "start_ride_time", value: ""))
        self.endRTLbl.text = (L102Language.AMLocalizedString(key: "end_ride_time", value: ""))
        self.billingDetailsLbl.text = (L102Language.AMLocalizedString(key: "billing_details", value: ""))
        self.rideCostLbl.text = (L102Language.AMLocalizedString(key: "ride_cost", value: ""))
        self.walletLbl.text = (L102Language.AMLocalizedString(key: "wallet", value: ""))
        self.subTotalLbl.text = (L102Language.AMLocalizedString(key: "sub_total", value: ""))
        self.grandTotalLbl.text = (L102Language.AMLocalizedString(key: "grand_total", value: ""))
        self.thakYouPaymentLbl.text = (L102Language.AMLocalizedString(key: "thankyou", value: ""))
        self.navRideTitleLbl.text = (L102Language.AMLocalizedString(key: "riding_details", value: ""))

        scooterBarBtn = UIBarButtonItem(image: UIImage(named: "scootr_white"), style: .plain, target: self, action: nil)
        
        navigationItem.leftBarButtonItem = scooterBarBtn
        
      //  self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onClickCancelBtn)), animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if isForEndRideBool {
            
            if let data1 = UserDefaults.standard.value(forKey: "RIDE_DETAILS") {
            
            let rideData = data1 as! NSDictionary
                    
                     self.rideCostMinsLbl.text = rideData["ride_cost"] as? String ?? ""
                     self.walletMinsLbl.text = rideData["from_wallet"] as? String  ?? ""
                     self.subMinsLbl.text = rideData["sub_total"] as? String ?? ""
                     self.grandTotalMinsLbl.text = rideData["grand_total"] as? String ?? ""
                     self.minsLbl.text = rideData["ride_cost"] as? String ?? ""
                     self.kmLBL.text = rideData["ride_distance"] as? String ?? ""
                     
                     let dateStr1 = rideData["ride_start_time"]as? String ?? ""
                     let strArr = dateStr1.components(separatedBy: " ")
                     let date1 = strArr[0]
                     
                     let date2 = strArr[1]
                     let date3 = strArr[2]
                     
                     self.startTimeLbl.text = String(format: "%@ %@ %@",date1, date2,date3)
                     //self.startTimeLbl.text = date3
                     
                     let dateStr = rideData["ride_end_time"]as? String ?? ""
                     let strArr1 = dateStr.components(separatedBy: " ")
                     let date7 = strArr[0]

                     let date4 = strArr1[1]
                     let date5 = strArr1[2]
                     self.endTimeLbl.text = String(format: "%@ %@ %@",date7, date4,date5)
                     //self.endTimeLbl.text = date5
                
            }
            
            
            
        } else {
            
            if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
                
                let rideDetails = data as! NSDictionary
                
                tokenKey = rideDetails["token_key"] as? String ?? ""
                print(tokenKey)
                userId = rideDetails["user_id"] as? String ?? ""
                print(userId)
                
              //  getRideDetails()
            }
            
            if let data2 = UserDefaults.standard.value(forKey: "RIDE_DETAILS") {
            
            let rideData = data2 as! NSDictionary
                    
                     self.rideCostMinsLbl.text = rideData["ride_cost"] as? String ?? ""
                     self.walletMinsLbl.text = rideData["from_wallet"] as? String  ?? ""
                     self.subMinsLbl.text = rideData["sub_total"] as? String ?? ""
                     self.grandTotalMinsLbl.text = rideData["grand_total"] as? String ?? ""
                     self.minsLbl.text = rideData["ride_cost"] as? String ?? ""
                     self.kmLBL.text = rideData["ride_distance"] as? String ?? ""
                     
                     let dateStr1 = rideData["ride_start_time"]as? String ?? ""
                     let strArr = dateStr1.components(separatedBy: " ")
                     let date1 = strArr[0]
                     
                     let date2 = strArr[1]
                     let date3 = strArr[2]
                     
                     self.startTimeLbl.text = String(format: "%@ %@ %@",date1, date2,date3)
                     //self.startTimeLbl.text = date3
                     
                     let dateStr = rideData["ride_end_time"]as? String ?? ""
                     let strArr1 = dateStr.components(separatedBy: " ")
                     let date7 = strArr[0]

                     let date4 = strArr1[1]
                     let date5 = strArr1[2]
                     self.endTimeLbl.text = String(format: "%@ %@ %@",date7, date4,date5)
                     //self.endTimeLbl.text = date5
                
            }
        }
        
        
    }
    
    static func getInstance() -> RideDetailsViewController {
        
        let storyboard = UIStoryboard(name: "RideDetails", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RideDetailsViewController") as! RideDetailsViewController
    }
    
//MARK: - ONCLICKCANCELBTN
//    @objc func onClickCancelBtn() {
//
//        DispatchQueue.main.async {
//
//        if isFromDismissRideDetailsVwBool  {
//
//        self.dismiss(animated: true, completion: nil)
//
//        } else {
//
//            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
//            }
//        }
//    }
    
    @IBAction func onTapDismissBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
        
        if isFromDismissRideDetailsVwBool  {
        
        self.dismiss(animated: true, completion: nil)
            
            
        }
        else {
            
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        }
    }
}

//MARK: - RIDE DETAILS API
extension RideDetailsViewController {
    
    func getRideDetails() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(RIDEHISTRORY_DETAILS)"
            
            let params = [
                "user_id": userId,
                "token_key": tokenKey,
                "ride_id" : rideId
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" RideDetails Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                isFromDismissRideDetailsVwBool = true

                                if let data = json ["data"] as? [String : Any] {
                                    
                                    print(self.rideHistrory)

                                    self.rideHistrory = data
                                    
                                   // self.displayRideDetails()
                                }
                                
                            } else {
                               // isFromDismissRideDetailsVwBool = true

                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("RideDetails Error:\(err.localizedDescription)")
                }
            }
        }else{
            
             UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
//            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    

}


