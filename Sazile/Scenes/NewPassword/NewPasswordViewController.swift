//
//  NewPasswordViewController.swift
//  Sazile
//
//  Created by harjitsingh on 09/11/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController {
    
    var responseData : [String : Any] = [:]
    var massageID = Int()
    var massageID1 = Int()

    var userId = String()
    var tokenKey = String()
    var firToken = String()
    //MARK:-  OUTLETS

    @IBOutlet weak var confirmPasswordLbl: UILabel!
    @IBOutlet weak var newPasswordLbl: UILabel!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var updateBtn: UIButton!
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutDesign()
        updateBtn.layer.shadowColor = UIColor.lightGray.cgColor
        updateBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        updateBtn.layer.shadowOpacity = 3.0
        updateBtn.layer.shadowRadius = 0.0
        updateBtn.layer.masksToBounds = false
        updateBtn.layer.cornerRadius = updateBtn.frame.size.height / 2
        self.newPasswordLbl.text = (L102Language.AMLocalizedString(key: "new_password", value: ""))
        self.confirmPasswordLbl.text = (L102Language.AMLocalizedString(key: "confirm_password", value: ""))
        self.updateBtn.setTitle(L102Language.AMLocalizedString(key: "update", value: ""), for: .normal)
        
    }
    
    func layoutDesign() {
        
        let imgVw = UIImage(named: "sazileLogo")
        let navLogo = UIImageView(image:imgVw)
        self.navigationItem.titleView = navLogo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            let newPosswordData = data as! NSDictionary
            tokenKey = newPosswordData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = newPosswordData["user_id"] as? String ?? ""
            print(userId)
            
        }
    }
    
    static func getInstance() -> NewPasswordViewController {
        
        let storyboard = UIStoryboard(name: "NewPassword", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "NewPasswordViewController") as! NewPasswordViewController
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onTapUpdateBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.newPasswordTF.text = self.newPasswordTF.text?.trimmingCharacters(in: .whitespaces)
            
            self.confirmPasswordTF.text = self.confirmPasswordTF.text?.trimmingCharacters(in: .whitespaces)
            
            if self.newPasswordTF.hasText{
                
                if self.confirmPasswordTF.hasText{
                    
                    self.updatePasswordAPI()
                    
                } else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "password_enter_confirm", value: "")))
                }
                
            } else {
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "password_enter_new", value: "")))
                
            }
        }
    }
}
//
//user_id:1
//token_key:x1OGqjw7hebAtp9EKYSCEYDsyWgWL9YDx8WFTCESJT4ir5asYa
//password:12345678
//confirm_password:12345678

//MARK: - UPDATE PASSWORD API

extension NewPasswordViewController{
    
    func updatePasswordAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(UPDATE_PASSWORD)"
            
            let params = [  "firebase_token": firToken,
                            "user_id": userId,
                            "token_key" : tokenKey,
                            "password":  newPasswordTF.text ?? "",
                            "confirm_password": confirmPasswordTF.text ?? ""            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" NewPassoword Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID1 = json["message_id"] as? Int ?? 0
                                if self.massageID1 == 37 {
                                    if let data = json as? [String : Any] {
                                        
                                        self.responseData = (data["user_detail"] as! NSDictionary) as! [String : Any]
                                        print(self.responseData)
                                        
                                        UserDefaults.standard.set(self.responseData, forKey: "LOGIN_RESPONSE")
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "pwd_update_successfully", value: "")), buttonTitle: "Ok") {
                                            
                                            DispatchQueue.main.async {
                                                
                                                UserDefaults.standard.set(true, forKey: "IS_LOGGED_IN")
                                                UserDefaults.standard.synchronize()
                                                
                                                let hvc = MainViewController.getInstance()
                                                hvc.setup(type: 1)
                                                self.navigationController?.pushViewController(hvc, animated: true)
                                            }
                                        }
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    
                                }

                                
                            } else {
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                
                                if self.massageID == 36 {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "both_pwd_did_not_match", value: "")))

                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")

                                    
                                }
                                
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("New Password Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
}




