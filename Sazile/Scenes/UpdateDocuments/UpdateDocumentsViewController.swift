//
//  UpdateDocumentsViewController.swift
//  Sazile
//
//  Created by harjitsingh on 14/11/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class UpdateDocumentsViewController: UIViewController {
    
    var dataDict : [String:Any] = [:]
    var tokenKey = String()
    var userId = String()
    var govtIDBool = Bool()
    var licenceBool = Bool()
    var govtIDData = NSData()
    var licenceData = NSData()
    var govtIDStr = String ()
    var licenceIdStr = String()
    var documentsBool = Bool()
    var massageID = Int()
    var massageID1 = Int()
    
    
    @IBOutlet weak var govtImgVw: UIImageView!
    @IBOutlet weak var govtIDUploadBtn: UIButton!
    
    @IBOutlet weak var licenceIDImgVw: UIImageView!
    @IBOutlet weak var govtIDVw: UIView!
    @IBOutlet weak var govtIDLbl: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var licenceUploadBtn: UIButton!
    @IBOutlet weak var licenceVw: UIView!
    @IBOutlet weak var licenceorbsrLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.documentsBool = false
        
        // self.navigationItem.title = (L102Language.AMLocalizedString(key: "UPDATE_DOCUMENTS", value: ""))
        
        govtIDUploadBtn.layer.shadowColor = UIColor.lightGray.cgColor
        govtIDUploadBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        govtIDUploadBtn.layer.shadowOpacity = 3.0
        govtIDUploadBtn.layer.shadowRadius = 0.0
        govtIDUploadBtn.layer.masksToBounds = false
        govtIDUploadBtn.layer.cornerRadius = govtIDUploadBtn.frame.size.height / 2
        
        licenceUploadBtn.layer.shadowColor = UIColor.lightGray.cgColor
        licenceUploadBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        licenceUploadBtn.layer.shadowOpacity = 3.0
        licenceUploadBtn.layer.shadowRadius = 0.0
        licenceUploadBtn.layer.masksToBounds = false
        licenceUploadBtn.layer.cornerRadius = licenceUploadBtn.frame.size.height / 2
        
        licenceVw.layer.borderWidth = 1
        licenceVw.layer.borderColor = UIColor.gray.cgColor
        licenceVw.layer.cornerRadius = 2
        licenceVw.clipsToBounds = true
        
        govtIDVw.layer.borderWidth = 1
        govtIDVw.layer.borderColor = UIColor.gray.cgColor
        govtIDVw.layer.cornerRadius = 2
        govtIDVw.clipsToBounds = true
        
        updateBtn.layer.shadowColor = UIColor.lightGray.cgColor
        updateBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        updateBtn.layer.shadowOpacity = 3.0
        updateBtn.layer.shadowRadius = 0.0
        updateBtn.layer.masksToBounds = false
        updateBtn.layer.cornerRadius = updateBtn.frame.size.height / 2
        
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "updates_documents", value: ""))
        self.govtIDLbl.text = (L102Language.AMLocalizedString(key: "government_id", value: ""))
        self.licenceorbsrLbl.text = (L102Language.AMLocalizedString(key: "licence_or_bsr", value: ""))
        self.govtIDUploadBtn.setTitle(L102Language.AMLocalizedString(key: "upload", value: ""), for: .normal)
        self.licenceUploadBtn.setTitle(L102Language.AMLocalizedString(key: "upload", value: ""), for: .normal)
        self.updateBtn.setTitle(L102Language.AMLocalizedString(key: "update", value: ""), for: .normal)
        
        if let data = UserDefaults.standard.value(forKey: "PROFILE_RESPONSE"){
            let updateDocumentsData = data as! NSDictionary
            print("UPDATE DOCUMENTS: \(updateDocumentsData)" )
            
            guard let imageGovtId = updateDocumentsData["government_id"] as? String else { return  }
            govtImgVw.sd_setImage(with: URL(string: imageGovtId), placeholderImage: UIImage(named: "govtid"))
            
            guard let image2 = updateDocumentsData["licence_bsr"] as? String else { return }
            licenceIDImgVw.sd_setImage(with: URL(string: image2), placeholderImage: UIImage(named: "licence"))
            
        }
    }
    
    static func getInstance() -> UpdateDocumentsViewController{
        let storyboard = UIStoryboard(name: "UpdateDocuments", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "UpdateDocumentsViewController") as! UpdateDocumentsViewController
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if documentsBool {
            
            documentsBool = false
            
        } else{
            
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            
            if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
                let loginData = data as! NSDictionary
                
                print(loginData)
                tokenKey = loginData["token_key"] as? String ?? ""
                print(tokenKey)
                userId = loginData["user_id"] as? String ?? ""
                print(userId)
            }
        }
        
    }
    
    //MARK: - TWO VIEWCONTROLLERS POP
    func backTwo() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    func sellectphoto() {
        
        let alert = UIAlertController(title: (L102Language.AMLocalizedString(key: "choose_image", value: "")), message: "", preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: UIAlertAction.Style.default, handler: { _ in
            
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "photo_library", value: "")),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.photoLibrary()
                                        
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "cancel1", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onTapGovtIDUploadBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.documentsBool = true
            self.licenceBool = false
            self.govtIDBool = true
            self.sellectphoto()
        }
    }
    @IBAction func onTapUploadLicenceBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.documentsBool = true
            self.govtIDBool = false
            self.licenceBool = true
            self.sellectphoto()
        }
        
    }
    
    @IBAction func onTapUpdateBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.getUpdateDocumentsAPI()
        }
    }
}

//MARK: - UPDATE DOCUMENTS API
extension UpdateDocumentsViewController{
    
    func getUpdateDocumentsAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            
            let urlString = "\(BASEURL)\(UPDATEDOCUMENTS)"
            
            let params = ["user_id": userId,
                          "token_key" : tokenKey
                
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if self.govtIDData.count > 0 {
                    
                    let data : Data = self.govtIDData as Data
                    
                    MultipartFormData.append(data, withName: "government_id", fileName: "image.png", mimeType: "image/png")
                }
                
                if self.licenceData.count > 0 {
                    
                    let data : Data = self.licenceData as Data
                    
                    MultipartFormData.append(data, withName: "licence_bsr", fileName: "image.png", mimeType: "image/png")
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Update documentsResponse : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID1 = json["message_id"] as? Int ?? 0
                                
                                if self.massageID1 == 114 {
                                    
                                    DispatchQueue.main.async {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "documents_updated", value: "")), buttonTitle: "Ok") {
                                            self.backTwo()
                                            
                                        }
                                    }
                                    
                                } else {
                                    
                                    
                                }
                                
                                
                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        self.massageID = json["message_id"] as? Int ?? 0
                                        if self.massageID == 115 {
                                            
                                            DispatchQueue.main.async {
                                                
                                                UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "no_changes", value: "")), buttonTitle: "Ok") {
                                                    self.backTwo()
                                                    
                                                }
                                            }
                                        } else {
                                            
                                            
                                        }
                                        
                                        
                                        //                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("UpdateDocuments Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
}

//MARK: - UIIMAGE PICKER DELEGATE
extension UpdateDocumentsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .photoLibrary
            
            self.present(imagePickerController, animated: true, completion: nil)
            
        }
    }
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
            
            let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            
            let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
            
            if govtIDBool {
                govtIDData = data!
                govtImgVw.image = chosenImage
                
            } else {
                licenceData = data!
                licenceIDImgVw.image = chosenImage
                
            }
            
            
            picker.dismiss(animated: true)
            
        } else {
            
            print("BEGIN:Sazile ======> Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
