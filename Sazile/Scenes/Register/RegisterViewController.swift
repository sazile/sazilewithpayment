//
//  RegisterViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 24/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit
import Photos
import ADCountryPicker

class RegisterViewController: UIViewController,UITextFieldDelegate {
    
    var dailingCode = String()
    var dataDict : [String: Any] = [:]
    var isFromPrivacyPolicy = Bool()
    var isFromTermsofservice = Bool()
    var userAgreement = Bool()
    var licenceData = NSData()
    var govtIdData = NSData()
    var govtIDBool = Bool()
    var licenceBool = Bool()
    var emailValidationstr = String()
    var ischeckbox = Bool()
    var genderStr = String()
    var FCMTOKEN = String()
    var isGenderSelected = Bool()
    var massageID = Int()
    var massageID1 = Int()
    
    //MARK: -  OUTLETS
    @IBOutlet weak var dropDownImgVw: UIImageView!
    @IBOutlet weak var optionalLbl: UILabel!
    @IBOutlet weak var referralCodeTF: UITextField!
    @IBOutlet weak var termsOfServiceBtn: UIButton!
    @IBOutlet weak var userAgreementBtn: UIButton!
    @IBOutlet weak var privacyPolacyBtn: UIButton!
    @IBOutlet weak var iAgreeLbl: UILabel!
    @IBOutlet weak var mandratryLbl: UILabel!
    @IBOutlet weak var licenceBarLbl: UILabel!
    @IBOutlet weak var govtIDLbl: UILabel!
    @IBOutlet weak var femaleLbl: UILabel!
    @IBOutlet weak var maleLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var signInContinueLbl: UILabel!
    @IBOutlet weak var regLbl: UILabel!
    @IBOutlet weak var femaleImgVw: UIImageView!
    @IBOutlet weak var maleImgVw: UIImageView!
    @IBOutlet weak var countryCodeTF: UITextField!
    @IBOutlet weak var countryImgVw: UIImageView!
    
    @IBOutlet weak var licenceImgVw: UIImageView!
    @IBOutlet weak var govtIDImgVw: UIImageView!
    @IBOutlet weak var pravicyImgVw: UIImageView!
    
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var governmentBgVw: UIView!
    @IBOutlet weak var uploadGovtIdBtn: UIButton!
    
    @IBOutlet weak var licenseBgVw: UIView!
    @IBOutlet weak var uploadLicenseBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet var maleBtn: UIButton!
    @IBOutlet var femaleBtn: UIButton!
    
    @IBOutlet weak var registerLbl: UILabel!
    @IBOutlet weak var signupLbl: UILabel!
    @IBOutlet weak var numberTextFd: UITextField!
    @IBOutlet weak var mobileNumTxtFd: UITextField!
    @IBOutlet weak var imgSecureEye: UIImageView!

    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isCountrySelectedForRegister = false
        ischeckbox = false
        numberTF.delegate = self
        layoutDesign()
        genderStr = "male"
        self.maleImgVw.image = UIImage(named: "circle-fill-icon")
        isGenderSelected = true
        
        self.regLbl.text = (L102Language.AMLocalizedString(key: "create_an_account", value: ""))
        self.signInContinueLbl.text = (L102Language.AMLocalizedString(key: "sign_up_to_continue", value: ""))
        self.numberTF.placeholder = (L102Language.AMLocalizedString(key: "mobile_number", value: ""))
        self.emailTF.placeholder = (L102Language.AMLocalizedString(key: "email_address", value: ""))
        self.passwordTF.placeholder = (L102Language.AMLocalizedString(key: "password", value: ""))
        self.fullNameTF.placeholder = (L102Language.AMLocalizedString(key: "full_name", value: ""))
        self.referralCodeTF.placeholder = (L102Language.AMLocalizedString(key: "referral_code_sazile", value: ""))
        self.optionalLbl.text = (L102Language.AMLocalizedString(key: "optional", value: ""))
        self.genderLbl.text = (L102Language.AMLocalizedString(key: "gender", value: ""))
        self.maleLbl.text = (L102Language.AMLocalizedString(key: "male", value: ""))
        self.femaleLbl.text = (L102Language.AMLocalizedString(key: "female", value: ""))
        self.govtIDLbl.text = (L102Language.AMLocalizedString(key: "government_id", value: ""))
        self.uploadGovtIdBtn.setTitle((L102Language.AMLocalizedString(key: "upload", value: "")), for: .normal)
        self.uploadLicenseBtn.setTitle((L102Language.AMLocalizedString(key: "upload", value: "")), for: .normal)
        
        self.licenceBarLbl.text = (L102Language.AMLocalizedString(key: "licence_or_bsr", value: ""))
        //self.mandratryLbl.text = (L102Language).AMLocalizedString(key: "note_mandatory_if_you_was_born_after_1_january_1988", value: "")
        self.iAgreeLbl.text = (L102Language).AMLocalizedString(key: "continuing_means_you_ve_read_and_agreed_to", value: "")
        self.termsOfServiceBtn.setTitle(L102Language.AMLocalizedString(key: "terms_of_service", value: ""), for: .normal)
        self.privacyPolacyBtn.setTitle(L102Language.AMLocalizedString(key: "privacy2", value: ""), for: .normal)
        self.userAgreementBtn.setTitle(L102Language.AMLocalizedString(key: "user3", value: ""), for: .normal)
        self.registerBtn.setTitle(L102Language.AMLocalizedString(key: "create_an_account", value: ""), for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (UserDefaults.standard.value(forKey: "CurrentLanguage") as? String) != nil {
            
            let currentLang : String = UserDefaults.standard.value(forKey: "CurrentLanguage") as? String ?? ""
            print("Current Language : \(currentLang)")
            
            if currentLang == "en" {
                termsOfServiceBtn.titleLabel?.font = termsOfServiceBtn.titleLabel?.font.withSize(13)
                
                privacyPolacyBtn.titleLabel?.font = privacyPolacyBtn.titleLabel?.font.withSize(13)
                
            } else {
                termsOfServiceBtn.titleLabel?.font = termsOfServiceBtn.titleLabel?.font.withSize(9)
                
                privacyPolacyBtn.titleLabel?.font = privacyPolacyBtn.titleLabel?.font.withSize(9)
                
            }
        }
        
        if isCountrySelectedForRegister {
            
            isCountrySelectedForRegister = false
            
        } else {
            
            countryCodeTF.text = "YT +262"
            countryImgVw.image = UIImage(named: "YT")
            dailingCode = "+262"
        }
        
        let fcmToken : String = UserDefaults.standard.value(forKey: "fcm_token") as? String ?? ""
        
        if fcmToken.isEmpty {
            
            FCMTOKEN = ""
            
        } else {
            
            FCMTOKEN = fcmToken
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    static func getInstance() -> RegisterViewController {
        
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
    }
    
    //MARK: - chooseOrSelectImageViaAlertController
    func chooseOrSelectImageViaAlertController() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        
        // let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: UIAlertAction.Style.default, handler: { _ in
            self.openCamera()
            
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "photo_library", value: "")),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "cancel1", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
        }))
        
        //        if IS_IPHONE {
        //
        //            self.present(alert, animated: true, completion: nil)
        //
        //        } else {
        //
        //            let popOver = alert.popoverPresentationController
        //            if popOver != nil {
        //                popOver?.sourceView = alert.view
        //                popOver?.sourceRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        //
        //                self.present(alert, animated: true)
        //            }
        //        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - LAYOUT DESIGN
    func layoutDesign() {
        
        let imgVw = UIImage(named: "sazileLogo")
        let navLogo = UIImageView(image:imgVw)
        self.navigationItem.titleView = navLogo
        
        governmentBgVw.layer.borderWidth = 1.0
        governmentBgVw.layer.borderColor = UIColor.lightGray.cgColor
        
        licenseBgVw.layer.borderWidth = 1.0
        licenseBgVw.layer.borderColor = UIColor.lightGray.cgColor
        
        uploadGovtIdBtn.layer.shadowColor = UIColor.lightGray.cgColor
        uploadGovtIdBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        uploadGovtIdBtn.layer.shadowOpacity = 3.0
        uploadGovtIdBtn.layer.shadowRadius = 0.0
        uploadGovtIdBtn.layer.masksToBounds = false
        uploadGovtIdBtn.layer.cornerRadius = uploadGovtIdBtn.frame.size.height / 2
        
        uploadLicenseBtn.layer.shadowColor = UIColor.lightGray.cgColor
        uploadLicenseBtn.layer.shadowOffset = CGSize(width:0, height: 1.5 )
        uploadLicenseBtn.layer.shadowOpacity = 3.0
        uploadLicenseBtn.layer.shadowRadius = 0.0
        uploadLicenseBtn.layer.masksToBounds = false
        uploadLicenseBtn.layer.cornerRadius = uploadLicenseBtn.frame.size.height / 2
        
        registerBtn.layer.shadowColor = UIColor.lightGray.cgColor
        registerBtn.layer.shadowOffset = CGSize(width:0, height: 2.5 )
        registerBtn.layer.shadowOpacity = 3.0
        registerBtn.layer.shadowRadius = 0.0
        registerBtn.layer.masksToBounds = false
        registerBtn.layer.cornerRadius = registerBtn.frame.size.height / 2
        
    }
    
    //MARK: - IB ACTIONS
    @IBAction func onTapCountryBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            let picker = ADCountryPicker(style: .grouped)
            picker.delegate = self
            picker.showCallingCodes = true
            picker.searchBarBackgroundColor = UIColor.white
            picker.pickerTitle = (L102Language.AMLocalizedString(key: "select_country", value: ""))
            picker.hidesNavigationBarWhenPresentingSearch = false
            self.navigationController?.pushViewController(picker, animated: true)
        }
    }
    @IBAction func onTapUploadGovtIdBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isCountrySelectedForRegister = true
            self.licenceBool = false
            self.govtIDBool = true
            self.chooseOrSelectImageViaAlertController()
        }
    }
    @IBAction func onTapPrivacyBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            if self.ischeckbox {
                
                self.ischeckbox = false
                self.pravicyImgVw.image = UIImage(named: "box-shape")
                
            } else {
                
                self.ischeckbox = true
                self.pravicyImgVw.image = UIImage(named: "checkbox-tick")
            }
        }
    }
    @IBAction func onTapTermsCndBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isCountrySelectedForRegister = true
            let ppvc = PrivicyPolicyViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            ppvc.navigationController?.modalPresentationStyle = .fullScreen
            ppvc.isFromTermsofservice = true
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapPrivacyPolicyBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isCountrySelectedForRegister = true
            let ppvc = PrivicyPolicyViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            ppvc.navigationController?.modalPresentationStyle = .fullScreen
            ppvc.isFromPrivacyPolicy = true
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapUserAgreeBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            isCountrySelectedForRegister = true
            let ppvc = PrivicyPolicyViewController.getInstance()
            let navigationController = UINavigationController(rootViewController: ppvc)
            ppvc.navigationController?.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapUploadLicenseBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            isCountrySelectedForRegister = true
            self.govtIDBool = false
            self.licenceBool = true
            self.chooseOrSelectImageViaAlertController()
        }
    }
    @IBAction func onTapMaleBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.maleImgVw.image = UIImage(named: "circle-fill-icon")
            self.femaleImgVw.image = UIImage(named: "circle-unfill")
            self.genderStr = "male"
            self.isGenderSelected = true
            
        }
    }
    @IBAction func onTapFemaleBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.femaleImgVw.image = UIImage(named: "circle-fill-icon")
            self.maleImgVw.image = UIImage(named: "circle-unfill")
            self.genderStr = "female"
            self.isGenderSelected = false
        }
    }
    @IBAction func onTapRegisterBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.hideKeyBoard()
            self.numberTF.text = self.numberTF.text?.trimmingCharacters(in: .whitespaces)
            self.emailTF.text = self.emailTF.text?.trimmingCharacters(in: .whitespaces)
            self.fullNameTF.text = self.fullNameTF.text?.trimmingCharacters(in: .whitespaces)
            self.passwordTF.text = self.passwordTF.text?.trimmingCharacters(in: .whitespaces)
            
            if self.countryCodeTF.hasText {
                
                if self.numberTF.hasText {
                    
                    if self.emailTF.hasText {
                        
                        if AppHelper.isValidEmail(with: self.emailTF.text) {
                            
                            if self.passwordTF.hasText {
                                
                                if self.fullNameTF.hasText {
                                    
                                    if self.govtIdData.length > 0 {
                                        
                                        //if self.licenceData.length > 0 {
                                            
                                            if self.ischeckbox {
                                                
                                                self.userregisterAPI()
                                                
                                            } else {
                                                
                                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "you_must_conditions", value: "")))
                                            }
                                            
//                                        } else {
//
//                                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "licence", value: "")))
//
//                                        }
                                        
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "governmetn_id", value: "")))
                                    }
                                    
                                } else {
                                    
                                    
                                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "full_name_enter", value: "")))
                                    
                                }
                                
                            } else {
                                
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "password_enter", value: "")))
                            }
                            
                            
                        } else {
                            
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key:"please_enter_valid_email_address", value: "")))
                        }
                        
                    } else {
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "email_address_enter", value: "")))
                    }
                    
                } else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: (L102Language.AMLocalizedString(key: "mobile_number_enter", value: "")))
                }
                
            }else{
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please Select Country")
            }
        }
    }
    
    @IBAction func onTapSecureUnsecure(_ sender: UIButton) {
        sender.tintColor = .clear
        if sender.isSelected == false{
            imgSecureEye.image = #imageLiteral(resourceName: "eyeUnsecure")
            sender.isSelected = true
            passwordTF.isSecureTextEntry = false
        }else{
            imgSecureEye.image = #imageLiteral(resourceName: "eyeSecure")
            sender.isSelected = false
            passwordTF.isSecureTextEntry = true
        }

    }
    
    //MARK: - UITEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        if numberTF.text?.count == 0 && string == "0" {
            
            return false
        }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 15
    }
    
}

//MARK: - IMAGEPICKER CONTROLLER DELEGATE , NAVIGATION DELEGATE
extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.modalPresentationStyle = .fullScreen
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
            
            let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            
            let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
            
            if govtIDBool {
                
                govtIDImgVw.image = chosenImage
                
            } else{
                licenceImgVw.image = chosenImage
                
            }
            
            if govtIDBool {
                
                govtIdData = data!
                // print("BEGIN:Sazile ======> govt Id Data : \(govtIdData)")
                
            } else {
                
                licenceData = data!
                // print("BEGIN:Sazile ======> licence Data : \(licenceData)")
            }
            
            picker.dismiss(animated: true)
            
        } else {
            
            print("BEGIN:Sazile ======> Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

//MARK: - SIGNUPAPI
extension RegisterViewController {
    
    func userregisterAPI(){
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(SIGNUP)"
            let params = ["dailing_code": dailingCode,
                          "mobile_number": numberTF.text ?? "",
                          "email": emailTF.text ?? "",
                          "password": passwordTF.text ?? "",
                          "full_name": fullNameTF.text ?? "",
                          "gender": genderStr,
                          "user_agreement": "true",
                          "fcm_id" : FCMTOKEN
                
                ] as [String : Any]
            
            
            #if DEBUG
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                if self.govtIdData.count > 0 {
                    
                    let data : Data = self.govtIdData as Data
                    
                    MultipartFormData.append(data, withName: "government_id", fileName: "image.png", mimeType: "image/png")
                }
                if self.licenceData.count > 0{
                    let data : Data = self.licenceData as Data
                    MultipartFormData.append(data, withName: "licence_bsr",fileName: "image.png", mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" SIGNUP Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.massageID1 = json["message_id"] as? Int ?? 0
                                if self.massageID1 == 17 {
                                    if let data = json as? [String: Any] {
                                        
                                        self.dataDict = data["data"] as? NSDictionary as! [String : Any]
                                        print(self.dataDict)
                                        UserDefaults.standard.set(self.dataDict, forKey: "LOGIN_RESPONSE")
                                        UserDefaults.standard.synchronize()
                                        
                                        DispatchQueue.main.async {
                                            
                                            UIAlertController.showAlertWithOkAction(self, title: (L102Language.AMLocalizedString(key: "signup_success", value: "")), message: "", buttonTitle: "Ok") {
                                                
                                                let lVc = LoginViewController.getInstance()
                                                lVc.isRegisteredSuccessfully = true
                                                self.navigationController?.pushViewController(lVc, animated: true)
                                            }
                                        }
                                        
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    
                                    
                                }
                                
                                
                            } else {
                                
                                self.massageID = json["message_id"] as? Int ?? 0
                                if self.massageID == 1  {
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "mobile_number_already_exist", value: "")) , buttonTitle: "OK") {
                                        
                                    }
                                    
                                } else if self.massageID == 2 {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "email_already_exist", value: "")) , buttonTitle: "OK") {
                                        
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                }
                                
                                self.hideActivityIndicator()
                                // UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Register Error:\(err.localizedDescription)")
                }
            }
            
        } else {
            
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
}

extension RegisterViewController: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        countryCodeTF.text = String(format: "%@ %@", code,dialCode)
        dailingCode = dialCode
        
        countryImgVw.image = picker.getFlag(countryCode: code)
        
        //        print("Dialing Code : \(dailingCode)")
        //        print("code : \(code)")
        //        print("Dial code : \(dialCode)")
        isCountrySelectedForRegister = true
        navigationController?.popViewController(animated: true)        
    }
    
}

