//
//  QuotientsSummaryViewController.swift
//  Sazile
//
//  Created by Pankaj Rana on 02/03/21.
//  Copyright © 2021 Harjit Singh. All rights reserved.
//

import UIKit

class QuotientsSummaryViewController: UIViewController {
    @IBOutlet weak var noTrasactionsLbl: UILabel!
    @IBOutlet weak var summaryTableVw: UITableView!
    var refreshControl = UIRefreshControl()
    var userId = String()
    var tokenKey = String()
    var summary = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "Quotients_Summary", value: ""))
        summaryTableVw.rowHeight = UITableView.automaticDimension
        summaryTableVw.separatorStyle = .none
        self.noTrasactionsLbl.isHidden = true
//        refreshControl.addTarget(self, action: #selector(refresh(sender: )), for: UIControl.Event.valueChanged)
//        summaryTableVw.addSubview(refreshControl) // not required when using UITableViewController
        // Do any additional setup after loading the view.
    }
    @objc func refresh(sender:AnyObject){
        self.summaryTableVw.reloadData()
        self.refreshControl.endRefreshing()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noTrasactionsLbl.isHidden = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE"){
            
            let transactionsData = data as! NSDictionary
            tokenKey = transactionsData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = transactionsData["user_id"] as? String ?? ""
            print(userId)
            
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            
        }
        get_summary()
    }

    static func getInstance() -> QuotientsSummaryViewController {
        let storyboard = UIStoryboard(name: "QuotientSummary", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "QuotientsSummaryViewController") as! QuotientsSummaryViewController
    }

    
    func get_summary() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(quotient_summary)"
            
            let params = [  "user_id": userId,
                            "token_key": tokenKey,
                            "page":"1",
                            "limit":"1"]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" MyWallet Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
            
                                self.summary = json.value(forKey: "data") as! [[String:Any]]
                                    DispatchQueue.main.async {
                                        self.summaryTableVw.delegate = self
                                        self.summaryTableVw.dataSource = self
                                        self.summaryTableVw.reloadData()
                                    }
                            }else{
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        self.noTrasactionsLbl.isHidden = false
                                       // self.noTrasactionsLbl.text = (L102Language.AMLocalizedString(key: "no_transactions", value: ""))
                                        
                                        //UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                }
                            }
                            self.summaryTableVw.reloadData()
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("MyWallet Error:\(err.localizedDescription)")
                    
                }
            }
            
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
        }
    }
}

extension QuotientsSummaryViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.summary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? quotientTableCell{
            cell.backView.layer.cornerRadius = 10
            cell.backView.clipsToBounds = true
//            cell.backView.layer.shadowOffset = CGSize(width:1, height:1 )
//            cell.backView.layer.shadowRadius = 1
//            cell.backView.layer.shadowOffset = CGSize.zero
//            cell.backView.layer.shadowOpacity = 1
//            cell.backView.clipsToBounds = false
            
            
            cell.titleLbl.text = "Amount Deducted"
            cell.descLbl.text = self.summary[indexPath.row]["quotient_reason"] as? String ?? ""
            let t = self.summary[indexPath.row]["quotient_careated_at"] as? String ?? ""
            let tr = t.components(separatedBy: " ")
            cell.timelbl.text = tr[0]
            
            cell.amontLbl.text = "€\(self.summary[indexPath.row]["quotient_amount"] as? String ?? "")"
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

class quotientTableCell:UITableViewCell{
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var amontLbl: UILabel!
    
}
