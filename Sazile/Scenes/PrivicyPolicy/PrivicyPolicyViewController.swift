//
//  PrivicyPolicyViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 07/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class PrivicyPolicyViewController: UIViewController {
    
    //MARK: -  OUTLETS
    @IBOutlet weak var webVw: UIWebView!
    
    var isFromPrivacyPolicy = Bool()
    var isFromTermsofservice = Bool()
    var userAgreement = Bool()
    
    //MARK: VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onClickCancelBtn)), animated: true)
        
        configureView()
    }
    
    static func getInstance() -> PrivicyPolicyViewController {
        
        let storyboard = UIStoryboard(name: "privicyPolicy", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PrivicyPolicyViewController") as! PrivicyPolicyViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        
        if webVw.isLoading {
            
            webVw .stopLoading()
            self.hideActivityIndicator()
        }
    }
    
    @objc func onClickCancelBtn() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - CONFIGUREVIEW
    func configureView() {
        
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor .white]
        
        webVw.scrollView.bounces = false
        webVw.delegate = self
        
        if isFromTermsofservice {
            
            //navigationItem.title = "Terms of Service"
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "terms1", value: ""))

        } else if isFromPrivacyPolicy {
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "privacy2", value: ""))

           // navigationItem.title = "Privacy Policy"
            
        } else {
            self.navigationItem.title = (L102Language.AMLocalizedString(key: "user3", value: ""))

           // navigationItem.title = "User Agreement"
        }
        
        loadWebVw()
    }
    
    func setUpUI() {
        
        //navigationController?.showNavigationBar()
        //navigationController?.navigationBar.applyGradient()
    }
    
    //MARK: - LOADVIEW
    func loadWebVw () {
        
        if Reachability.isNetworkAvailable() {
            
            self.showActivityIndicator()
            
            var webUrlStr: String? = nil
            
            if isFromTermsofservice {
                
                webUrlStr = "\(BASEURL)\(WEBVIEW_TERMS_CODITIONS)"
                
            } else if isFromPrivacyPolicy {
                
                webUrlStr = "\(BASEURL)\(WEBVIEW_PRIVICY_POLICY)"
                
            } else {
                
                webUrlStr = "\(BASEURL)\(WEBVIEW_USER_AGREEMENTS)"
            }
            
            if let url = URL(string: webUrlStr ?? "") {
                
                var urlRequest: URLRequest? = nil
                
                urlRequest = URLRequest(url: url)
                
                if let urlRequest = urlRequest {
                    
                    webVw.loadRequest(urlRequest)
                }
            }
            
        } else {
            
            self.hideActivityIndicator()
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: INTERNET_MSG)
        }
    }
    
}

//MARK: -  UIWebView Delegate
extension PrivicyPolicyViewController: UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.hideActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        self.hideActivityIndicator()
        
        // ERROR_MESSAGE = error.localizedDescription
        // UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
    }
}




