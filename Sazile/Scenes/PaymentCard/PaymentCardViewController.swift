//
//  PaymentCardViewController.swift
//  Sazile
//
//  Created by Harjit Singh on 25/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.

//   pk_test_i50kbLgAETfej6CHLxjU5BBV

import UIKit
import Stripe
import CoreData

class PaymentCardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var payMoneyCard : [String: Any] = [:]
    var getCardsArr : [[String: Any]] = []
    var deleateCard : [[String: Any]] = []
    var dummyArrq : [[String : Any]] = []
    var userId = String()
    var tokenKey = String()
    var deletecardId = String()
    var cardIdNum = NSInteger()
    var checkBox = Bool()
    var planId = String()
    var deleteCheckedArr = NSMutableArray()
    var deleteSuccessDict : NSDictionary = [:]
    var checkedRow: Int = -1
    var isFromPlansBool = Bool()
    var defualtCardID = String()
    var isFromDefualtCardBool = Bool()
    var selectCard = String()
    var params : NSDictionary = [:]
    
    var defaultCardShowStr = String()
    var massageID = Int()
   var massageID1 = Int()
    var strClientSecret : String?
    var strComeFrom : String? 

    
    @IBOutlet weak var noCardsAddedLbl: UILabel!
    @IBOutlet weak var noCardImgVw: UIImageView!
    @IBOutlet weak var addPaymentBtn: UIButton!
    @IBOutlet weak var paymentCardTableVw: UITableView!
    
    var paymentCardTextField = STPPaymentCardTextField()
    var brand = STPCardBrand(rawValue: 1)
    var sdkToken = String()
    var last4 = String()
    var stripeCardID = String()
    var expMonth = String()
    var expYear = String()
    var cardHolderName = String()
    var isSelectCard = Bool()
    
    var dummyText = String()
    
    //var saveBarBtnItem = UIBarButtonItem()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paymentCardTableVw.separatorStyle = .none
        self.navigationItem.title = (L102Language.AMLocalizedString(key: "payment_cards", value: ""))
        self.noCardsAddedLbl.text = (L102Language.AMLocalizedString(key: "no_cards", value: ""))
        //        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: (L102Language.AMLocalizedString(key: "save_text", value: "")), style: .plain, target: self, action: #selector(onClickSaveBtn))
        
        //title = "Payment Cards"
        checkBox = false
        
        if isFromPlansBool {
            
            let saveBarBtnItem = UIBarButtonItem(title: (L102Language.AMLocalizedString(key: "pay", value: "")), style: .plain, target: self, action: #selector(onClickSaveBtn))

            self.navigationItem.rightBarButtonItem = saveBarBtnItem
            
        } else {
            
            let saveBarBtnItem = UIBarButtonItem(title: (L102Language.AMLocalizedString(key: "save1", value: "")), style: .plain, target: self, action: #selector(onClickSaveBtn))
            self.navigationItem.rightBarButtonItem = saveBarBtnItem
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isFromDefualtCardBool = false
        isSelectCard = false
        dummyText = ""
        defualtCardID = ""
        
        print("Plan Id : \(planId)")
        checkBox = true
        self.noCardsAddedLbl.isHidden = true
        self.noCardImgVw.isHidden = true
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            
            let getCardsData = data as! NSDictionary
            
            tokenKey = getCardsData["token_key"] as? String ?? ""
            print(tokenKey)
            userId = getCardsData["user_id"] as? String ?? ""
            print(userId)
            
            getCards()
        }
    }
    
    func showDeleteAlert() {
        
        let alert = UIAlertController(title:nil , message: (L102Language.AMLocalizedString(key: "delete_this_card", value: "")),preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "no", value: "")), style: UIAlertAction.Style.default, handler: { _ in
            
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "yes", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.getDeleteCards()
                                        
                                        
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    static func getInstance() -> PaymentCardViewController {
        
        let storyboard = UIStoryboard(name: "PaymentCard", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PaymentCardViewController") as! PaymentCardViewController
    }
    
    
    func insertIntoDatabase(name : String,cardnumber : String,expiremonth : String,cardtype : String,cvc : String ) {
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        newUser.setValue(cardtype, forKey: "card_type")
        newUser.setValue(cardnumber, forKey: "card_number")
        newUser.setValue(name, forKey: "card_holder_name")
        newUser.setValue(expiremonth, forKey: "card_expiry")
        newUser.setValue(cvc, forKey: "cvc")
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    @objc func onClickSaveBtn() {
        
        if isFromPlansBool {
            
            // Pay Action
            
            if defualtCardID == ""  {
                
                UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "select_card", value: "")))
                
            } else {
                isFromDefualtCardBool = true
                
                getAddMinutesAPI()
                
                print("pay")
                
            }
            
        } else {
            
            if defualtCardID.isEmpty {
                
                UIAlertController.showAlert(vc: self, title: "", message: (L102Language.AMLocalizedString(key: "select_card", value: "")))
                
            } else {
                
                isFromDefualtCardBool = true
                print("Card ID For Default : \(defualtCardID)")
                getCards()
            }
        }
    }
    
    @objc func onClickSelectCardBtn(_ sender: UIButton) {
        
        if isSelectCard == false {
            
            print("Tapped the Btn")
            defualtCardID = getCardsArr[sender.tag]["card_id"] as? String ?? ""
            print("Card ID For Default : \(defualtCardID)")
            isSelectCard = true
            
        }else {
            
            dummyText = "@#$%^&*"
            isSelectCard = false
            defualtCardID = ""
        }
        
        paymentCardTableVw.reloadData()
        
    }
    
    
    func backTwo() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    //MARK: - UITABLEVIEW DELEGATE & DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return getCardsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardTableViewCell", for: indexPath) as! PaymentCardTableViewCell
        
        cell.cardTypeLbl.text = String(format: "%@", getCardsArr[indexPath.row]["card_type"] as? String ?? "")
        
        cell.cardNumberLbl.text = String(format: "%@", getCardsArr[indexPath.row]["card_number"] as? String ?? "")
        
        cell.holderNameLbl.text = String(format: "%@", getCardsArr[indexPath.row] ["card_holder_name"] as? String ?? "")
        
        cell.expireLbl.text = String(format: "%@", getCardsArr[indexPath.row] ["card_expiry"]as? String ?? "")
        cell.cardExpiryLbl.text = (L102Language.AMLocalizedString(key: "card_expiry", value: ""))
        cell.namelbl.text = (L102Language.AMLocalizedString(key: "card_holder", value: ""))
        
        defaultCardShowStr = getCardsArr[indexPath.row]["is_default_card"] as? String ?? ""
        
        cell.SelectCardBtn.tag = indexPath.row
        cell.SelectCardBtn .addTarget(self, action: #selector(onClickSelectCardBtn), for: .touchUpInside)
        
        cell.deleteCardBtn.tag = indexPath.row
        cell.deleteCardBtn .addTarget(self, action: #selector(self.onTapDeleateBtn), for: .touchUpInside)
        
        
        if isSelectCard == true {
            
            if defualtCardID == getCardsArr[indexPath.row]["card_id"] as? String {
                
                cell.selectCardImg.image = UIImage(named: "checkbox")
                cell.paymentCardVw.backgroundColor = UIColor.darkGray
                
            }else {
                
                cell.selectCardImg.image = UIImage(named: "Ellipse 1")
                cell.paymentCardVw.backgroundColor = UIColor.lightGray
                
            }
            
        }else {
            
            if dummyText == "@#$%^&*" {
                
                cell.selectCardImg.image = UIImage(named: "Ellipse 1")
                cell.paymentCardVw.backgroundColor = UIColor.lightGray
                
            }else {
                if strComeFrom == "changecard"{
                    cell.selectCardImg.image = UIImage(named: "Ellipse 1")
                    cell.paymentCardVw.backgroundColor = UIColor.lightGray
                }else{
                if defaultCardShowStr == "1" {
                    
                    cell.selectCardImg.image = UIImage(named: "checkbox")
                    cell.paymentCardVw.backgroundColor = UIColor.darkGray
                    
                } else {
                    
                    cell.selectCardImg.image = UIImage(named: "Ellipse 1")
                    cell.paymentCardVw.backgroundColor = UIColor.lightGray
                 }
                }
            }
            
        }
        
        cell.selectionStyle = .none
        cell.paymentCardVw.layer.borderWidth = 1.0
        cell.paymentCardVw.layer.borderColor = UIColor.gray.cgColor
        cell.paymentCardVw.layer.cornerRadius = 10
        cell.paymentCardVw.clipsToBounds = true
        
        
        return cell
    }
    
    
    //MARK: - IB ACTIONS
    
    @IBAction func onTapSelectCardBtn(_ sender: UIButton) {
        
        print("Taped Checkbox Button")
        
        defualtCardID = getCardsArr[sender.tag]["card_id"] as? String ?? ""
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = paymentCardTableVw.cellForRow(at:indexPath) as! PaymentCardTableViewCell
        if (isFromDefualtCardBool)
        {
            cell.selectCardImg.image = UIImage(named: "checkbox")
            cell.paymentCardVw.backgroundColor = UIColor.darkGray
            isFromDefualtCardBool = false
            paymentCardTableVw.reloadData()
            
        } else {
            
            cell.selectCardImg.image = UIImage(named: "Ellipse 1")
            cell.paymentCardVw.backgroundColor = UIColor.lightGray
            isFromDefualtCardBool = true
            
        }
        
    }
    
    @IBAction func onTapDeleateBtn(_ sender: UIButton) {
        
        print("Taped Delete Button")
        DispatchQueue.main.async {
            
            self.deletecardId = self.getCardsArr[sender.tag]["card_id"] as? String ?? ""
            print(self.deletecardId)
            self.showDeleteAlert()
        }
        
    }
    
    @IBAction func onTapPayBtn(_ sender: Any) {
        
        DispatchQueue.main.async {
            let aVC = AddCardViewController.getInstance()
            self.navigationController?.pushViewController(aVC, animated: true)
        }
        
    }
}

//MARK: - PAYMENTCARD API
extension PaymentCardViewController{
    
    func getCards() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(GET_CARDS_DEFULT)"
            
            if isFromDefualtCardBool == false {
                
                params = [  "user_id": userId,
                            "token_key": tokenKey
                ]
                
            } else {
                
                params = [  "user_id": userId,
                            "token_key": tokenKey,
                            "default_card_id": defualtCardID
                ]
            }
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in self.params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as! String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Payment Card Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                if let data = json["data"] as? [[String : Any]] {
                                    self.getCardsArr = data
                                    
                                    for index in self.getCardsArr {
                                        
                                        let dict = index as NSDictionary
                                        
                                        if let isDefaultCard = dict["is_default_card"] as? String {
                                            if isDefaultCard == "1" {
                                                
                                                print("IS DEFAULT CARD DICT : \(dict)")
                                                UserDefaults.standard.set(dict, forKey: "DEFAULT_CARD_DATA")
                                                
                                            } else {
                                                
                                                
                                            }
                                        }
                                    }
                                    
                                    if self.isFromDefualtCardBool {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "save_success", value: "")), buttonTitle: "Ok") {
                                            
                                            // isForSelectCardBool = true
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                    
                                    for i in 0..<self.getCardsArr.count{
                                        
                                        if let DefaultCard = self.getCardsArr[i]["is_default_card"] as? String{
                                            if DefaultCard == "1" {
                                                var dict = self.getCardsArr[i]
                                                dict["isSelectedRow"] =  "true"
                                                self.getCardsArr[i] = dict
                                                
                                            } else {
                                                var dict = self.getCardsArr[i]
                                                dict["isSelectedRow"] =  "false"
                                                self.getCardsArr[i] = dict
                                            }
                                        }
                                    }
                                    
                                    print(self.getCardsArr)
                                    if self.getCardsArr.count > 0 {
                                        self.noCardImgVw.isHidden = true
                                        
                                        self.paymentCardTableVw.isHidden = false
                                        self.noCardsAddedLbl.isHidden = true
                                        self.noCardImgVw.isHidden = true
                                        
                                        DispatchQueue.main.async {
                                            
                                            self.paymentCardTableVw.delegate = self
                                            self.paymentCardTableVw.dataSource = self
                                            self.paymentCardTableVw.reloadData()
                                        }
                                        
                                    } else {
                                        
                                        self.paymentCardTableVw.isHidden = true
                                        self.noCardImgVw.isHidden = false
                                        self.noCardsAddedLbl.isHidden = false
                                    }
                                }
                                
                            }
                            else
                            {
                                
                                if let error = json["message"] as? String {
                                    
                                    ERROR_MESSAGE = error
                                    
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        
                                        self.userSessionExpired()
                                        
                                    } else {
                                        
                                        self.hideActivityIndicator()
                                        //UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                        self.paymentCardTableVw.isHidden = true
                                        self.noCardImgVw.isHidden = false
                                        self.noCardsAddedLbl.isHidden = false
                                    }
                                }
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("cards Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))

           // UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
    //MARK: - ADD MINUTES API
    
    func getAddMinutesAPI() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(SUBSCRIBE_PLAN)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "plan_id": planId,
                          "card_id": defualtCardID
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Add Minutes Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            let authRequired : Int = json["is_authentication_required"] as? Int ?? 0

                            let data = json["data"] as! [String:Any]
                            if(status_code == "1" && authRequired == 0) {
                                
                                self.backTwo()
                                self.massageID = json["message_id"] as? Int ?? 0
                                print("MASSAGE ID: =====> \(self.massageID)")
                                
                                if self.massageID == 92 {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "plan_add", value: "")) as? String ?? "", buttonTitle: "OK") {
                                        
                                        self.getCards()
                                        
                                    }
                                    
                                } else {
                                    
                                    UIAlertController.showAlertWithOkAction(self, title: "", message: json["message"] as? String ?? "", buttonTitle: "OK") {
                                        
                                    }
                                    
                                }
                                
                               
                                
                                if let data = json["data"] as? [String : Any] {
                                    self.payMoneyCard = data
                                    //self.planDataArray = data
                                    
                                    DispatchQueue.main.async {
                                        
                                    }
                                }
                                
                            }
                            else{
                                self.strClientSecret = data["client_secret"] as? String ?? ""

                                if authRequired == 1{
                                    self.setUPIntenet(id: data["id"] as? String ?? "", order_id: data["order_id"]  as? Int ?? 0)
                                }
                                else{
                                if let error = json["message"] as? String {
                                    ERROR_MESSAGE = error
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        self.userSessionExpired()
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                 }
                                }
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Add minutes Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    //Used for 3D auth
    /// client secret from used here if card is 3D secure then new window open for 3D auth
    /// if infomation provided on 3D auth screen is correct then success block will call other wise failure block call
    func setUPIntenet(id:String,order_id:Int) {
            guard let setupIntentClientSecret = strClientSecret else {
                return;
            }

        let paymentIntentParams = STPPaymentIntentParams(clientSecret: setupIntentClientSecret)
        paymentIntentParams.paymentMethodId = id

        STPPaymentHandler.shared().confirmPayment(paymentIntentParams, with: self) { (status, paymentIntent, error) in
                 switch (status) {
                 case .failed:
                    print(error?.localizedDescription ?? "")
                    print(error?.localizedFailureReason ?? "")
                    print(error?.localizedRecoveryOptions ?? "")
                    print(error?.userInfo ?? "")
                     break
                 case .canceled:
                     break
                 case .succeeded:
                    self.retrievePlanAPI(order_id: order_id, payment_intent_id: paymentIntent!.stripeId)
                     break
                 @unknown default:
                     fatalError()
                     break
                 }
             }
        }
    
    
    func retrievePlanAPI(order_id:Int,payment_intent_id:String) {
//        user_id:1
//        token_key:DEON1fgjHKgCxUF9t5eCg085w
//        order_id:190
//        payment_intent_id:pi_1IIq8FIjUebwhMeLZi5hSXAG

        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(retrieve_plan_payment)"
            
            let params = ["user_id": userId,
                          "token_key": tokenKey,
                          "order_id": String(format: "%i", order_id),
                          "payment_intent_id": payment_intent_id
                ] as [String : Any]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Add Minutes Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""

                            if(status_code == "1" ) {
                                UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "plan_add", value: "")) as? String ?? "", buttonTitle: "OK") {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            else{
                                if let error = json["message"] as? String {
                                    ERROR_MESSAGE = error
                                    if ERROR_MESSAGE == INVALID_TOKEN_KEY {
                                        self.userSessionExpired()
                                    } else {
                                        
                                        UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    }
                                 }
                            }
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    print("Add minutes Error:\(err.localizedDescription)")
                    
                }
            }
        }else{
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))
            //            UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
}

//token:87dWl1nusLFc8TJqcXWF8ECgyb26JGbROoOaqcFSXk56ePwrMt // User Token
//user_id:52
//card_id:125
//MARK: - DELETECARD API

extension PaymentCardViewController{
    
    func getDeleteCards() {
        
        if Reachability.isNetworkAvailable(){
            
            let urlString = "\(BASEURL)\(DELETECARD)"
            
            let params = [  "user_id": userId,
                            "token": tokenKey,
                            "card_id": deletecardId
            ]
            
            #if DEBUG
            
            print("BEGIN SAZILE======>URLSTRING" + urlString)
            print("BEGIN SAZILE=====Parameters>\(params)")
            
            #endif
            
            self.showActivityIndicator()
            
            ServiceLayer.upload(multipartFormData: { (MultipartFormData) in
                for (key,values) in params{
                    MultipartFormData.append("\(values)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post){ (result) in
                
                
                switch result{
                    
                case .success(let upload, _,_):
                    
                    upload.responseJSON { response in
                        
                        #if DEBUG
                        
                        print(" Delete Card Response : \(response)")
                        
                        #endif
                        
                        if let err = response.error {
                            
                            self.hideActivityIndicator()
                            print(err)
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Failure")
                            
                            return
                        }
                        
                        if let result = response.result.value {
                            
                            let json = result as! NSDictionary
                            
                            let status_code : String = json["success"] as? String ?? ""
                            
                            if(status_code == "1") {
                                
                                self.getCards()
                                //
                                //self.paymentCardTableVw.reloadData()
                                self.massageID1 = json["message_id"] as? Int ?? 0
                                
                                if self.massageID1 == 100 {
                                    
                                    DispatchQueue.main.async {
                                        
                                        UIAlertController.showAlertWithOkAction(self, title: "", message: (L102Language.AMLocalizedString(key: "card_delete_success", value: "")), buttonTitle: "Ok") {
                                            
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        
                                    }
                                } else {
                                    
                                    UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                                    
                                    
                                }
                                
                            }
                            else
                            {
                                self.hideActivityIndicator()
                                UIAlertController.showAlert(vc: self, title: "", message: json["message"] as? String ?? "")
                            }
                            
                            self.hideActivityIndicator()
                        }
                    }
                    
                case .failure(let err):
                    
                    
                    print("Delete card Error:\(err.localizedDescription)")
                    
                }
            }
        }
        else
        {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_connection", value: "")))

           // UIAlertController.showAlert(vc: self, title: "", message:"Internet not avaliable")
        }
    }
    
    
}

extension PaymentViewController
{
    
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        
        
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
    }
}
extension PaymentCardViewController: STPAuthenticationContext {
  func authenticationPresentingViewController() -> UIViewController {
      return self
  }
}
