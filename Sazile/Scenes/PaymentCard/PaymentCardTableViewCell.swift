//
//  PaymentCardTableViewCell.swift
//  Sazile
//
//  Created by Harjit Singh on 30/09/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import UIKit

class PaymentCardTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteCardBtn: UIButton!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var cardNameLbl: UILabel!
    @IBOutlet weak var cardExpiryLbl: UILabel!
    @IBOutlet weak var selectCardImg: UIImageView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var checkBoxImgVw: UIImageView!
    @IBOutlet weak var expireLbl: UILabel!
    @IBOutlet weak var holderNameLbl: UILabel!
    @IBOutlet weak var cardNumberLbl: UILabel!
    @IBOutlet weak var cardTypeLbl: UILabel!
    @IBOutlet weak var clickBtn: UIButton!
    @IBOutlet weak var paymentCardVw: UIView!
    
    
    @IBOutlet var SelectCardBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
