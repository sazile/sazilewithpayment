//
//  Toast+Extensions.swift
//  Yprra
//
//  Created by Harjit Singh's Mac (F35) on 20/06/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import Foundation
import Toast_Swift
import UIKit


var style = ToastStyle()

extension UIViewController {
    
    func initializeToast() {
        
        style.messageColor = UIColor.black
        style.messageAlignment = .center
        style.backgroundColor = UIColor.white
    }

}
