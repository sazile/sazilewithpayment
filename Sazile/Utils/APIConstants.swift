//
//  APIConstants.swift
//  Sazile
//
//  Created by Harjit Singh on 07/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import Foundation

//let BASEURL = "https://www.intellisensetechnologies.com/sazile/webservices/" // live  URL

/*Live:- (intellisensetechnologies.com)
Base URL: https://www.intellisensetechnologies.com/sazile/webservices/

Local System:- (Deepak Mishra)
Base URL: http://192.168.0.34/sazile/webservices/
 */

let BASEURL = "https://sazile.fr/app/webservices/"

let SIGNUP = "users/sign_up"
let LOGIN = "auth/login"
let VERIFY = "auth/verify"
let RESEND_OTP = "auth/resend_otp"
let VERIFY_PHONE = "users/verify_phone"
let VERIFY_PHONE1 = "users/verify_phone_3030"
let VERIFY_PHONE2 = "users/verify_phone_2020"
let VERIFY_PHONE_DIRECTLY = "users/verify_phone_directly"
let verify_password_directly = "users/forgot_password_directly"

let DEVICEMISSING_REPORT = "device/missing_report"
let NEARBYDEVICES = "device/nearby_all"
let UPDATEDEVICE_LOCATION = "device/update_location"
let BOOK_DEVICE_SCOOTOR = "ride/book_request"
let START_RIDE = "ride/start"
let LIVE_DEVICE_STATUS = "device/get_device_info"
let STOP_RIDE = "ride/stop"
let ZONE_DATA = "device/zone_data"
let GETTHE_QRIMAGES_WITHID = "device/qr_list"
let PLAN_LIST = "plan/plan_list"
let REFERRAL_HISTORY = "referral/allreferrals"
let NOTIFICATIONS = "notification/list"
let MESSEAGE = "notification/message"
let ADD_CARD = "payment/create_customer"
let PAYMENT_LIST = "payment/payment_list"
let SUBSCRIBE_PLAN = "plan/subscribe_plan"
//let GET_CARDS = "payment/get_cards"
let GET_CARDS_DEFULT = "payment/get_cards_ios"
let MYWALLET = "users/my_wallet"
let PAYMENT_OPTIONS = "payment/payment_options"
let DELETECARD = "payment/delete_card"
let CHECK_SCOOTER_BOOKINGBY_USER = "device/check_scooter"
let RIDE_HISTORY_BYUSER = "ride/history"
let MYTRIPS = "users/my_trips"
let RIDEHISTRORY_DETAILS = "ride/ride_history_details"
let GETPROFILEOF_USERBY_ID = "users/get_profile"
let SETPROFILEOF_USERBY_ID = "users/set_profile"
let CHANGE_PASSWORD = "users/change_password"
let DEVICE_MISSING_REPORT = "device/missing_report"
let MYCOUPONS = "users/my_coupons"
let ACTIVE_COUPONS = "users/active_coupons"
let ADD_PAYMENT = "payment/add_payment"
let SOCIAL_LOGIN = "auth/fb_login"
let CHARGER_SIGNUP = "charge/signup"
let FIND_DEVICES = "charge/find_device"
let CAPTURE_DEVICES = "charge/capture_device"
let RELEASE_DEVICES = "charge/release_device"
let GET_RELEASE_SPOT_DEVICES = "charge/get_release_spot"
let ACTIVETASK = "charge/get_tasklist"
let EARNING_HISTORY = "charge/earning_history"
let GET_DAY_EARNING = "charge/get_day_earning"
let WEBVIEW_FORFAQ = "content/faq"
let WEBVIEW_CHARGER_HELP = "content/charger_help"
let WEBVIEW_SAFTEY = "content/safety"
let WEBVIEW_TERMS_CODITIONS = "content/terms"
let WEBVIEW_PRIVICY_POLICY = "content/privacy_policy"
let WEBVIEW_USER_AGREEMENTS = "content/user_agreement"
let WEBVIEW_RIDERHELP = "content/rider_help"
let UPDATE_PASSWORD = "users/update_password"
let UPDATEDOCUMENTS = "users/update_docs"
let NOTIFICATIONS_COUNT = "users/notification_count"
let DEVICE_LIST = "device/scooter_list"
let MY_WALLET = "users/my_wallet"
let NOTIFICATION_TIME = "users/get_content_updtion_time"
let TAP_CASE_BOX = "device/top_box_open"
let LOGOUT_API = "users/ride_status_for_logout"
let HoldedAmount = "payment/hold_refund_summery"
let commonconfig = "common/config"
let TOM_TOM_ROUTE = ""
let quotient_summary = "payment/quotient_summary"
let setupintent = "payment/setupintent"
let get_configurations = "common/config"
let retrieve_plan_payment = "payment/retrieve_plan_payment"
let retrieve_hold_payment = "payment/retrieve_hold_payment"
let delete_Account = "users/delete_user"







