//
//  GlobalConstants.swift
//  Sazile
//
//  Created by Harjit Singh on 07/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import Foundation
import Alamofire

//MARK: - APP Constants

let APP_NAME = "Sazile"

let TIME_OUT_INTERVAL = 10.0


//Alamofire Session Manager
let ServiceLayer = Alamofire.SessionManager.default
    
//MARK: - BOOL

var isForSelectImageReportingBool = Bool()
var isForSelectCardBool = Bool()
var isForEndRideBool = Bool()
var isFromDismissRideDetailsVwBool = Bool()

var isCountrySelectedForLogIn = Bool()
var isCountrySelectedForForgot = Bool()
var isCountrySelectedForRegister = Bool()
var isForImageBool = Bool()
var istimerAgainBool = Bool()
var isSearchBarVwBool = Bool()
var isFromLoginToForgot = Bool()
var isForScooterStopadmin = Bool()


//MARK: - Alert Titles

var ERROR_MESSAGE = String()

let STATUS = "status"
let ALERT_TITLE = "Oops !"
let BUTTON_TITLE = "Ok"
let REQUEST_TIMED_OUT = "Unable to connect, Please try again later."
let INTERNET_MSG = "The Internet connection appears to be offline."
let ERROR = "Error"

let INVALID_TOKEN_KEY = "Incorrect user id or token key"


//MARK: NSNotificationCenter

let LOCALIZATIONREFRESH = "LocalizationRefresh"
let REFRESHPROFILEDATA = "RefreshProfileDataForSideMenu"
let NOTIFICATIONREFRESH = "NotificationRefresh"
let GETNEARBYSCOOTERS = "Getnearbyscooters"
//MARK: Dictionary

var notificationsCount : [String: Any] = [:]

var notificationTimer : Timer?
var zoneRedorNoneTimer : Timer? = Timer()
