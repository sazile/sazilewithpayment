//
//  UIColor+CustomColor.swift
//  Ally
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Unyde. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    class func getLightTextColor() -> UIColor{
        return UIColor(red:230.0/255, green:230.0/255 ,blue:230.0/255, alpha:1.00)
    }
    class func locateMeButtonBGColor() -> UIColor{
        return UIColor(red:118.0/255, green:129.0/255 ,blue:247.0/255, alpha:1.00)
    }
    class func CarouselBG() -> UIColor{
        return UIColor(red:221.0/255, green:221.0/255 ,blue:221.0/255, alpha:1.00)
    }
    class func markerLableColor() -> UIColor{
        return UIColor(red:255.0/255, green:199.0/255 ,blue:91.0/255, alpha:1.00)
    }
    class func blueTostBG() -> UIColor{
        return UIColor(red:94.0/255, green:173.0/255 ,blue:232.0/255, alpha:1.00)
    }
    class func allyLightGray() -> UIColor{
        return UIColor(red:170.0/255, green:170.0/255 ,blue:170.0/255, alpha:1.00)
    }
    class func allyBlueTheme() -> UIColor{
        return UIColor(red:255.0/255, green:255.0/255 ,blue:255.0/255, alpha:0.70)
    }
    class func blueBG() -> UIColor{
        return UIColor(red:92/255, green:101.0/255 ,blue:203.0/255, alpha:1.00)
    }
    class func redWoogly() -> UIColor{
        return UIColor(red:225.0/255, green:38.0/255 ,blue:0.0/255, alpha:1.00)
    }
    class func buttonDisable() -> UIColor{
        return UIColor(red:204.0/255, green:204.0/255 ,blue:204.0/255, alpha:1.00)
    }
    class func buttonEnable() -> UIColor{
        return UIColor(red:37.0/255, green:45.0/255 ,blue:56.0/255, alpha:1.00)
    }
    class func placeHolderTextColor() -> UIColor{
        return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
    }
    class func floorSelectedColor() -> UIColor{
        return UIColor(red: 121/255, green: 191/255, blue: 81/255, alpha: 1.00)
    }
    class func disabledColor() -> UIColor{
        return UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1.00)
    }
    class func blueButton() -> UIColor{
        return UIColor(red: 65/255, green: 127/255, blue: 228/255, alpha: 1.00)
    }
    class func toastBG() -> UIColor{
        return UIColor(red:35.0/255, green:45.0/255 ,blue:56.0/255, alpha:1.00)
    }
    class func InterestSelectionColor() -> UIColor{
        return UIColor(red:0.0/255, green:0.0/255 ,blue:0.0/255, alpha:0.7)
    }
}
extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        assert(hexFormatted.count == 6, "Invalid hex code used.")

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
}
