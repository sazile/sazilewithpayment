//
//  AppHelper.swift
//  Sazile
//
//  Created by harjitsingh on 01/11/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import Foundation

class AppHelper {
    
    class func isValidEmail(with email: String?) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: email)
    }
    
    
}
